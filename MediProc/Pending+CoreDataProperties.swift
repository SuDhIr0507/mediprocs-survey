//
//  Pending+CoreDataProperties.swift
//  
//
//  Created by codeworx on 9/30/19.
//
//

import Foundation
import CoreData


extension Pending {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Pending> {
        return NSFetchRequest<Pending>(entityName: "Pending")
    }

    @NSManaged public var id: String?
    @NSManaged public var jsonObject: NSData?
    @NSManaged public var error: String?

}

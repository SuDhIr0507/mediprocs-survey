//
//  City+CoreDataProperties.swift
//  
//
//  Created by codeworx on 9/5/19.
//
//

import Foundation
import CoreData


extension City {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<City> {
        return NSFetchRequest<City>(entityName: "City")
    }

    @NSManaged public var cityArray: NSData?
    @NSManaged public var id: String?

}

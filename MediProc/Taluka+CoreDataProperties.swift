//
//  Taluka+CoreDataProperties.swift
//  
//
//  Created by codeworx on 9/5/19.
//
//

import Foundation
import CoreData


extension Taluka {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Taluka> {
        return NSFetchRequest<Taluka>(entityName: "Taluka")
    }

    @NSManaged public var talukaArray: NSData?
    @NSManaged public var id: String?

}

//
//  Registration+CoreDataProperties.swift
//  
//
//  Created by Avante on 03/09/19.
//
//

import Foundation
import CoreData


extension Registration {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Registration> {
        return NSFetchRequest<Registration>(entityName: "Registration")
    }

    @NSManaged public var jsonObject: NSData?
    @NSManaged public var id: String?

}

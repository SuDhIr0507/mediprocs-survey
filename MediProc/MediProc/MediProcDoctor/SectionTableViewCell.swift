//
//  SectionTableViewCell.swift
//  ExpandableTable
//
//  Created by codeworx on 3/20/19.
//  Copyright © 2019 Avante Codeworx. All rights reserved.
//

import UIKit

class SectionTableViewCell: UITableViewCell {

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    var checkId = Int()
    
    @IBOutlet weak var Headerview: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        Headerview.backgroundColor = UIColor.white
        Headerview.layer.borderWidth = 1
        Headerview.layer.borderColor = Color().hexStringToUIColor(hex: "#ECECEC").cgColor
        Headerview.layer.cornerRadius = 15
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
//        btnYes.setImage( UIImage(named: "radio-btn2.png"), for:.normal)
//        btnNo.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
    }

}

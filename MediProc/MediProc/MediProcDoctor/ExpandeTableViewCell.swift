//
//  ExpandeTableViewCell.swift
//  ExpandableTable
//
//  Created by codeworx on 3/25/19.
//  Copyright © 2019 Avante Codeworx. All rights reserved.
//

import UIKit

class ExpandeTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCell: UILabel!
    @IBOutlet weak var lblSrNotextfield: UILabel!
    @IBOutlet weak var lblCell2: UILabel!
    @IBOutlet weak var lblSrNoRadio: UILabel!
    @IBOutlet weak var bgRadiobtView: UIView!
    @IBOutlet weak var bgtxtView: UIView!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var textfieldAnswer: UITextField!
    
    @IBOutlet weak var txtRemark: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        

    }
    
}

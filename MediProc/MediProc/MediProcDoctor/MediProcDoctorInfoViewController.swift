//
//  MediProcDoctorInfoViewController.swift
//  MediProc
//
//  Created by Avante on 04/03/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation
import GoogleMaps
import Alamofire
import Contacts
import CoreData
import SkyFloatingLabelTextField

class MediProcDoctorInfoViewController: UIViewController,UITextFieldDelegate,NVActivityIndicatorViewable,CLLocationManagerDelegate,UITextViewDelegate,UIScrollViewDelegate {

    let moContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    fileprivate let pickerDistrict = ToolbarPickerView()
    fileprivate let pickerTaluka = ToolbarPickerView()
    fileprivate let pickerCity = ToolbarPickerView()
    fileprivate let pickerSpeciality = ToolbarPickerView()
    fileprivate let pickerTypeOfSetup = ToolbarPickerView()
    
    fileprivate var districtArray = [[String : AnyObject]]()
    fileprivate var talukaArray = [[String : AnyObject]]()
    fileprivate var cityArray = [[String : AnyObject]]()
    fileprivate var specialityArray = [[String : AnyObject]]()
    fileprivate var typeOfSetupArray = [[String : AnyObject]]()
    
    var districtID = Int()
    var talukaID = Int()
    var cityID = Int()
    var specialityID = Int()
    var setupID = Int()
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var btnCheckList: UIButton!
    @IBOutlet weak var btnDoctorInfo: UIButton!
    
    @IBOutlet weak var textfieldFirstname: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldMiddleName: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
 
    @IBOutlet weak var textfieldMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldEmailID: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldPracticeSpeciality: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldSpeciality: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldTypeOfSetup: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldNameofClinic: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSelectTaluka: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSelectDistrict: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSelectCity: SkyFloatingLabelTextField!
    
    @IBOutlet weak var textviewAddress: UITextView!
    @IBOutlet weak var textfieldClinicMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldLandlineNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldClinicEmailID: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldVendorName: SkyFloatingLabelTextField!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var LabInfoview: UIView!
    @IBOutlet weak var textfieldLabName: SkyFloatingLabelTextField!
    @IBOutlet weak var btnLabMale: UIButton!
    @IBOutlet weak var btnLabFemale: UIButton!
  
    @IBOutlet weak var textfieldLabInchargeMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldLabInchargeEmailID: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldCurrentLocation: SkyFloatingLabelTextField!
    @IBOutlet weak var btnRefresh: UIButton!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var btnSaveNext: UIButton!
    @IBOutlet weak var Personalinfoview: UIView!
    @IBOutlet weak var proficiencyinfoview: UIView!
    @IBOutlet weak var ClinicInfoview: UIView!
    @IBOutlet weak var currentLocationview: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var constraintContentHeight: NSLayoutConstraint!
    var activeField : UITextField?
    var lastOffset : CGPoint!
    var keyBoardHeight : CGFloat!
    
    
    var locationManager = CLLocationManager()
    var didFindMyLocation = false
    var flag = String()
    var latitude = Double()
    var longitude = Double()
    var currentlatitude = Double()
    var currentlongitude = Double()
    var gender = String()
    var female = String()
    var labgender = String()
    var labfemale = String()
    var labavailbility = String()
    var No = String()
    var combinestring = String()
  
    
    var centerMapCoordinate = CLLocationCoordinate2D()
    @IBOutlet weak var lblLabInfo: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var lblGender: UILabel!
   // @IBOutlet weak var inputViewBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      
        txtSelectDistrict.delegate = self
        txtSelectTaluka.delegate = self
        txtSelectCity.delegate = self
        textfieldSpeciality.delegate = self
        textfieldTypeOfSetup.delegate = self
        
        addRightTextButon(viewText: txtSelectDistrict, imagename: "dropdown")
        addRightTextButon(viewText: txtSelectTaluka, imagename: "dropdown")
        addRightTextButon(viewText: txtSelectCity, imagename: "dropdown")
        addRightTextButon(viewText: textfieldSpeciality, imagename: "dropdown")
        addRightTextButon(viewText: textfieldTypeOfSetup, imagename: "dropdown")

        gender = "1"
        labgender = "1"
        
        self.pickerSpeciality.dataSource = self
        self.pickerSpeciality.delegate = self
        self.pickerSpeciality.toolbarDelegate = self
        
        self.pickerDistrict.dataSource = self
        self.pickerDistrict.delegate = self
        self.pickerDistrict.toolbarDelegate = self
        
        self.pickerTypeOfSetup.dataSource = self
        self.pickerTypeOfSetup.delegate = self
        self.pickerTypeOfSetup.toolbarDelegate = self
        
        self.pickerTaluka.dataSource = self
        self.pickerTaluka.delegate = self
        self.pickerTaluka.toolbarDelegate = self
        
        self.pickerCity.dataSource = self
        self.pickerCity.delegate = self
        self.pickerCity.toolbarDelegate = self
        

        self.txtSelectDistrict.inputView = self.pickerDistrict
        self.txtSelectDistrict.inputAccessoryView = self.pickerDistrict.toolbar
        
        self.txtSelectTaluka.inputView = self.pickerTaluka
        self.txtSelectTaluka.inputAccessoryView = self.pickerTaluka.toolbar
        
        self.txtSelectCity.inputView = self.pickerCity
        self.txtSelectCity.inputAccessoryView = self.pickerCity.toolbar
        
        self.textfieldSpeciality.inputView = self.pickerSpeciality
        self.textfieldSpeciality.inputAccessoryView = self.pickerSpeciality.toolbar
        
        self.textfieldTypeOfSetup.inputView = self.pickerTypeOfSetup
        self.textfieldTypeOfSetup.inputAccessoryView = self.pickerTypeOfSetup.toolbar
    
        
        
        applyGradientView()
        applyGradient()
        
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x:btnDoctorInfo.frame.origin.x, y: btnDoctorInfo.frame.height + 1, width:btnDoctorInfo.frame.width, height:1.5)
        bottomBorder.backgroundColor = Color().hexStringToUIColor(hex: "#0060C5").cgColor
        btnDoctorInfo.layer.addSublayer(bottomBorder)
        
        textviewAddress.layer.borderWidth = 0.5
        textviewAddress.layer.borderColor = UIColor.lightGray.cgColor
        textviewAddress.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcDoctorInfoViewController.doneDatePickerPressed))
        
        toolBar.setItems([space, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        textfieldMobileNumber.inputAccessoryView = toolBar
        
        
        let toolBar1 = UIToolbar()
        toolBar1.barStyle = UIBarStyle.default
        toolBar1.isTranslucent = true
        let space1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton1 = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcDoctorInfoViewController.doneDatePickerPressed1))
        
        toolBar1.setItems([space1, doneButton1], animated: false)
        toolBar1.isUserInteractionEnabled = true
        toolBar1.sizeToFit()
        textfieldClinicMobileNumber.inputAccessoryView = toolBar1
        
        let toolBar2 = UIToolbar()
        toolBar2.barStyle = UIBarStyle.default
        toolBar2.isTranslucent = true
        let space2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton2 = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcDoctorInfoViewController.doneDatePickerPressed2))
        
        toolBar2.setItems([space2, doneButton2], animated: false)
        toolBar2.isUserInteractionEnabled = true
        toolBar2.sizeToFit()
        textfieldLandlineNumber.inputAccessoryView = toolBar2
        
        let toolBar3 = UIToolbar()
        toolBar3.barStyle = UIBarStyle.default
        toolBar3.isTranslucent = true
        let space3 = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton3 = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcDoctorInfoViewController.doneDatePickerPressed3))
        
        toolBar3.setItems([space3, doneButton3], animated: false)
        toolBar3.isUserInteractionEnabled = true
        toolBar3.sizeToFit()
        
        textfieldLabInchargeMobileNumber.inputAccessoryView = toolBar3
    
        
        // For use in foreground
        
    
        let toolBarEmailID = UIToolbar()
        toolBarEmailID.barStyle = UIBarStyle.default
        toolBarEmailID.isTranslucent = true
        let spaceEmailID = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButtonEmailID = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcDoctorInfoViewController.doneDatePickerPressedEmailID))
        
        toolBarEmailID.setItems([spaceEmailID, doneButtonEmailID], animated: false)
        toolBarEmailID.isUserInteractionEnabled = true
        toolBarEmailID.sizeToFit()
        textfieldEmailID.inputAccessoryView = toolBarEmailID
        
        
        //textfieldClinicEmailID
        
        let toolBarClinicEmailID = UIToolbar()
        toolBarClinicEmailID.barStyle = UIBarStyle.default
        toolBarClinicEmailID.isTranslucent = true
        let spaceClinicEmailID = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButtonClinicEmailID = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcDoctorInfoViewController.doneDatePickerPressedClinicEmailID))
        
        toolBarClinicEmailID.setItems([spaceClinicEmailID, doneButtonClinicEmailID], animated: false)
        toolBarClinicEmailID.isUserInteractionEnabled = true
        toolBarClinicEmailID.sizeToFit()
        textfieldClinicEmailID.inputAccessoryView = toolBarClinicEmailID
        
        //textfieldLabInchargeEmailID
        
        let toolBarLabInchargeEmailID = UIToolbar()
        toolBarLabInchargeEmailID.barStyle = UIBarStyle.default
        toolBarLabInchargeEmailID.isTranslucent = true
        let spaceLabInchargeEmailID = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButtonLabInchargeEmailID = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcDoctorInfoViewController.doneDatePickerPressedLabInchargeEmailID))
        
        toolBarLabInchargeEmailID.setItems([spaceLabInchargeEmailID, doneButtonLabInchargeEmailID], animated: false)
        toolBarLabInchargeEmailID.isUserInteractionEnabled = true
        toolBarLabInchargeEmailID.sizeToFit()
        textfieldLabInchargeEmailID.inputAccessoryView = toolBarLabInchargeEmailID
        
    
        btnRefresh.layer.borderWidth = 0.5
        btnRefresh.layer.borderColor = Color().hexStringToUIColor(hex: "#1561AD").cgColor
        btnRefresh.layer.cornerRadius = 10
        
        btnClear.layer.borderWidth = 1.0
        btnClear.layer.borderColor = Color().hexStringToUIColor(hex: "#1561AD").cgColor
        btnClear.layer.cornerRadius = 10
        
        btnSaveNext.layer.borderWidth = 0.5
        btnSaveNext.layer.borderColor = Color().hexStringToUIColor(hex: "#1561AD").cgColor
        btnSaveNext.layer.cornerRadius = 10
        
        Personalinfoview.layer.cornerRadius = 5
        proficiencyinfoview.layer.cornerRadius = 5
        ClinicInfoview.layer.cornerRadius = 5
        currentLocationview.layer.cornerRadius = 5
        
        //checkLocation()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability().isConnectedToNetwork() == true {
            print("Connected to the internet")

            ////////// Get speciality list //////////////
            
            let specialityarray = UserData().getSpecialityArrayData().globalSpecialityArray
            if specialityarray.count != 0 {
                self.specialityArray = specialityarray as! [[String : AnyObject]]
                pickerSpeciality.reloadAllComponents()
            }
            else{
                WebserviceCall().getDoctorSpeciality({ Array in
                    self.specialityArray = Array as! [[String : AnyObject]]
                    self.pickerSpeciality.reloadAllComponents()
                })
            }
            
            ////////// Get setup type list //////////////
            
            let setuparray = UserData().getSetupTypeArrayData().globalSetupTypeArray
            if setuparray.count != 0 {
                self.typeOfSetupArray = setuparray as! [[String : AnyObject]]
                pickerTypeOfSetup.reloadAllComponents()
            }
            else{
                WebserviceCall().getTypeOfSetup({ Array in
                    self.typeOfSetupArray = Array as! [[String : AnyObject]]
                    self.pickerTypeOfSetup.reloadAllComponents()
                })
            }
            
            ////////// Get district list //////////////
            
            let districtarray = UserData().getDistrictArrayData().globalDistrictArray
            if districtarray.count != 0 {
                self.districtArray = districtarray as! [[String : AnyObject]]
                pickerDistrict.reloadAllComponents()
            }
            else{
                WebserviceCall().getAllDistrictFromServer({ Array in
                    self.districtArray = Array as! [[String : AnyObject]]
                    self.pickerDistrict.reloadAllComponents()
                })
            }
            
            //////////////////////////////////////////////
            
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startUpdatingLocation()
            }
            
        } else {
            
            print("No internet connection")
            
            ////////// Get speciality list //////////////
            
            let specialityarray = UserData().getSpecialityArrayData().globalSpecialityArray
            if specialityarray.count != 0 {
                self.specialityArray = specialityarray as! [[String : AnyObject]]
                pickerSpeciality.reloadAllComponents()
            }
            else{
                Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please Check Your Internet Connection")
            }
            
            ////////// Get setup type list //////////////
            
            let setuparray = UserData().getSetupTypeArrayData().globalSetupTypeArray
            if setuparray.count != 0 {
                self.typeOfSetupArray = setuparray as! [[String : AnyObject]]
                pickerTypeOfSetup.reloadAllComponents()
            }
            else{
                Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please Check Your Internet Connection")
            }
            
            ////////// Get District list //////////////
            
            let districtarray = UserData().getDistrictArrayData().globalDistrictArray
            if districtarray.count != 0 {
                self.districtArray = districtarray as! [[String : AnyObject]]
                pickerDistrict.reloadAllComponents()
            }
            else{
                Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please Check Your Internet Connection")
            }
            
            //////////////////////////////////////////////
            
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtSelectDistrict
        {
            flag = "District"
            txtSelectTaluka.text = ""
            talukaID = 0
            txtSelectCity.text = ""
            cityID = 0
            self.txtSelectDistrict.inputView = self.pickerDistrict
            self.txtSelectDistrict.inputAccessoryView = self.pickerDistrict.toolbar
        }
        else if textField == txtSelectTaluka
        {
            if txtSelectDistrict.text == "" && districtID == 0
            {
                txtSelectTaluka.resignFirstResponder()
                Alert().showAlertMessage(vc:self, titleStr: "Error", messageStr:"Please select district")
            }
            else{
                flag = "Taluka"
                txtSelectCity.text = ""
                cityID = 0
                
                self.txtSelectTaluka.inputView = self.pickerTaluka
                self.txtSelectTaluka.inputAccessoryView = self.pickerTaluka.toolbar
                getTalukaAndCityData(districtId:districtID)
              
            }
        }
        else if textField == txtSelectCity
        {
            if txtSelectDistrict.text == "" && districtID == 0
            {
                txtSelectCity.resignFirstResponder()
                Alert().showAlertMessage(vc:self, titleStr: "Error", messageStr:"Please select district")
            }
            else{
                    flag = "City"
                    self.txtSelectCity.inputView = self.pickerCity
                    self.txtSelectCity.inputAccessoryView = self.pickerCity.toolbar
                    self.pickerCity.reloadAllComponents()
                    getTalukaAndCityData(districtId:districtID)
            }
        }
        else if textField == textfieldSpeciality
        {
                flag = "Speciality"
                self.textfieldSpeciality.inputView = self.pickerSpeciality
                self.textfieldSpeciality.inputAccessoryView = self.pickerSpeciality.toolbar
                self.pickerSpeciality.reloadAllComponents()
        }
        else if textField == textfieldTypeOfSetup
        {
                flag = "TypeOfSetup"
                self.textfieldTypeOfSetup.inputView = self.pickerTypeOfSetup
                self.textfieldTypeOfSetup.inputAccessoryView = self.pickerTypeOfSetup.toolbar
                self.pickerTypeOfSetup.reloadAllComponents()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
  
        let location = locations.last
        currentlatitude = (location?.coordinate.latitude)!
        currentlongitude = (location?.coordinate.longitude)!
       
        let lat = String(format: "%.7f", currentlatitude)
        let longit = String(format: "%.7f", currentlongitude)
          combinestring = lat + "," + longit
        textfieldCurrentLocation.text = ("\(combinestring)")
       
        getAddressFromLatLon(pdblLatitude: lat, withLongitude: longit)
        locationManager.stopUpdatingLocation()
        
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)

        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
            
                let pm = placemarks as? [CLPlacemark]

                if pm?.count ?? 0 > 0 {
                    let pm = placemarks![0]

//                    print("placemark array",pm)
//                   
//                    print(pm.country)
//                    print(pm.locality)
//                    print(pm.subLocality)
//                    print(pm.thoroughfare)
//                    print(pm.postalCode)
//                    print(pm.subThoroughfare)
                    var addressString : String = ""

                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.subThoroughfare != nil {
                        addressString = addressString + pm.subThoroughfare! + "\n"
                    }
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.administrativeArea != nil
                    {
                        addressString = addressString + pm.administrativeArea! + ", "
                    }
//                    if pm.subAdministrativeArea != nil
//                    {
//                        addressString = addressString + pm.subAdministrativeArea! + ", "
//                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    print(addressString)
                    self.textviewAddress.text = addressString

                   
                }

        })

    }
    
    func checkLocation()
    {
        // 1
        let status  = CLLocationManager.authorizationStatus()
        
        // 2
        if status == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
            return
        }
        
        // 3
        if status == .denied || status == .restricted {
            let alert = UIAlertController(title: "Location Services Disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: .default, handler:{action in
                
                let settingsUrl = URL(string: UIApplication.openSettingsURLString)
                if UIApplication.shared.canOpenURL(settingsUrl!)  {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl!, completionHandler: { (success) in
                        })
                    }
                    else  {
                        let url = URL(string : "prefs:root=")
                        if UIApplication.shared.canOpenURL(url!) {
                            UIApplication.shared.openURL(url!)
                        }
                    }
                }
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler:nil)
            
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            
            present(alert, animated: true, completion: nil)
            return
        }
        
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        
    }
   
    func applyGradientView()
    {
        let GLlayer = CAGradientLayer()
        let hexclass = HexFromColorCodeClass()
        var  first = UIColor()
        var  second = UIColor()
        first = hexclass.hexStringToUIColor(hex:"#0060C5")
        second = hexclass.hexStringToUIColor(hex:"#2BB1FE")
        GLlayer.startPoint = CGPoint(x: 0, y: 0.5)
        GLlayer.endPoint = CGPoint(x: 1, y: 0.5)
        GLlayer.colors = [first,second]
        
        self.navView.layer.addSublayer(GLlayer)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func applyGradient() {
        
        let gradient = CAGradientLayer()
        let hexclass = HexFromColorCodeClass()
        
        let Start = hexclass.hexStringToUIColor(hex:"#0060C5").cgColor
        let End = hexclass.hexStringToUIColor(hex:"#2BB1FE").cgColor
        
        gradient.colors = [Start,End]
        gradient.locations = [0.0,0.5,1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.frame = CGRect(origin: CGPoint.zero, size: self.view.frame.size) //self.view.bounds
        self.view.layer.insertSublayer(gradient, at:0)
    }
    
    @objc func doneDatePickerPressed(){
        
        self.view.endEditing(true)
        
        textfieldMobileNumber.resignFirstResponder()
        
        if validateMobile(value: textfieldMobileNumber.text!) == false
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Valid Mobile Number", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func doneDatePickerPressed1(){
        self.view.endEditing(true)
        
        textfieldClinicMobileNumber.resignFirstResponder()
        
        if validateMobile(value: textfieldClinicMobileNumber.text!) == false
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Valid Mobile Number", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func doneDatePickerPressed2(){
        self.view.endEditing(true)
        
        textfieldLandlineNumber.resignFirstResponder()

//        if validateLandline(value: textfieldLandlineNumber.text!)
//        {
//            //print("Valid Landline number",textfieldLandlineNumber.text)
//        }
//        else
//        {
//            let alert = UIAlertController(title: "Warning", message: "Please Enter Valid Landline Number", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
      
    }
    
    @objc func doneDatePickerPressed3(){
        self.view.endEditing(true)
        
        textfieldLabInchargeMobileNumber.resignFirstResponder()
        
        if validateMobile(value: textfieldLabInchargeMobileNumber.text!) == false
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Valid Mobile Number", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func doneDatePickerPressedEmailID(){
        self.view.endEditing(true)
        
        textfieldEmailID.resignFirstResponder()
        
//        if isValidEmail(testStr: textfieldEmailID.text!) == false
//        {
//            let alert = UIAlertController(title: "Warning", message: "Please Enter Valid Email ID", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
//
//        else
//        {
//            textfieldEmailID.resignFirstResponder()
//        }
    }

    @objc func doneDatePickerPressedClinicEmailID(){
        self.view.endEditing(true)
        
        textfieldClinicEmailID.resignFirstResponder()
        
//        if isValidEmail(testStr: textfieldClinicEmailID.text!) == false
//        {
//            let alert = UIAlertController(title: "Warning", message: "Please Enter Valid Email ID", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
//        else
//        {
//            textfieldClinicEmailID.resignFirstResponder()
//        }
    }

    @objc func doneDatePickerPressedLabInchargeEmailID(){
        self.view.endEditing(true)
        
        textfieldLabInchargeEmailID.resignFirstResponder()
        
//        if isValidEmail(testStr: textfieldLabInchargeEmailID.text!) == false
//        {
//            let alert = UIAlertController(title: "Warning", message: "Please Enter Valid Email ID", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
//        else
//        {
//            textfieldClinicEmailID.resignFirstResponder()
//        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
       
        textfieldFirstname.resignFirstResponder()
        textfieldMiddleName.resignFirstResponder()
        textfieldLastName.resignFirstResponder()
        textfieldPracticeSpeciality.resignFirstResponder()
        textfieldNameofClinic.resignFirstResponder()
        textfieldLabName.resignFirstResponder()
        textfieldVendorName.resignFirstResponder()
       
        return true
    }
    
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textfieldMobileNumber || textField == textfieldClinicMobileNumber || textField == textfieldLabInchargeMobileNumber
        {
            
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 10
            
        }
        if textField == textfieldLandlineNumber
        {

            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 14

        }
        if textField == textfieldFirstname || textField == textfieldVendorName
        {
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
        if textField == textfieldMiddleName
        {
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
        if textField == textfieldLastName
        {
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
        if textField == textfieldLabName
        {
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
     return true
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            
            textviewAddress.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func btnDoctorInfoClicked(_ sender: Any) {
    
        
    }
    
    @IBAction func btnCheckListClicked(_ sender: Any) {
        
        
    }
    
    @IBAction func btnMaleClicked(_ sender: Any) {
        
        self.btnMale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
        self.btnFemale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        gender = "1"
        
    }
    
    @IBAction func btnFemaleClicked(_ sender: Any) {
        
        self.btnMale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        self.btnFemale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
        gender = "2"
        
    }

    @IBAction func btnYesClicked(_ sender: Any) {
        
        self.btnYes.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
        self.btnNo.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        labavailbility = "1"
        LabInfoview.isHidden = false
        //ClinicInfoview.frame.size.height = 860
        currentLocationview.frame.origin.y = 1370
        btnClear.frame.origin.y = 1585
        btnSaveNext.frame.origin.y = 1585

    }
    
    @IBAction func btnNoClicked(_ sender: Any) {
        
        self.btnYes.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        self.btnNo.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
        labavailbility = "0"
        LabInfoview.isHidden = true
        //ClinicInfoview.frame = CGRect(x: 5, y: 490, width: 365, height: 590)
          //ClinicInfoview.frame.size.height = 590
        currentLocationview.frame.origin.y = 1070 //CGRect(x: 5, y: 1070, width: 365, height: 180)
        btnClear.frame.origin.y = 1270 //CGRect(x: 10, y: 1370, width: 170, height: 45)
        btnSaveNext.frame.origin.y = 1270 //CGRect(x: 200, y: 1370, width: 170, height: 45)

    }
    
    @IBAction func btnLabMaleClicked(_ sender: Any) {
        
        self.btnLabMale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
        self.btnLabFemale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        labgender = "1"
    }
    
    @IBAction func btnLabFemaleClicked(_ sender: Any) {
        
        self.btnLabMale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        self.btnLabFemale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
        labgender = "2"
    }
    
    @IBAction func btnRefreshClicked(_ sender: Any) {
        
        if Reachability().isConnectedToNetwork() == true {
            print("Connected to the internet")
            
            checkLocation()
            
        } else {
            print("No internet connection")
            Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please check your internet connection")
        }
        
    }
    
    @IBAction func btnClearClicked(_ sender: Any) {
        
        textfieldFirstname.text = ""
        textfieldMiddleName.text = ""
        textfieldLastName.text = ""
        textfieldMobileNumber.text = ""
        textfieldEmailID.text = ""
        textfieldPracticeSpeciality.text = ""
        textfieldSpeciality.text = ""
        textfieldTypeOfSetup.text = ""
        textfieldNameofClinic.text = ""
        textviewAddress.text = ""
        textfieldClinicMobileNumber.text = ""
        textfieldLandlineNumber.text = ""
        textfieldClinicEmailID.text = ""
        textfieldLabName.text = ""
        textfieldLabInchargeMobileNumber.text = ""
        textfieldLabInchargeEmailID.text = ""
        textfieldVendorName.text = ""
        txtSelectDistrict.text = ""
        txtSelectTaluka.text = ""
        txtSelectCity.text = ""
        
        self.btnLabMale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
        self.btnLabFemale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        
        self.btnMale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
        self.btnFemale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        
    }
    
    @IBAction func btnSaveNextClicked(_ sender: Any) {
        
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DoctorInfoCheckListVC") as! MediProcDoctorInfoCheckListViewController
//        self.present(nextViewController, animated:false, completion:nil)

        if textfieldFirstname.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter first name")
        }
        else if textfieldLastName.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter last name")
        }
        else if textfieldMobileNumber.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter mobile number")
        }
        else if validateMobile(value: textfieldMobileNumber.text!) == false
        {
             Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter valid mobile number")
        }
        else if textfieldEmailID.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter email id")
        }
        else if isValidEmail(testStr: textfieldEmailID.text!) == false
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter valid email id")
        }
        else if textfieldSpeciality.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please select your speciality")
        }
        else if textfieldTypeOfSetup.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please select your type of setup")
        }
        else if textfieldNameofClinic.text == ""
        {
             Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter clinic name")
        }
        else if textviewAddress.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter address")
        }
        else if  txtSelectDistrict.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please select district")
        }
        else if txtSelectTaluka.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please select taluka")
        }
        else if txtSelectCity.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please select city")
        }
        else if textfieldClinicMobileNumber.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter clinic mobile number")
        }
        else if validateMobile(value: textfieldClinicMobileNumber.text!) == false
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter valid clinic mobile number")
        }
        else if labavailbility == "1" && textfieldLabName.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter lab name")
        }
        else if labavailbility == "1" && textfieldLabInchargeMobileNumber.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter mobile number")
        }
        else if labavailbility == "1" && validateMobile(value: textfieldLabInchargeMobileNumber.text!) == false
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter valid mobile number")
        }
        else
        {
            
            var userid = String()
          //userid = UserData().getUserData().UserID
           
            let IsHLLLogin = UserDefaults.standard.bool(forKey: "IsHLLLogin")
            
            if IsHLLLogin == true
            {
                userid = (UserDefaults.standard.value(forKey: "HLLUserID") as? String)!
                print("userid value is",userid)
            }
            else if IsHLLLogin == false
            {
                userid = UserData().getUserData().UserID
            }
            
            var Labjsonarray = [AnyObject]()
            let object = ["CITYCODE":cityID as Any,"ClinicAddress":textviewAddress.text!,"ClinicEmail":textfieldClinicEmailID.text!,"ClinicLandLine":textfieldLandlineNumber.text!,"ClinicMobile":textfieldClinicMobileNumber.text!,"ClinicName":textfieldNameofClinic.text!,"DISTLGDCODE":districtID as Any,"EmailId":textfieldEmailID.text!,"FirstName":textfieldFirstname.text!,"GenderId":gender,"LabAvailability":labavailbility,"LastName":textfieldLastName.text!,"Latitude":currentlatitude,"Logitude":currentlongitude,"MidName":textfieldMiddleName.text!,"MobileNo":textfieldMobileNumber.text!,"Practice_Dr_Speciality_ID":textfieldPracticeSpeciality.text!,"RoleID":"1","SetUpId":setupID,"SpecialityID":specialityID,"TALLGDCODE":talukaID,"UserId":userid,"AddedBy":userid,"SupplierDetails":textfieldVendorName.text!] as [String : Any]
          
            let object1 = ["LabInchargeEmail":textfieldLabInchargeEmailID.text!,"LabInchargeGender":labgender,"LabInchargeMobile":textfieldLabInchargeMobileNumber.text!,"LabInchargeName":textfieldLabName.text!]
            
            Labjsonarray.append(object1 as AnyObject)
            let drlabjson = ["DR_LAB_json":Labjsonarray] as NSDictionary
         
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DoctorInfoCheckListVC") as! MediProcDoctorInfoCheckListViewController
            nextViewController.savedData = object
            
            if labavailbility == "0"
            {
                let json = ["DR_LAB_json":""] as NSDictionary
                nextViewController.drlabjson = json
            }
            else
            {
                nextViewController.drlabjson = drlabjson
            }
           
            self.present(nextViewController, animated:false, completion:nil)
        }
    }
   
    @IBAction func btnBackClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcHomeVC") as! MediProcHomeViewController
        self.present(nextViewController, animated:false, completion:nil)
    }
   
    func isValidEmail(testStr:String) -> Bool {
        
        print("validate emilId: \(testStr)")
          let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    func validateMobile(value: String) -> Bool
    {
        let PHONE_REGEX = "^[6-9][0-9]{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        print("result",result)
        return result
    }
    func validateLandline(value: String) -> Bool
    {
        let PHONE_REGEX = "^[0-9]{14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        print("result of Landline",result)
        return result
    }
    
    func addRightTextButon(viewText:UITextField, imagename: String) {
        viewText.rightViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        let image = UIImage(named: imagename)
        imageView.image = image
        viewText.rightView = imageView
    }
    
}

extension MediProcDoctorInfoViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerDistrict
        {
            return self.districtArray.count
        }
        else if pickerView == pickerTaluka
        {
            return self.talukaArray.count
        }
        else if pickerView == pickerCity
        {
            return self.cityArray.count
        }
        else if pickerView == pickerSpeciality
        {
            return self.specialityArray.count
        }
        else if pickerView == pickerTypeOfSetup
        {
            return self.typeOfSetupArray.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerDistrict
        {
            return self.districtArray[row]["DISTNAME"] as? String
        }
        else if pickerView == pickerTaluka
        {
            return self.talukaArray[row]["TALNAME"] as? String
        }
        else if pickerView == pickerCity
        {
            return self.cityArray[row]["CityName"] as? String
        }
        else if pickerView == pickerSpeciality
        {
            return self.specialityArray[row]["Speciality"] as? String
        }
        else if pickerView == pickerTypeOfSetup
        {
            return self.typeOfSetupArray[row]["Type_SetUp"] as? String
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
         if pickerView == pickerDistrict
        {
            self.txtSelectDistrict.text = self.districtArray[row]["DISTNAME"] as? String
            districtID = self.districtArray[row]["DISTLGDCODE"] as? Int ?? 0
        }
        else if pickerView == pickerTaluka
        {
            self.txtSelectTaluka.text = self.talukaArray[row]["TALNAME"] as? String
            talukaID = self.talukaArray[row]["TALLGDCODE"] as? Int ?? 0
        }
        else if pickerView == pickerCity
        {
            self.txtSelectCity.text = self.cityArray[row]["CityName"] as? String
            cityID = self.cityArray[row]["CityCode"] as? Int ?? 0
        }
        else if pickerView == pickerSpeciality
        {
            self.textfieldSpeciality.text = self.specialityArray[row]["Speciality"] as? String
            specialityID = self.specialityArray[row]["SpecialityID"] as? Int ?? 0
        }
        else if pickerView == pickerTypeOfSetup
        {
            self.textfieldTypeOfSetup.text = self.typeOfSetupArray[row]["Type_SetUp"] as? String
            setupID = self.typeOfSetupArray[row]["SetUpId"] as? Int ?? 0
        }
    }
}

extension MediProcDoctorInfoViewController : ToolbarPickerViewDelegate {
    
    func didTapDone() {
        
        if flag == "District" && districtArray.count > 0{
            let selectedIndex2 = pickerDistrict.selectedRow(inComponent: 0)
            self.txtSelectDistrict.text = self.districtArray[selectedIndex2]["DISTNAME"] as? String
            districtID = self.districtArray[selectedIndex2]["DISTLGDCODE"] as? Int ?? 0
            txtSelectDistrict.resignFirstResponder()
            getTalukaAndCityData(districtId:districtID)
        }
        else if flag == "Taluka" && talukaArray.count > 0{
            let selectedIndex3 = pickerTaluka.selectedRow(inComponent: 0)
            self.txtSelectTaluka.text = self.talukaArray[selectedIndex3]["TALNAME"] as? String
            talukaID = self.talukaArray[selectedIndex3]["TALLGDCODE"] as? Int ?? 0
            txtSelectTaluka.resignFirstResponder()
        }
        else if flag == "City" && cityArray.count > 0{
            let selectedIndex4 = pickerCity.selectedRow(inComponent: 0)
            self.txtSelectCity.text = self.cityArray[selectedIndex4]["CityName"] as? String
            cityID = self.cityArray[selectedIndex4]["CityCode"] as? Int ?? 0
            txtSelectCity.resignFirstResponder()
        }
        else if flag == "Speciality" && specialityArray.count > 0{
            let selectedIndex5 = pickerSpeciality.selectedRow(inComponent: 0)
            self.textfieldSpeciality.text = self.specialityArray[selectedIndex5]["Speciality"] as? String
            specialityID = self.specialityArray[selectedIndex5]["SpecialityID"] as? Int ?? 0
            textfieldSpeciality.resignFirstResponder()
        }
        else if flag == "TypeOfSetup" && typeOfSetupArray.count > 0{
            let selectedIndex6 = pickerTypeOfSetup.selectedRow(inComponent: 0)
            self.textfieldTypeOfSetup.text = self.typeOfSetupArray[selectedIndex6]["Type_SetUp"] as? String
            setupID = self.typeOfSetupArray[selectedIndex6]["SetUpId"] as? Int ?? 0
            textfieldTypeOfSetup.resignFirstResponder()
        }
        else{
            self.textfieldSpeciality.resignFirstResponder()
            self.txtSelectDistrict.resignFirstResponder()
            self.txtSelectTaluka.resignFirstResponder()
            self.txtSelectCity.resignFirstResponder()
            self.textfieldTypeOfSetup.resignFirstResponder()
        }
    }
    func getTalukaAndCityData(districtId:Int){
        
        if Reachability().isConnectedToNetwork() == true {
            print("Connected to the internet")
            
            let taluarray = UserData().getTalukaArrayDataDistrictWise(districtId:("\(districtID)")).globalTalukaArray
            print(taluarray)
            if taluarray.count != 0 {
                self.talukaArray = taluarray as! [[String : AnyObject]]
                pickerTaluka.reloadAllComponents()
            }
            else{
                
                WebserviceCall().getAllTalukaOnDistrict(districtCode: ("\(districtID)"), { Array in
                    self.talukaArray = Array as! [[String : AnyObject]]
                    print(self.talukaArray)
                    self.pickerTaluka.reloadAllComponents()
                })
                
            }
            //////////////////////////////////////////
            let cityarray = UserData().getCityArrayDataDistrictWise(districtId:("\(districtID)")).globalCityArray
            print(cityarray)
            if cityarray.count != 0 {
                self.cityArray = cityarray as! [[String : AnyObject]]
                pickerCity.reloadAllComponents()
            }
            else{
                
                WebserviceCall().getAllCityOnDistrict(districtCode: ("\(districtID)"), { Array in
                    self.cityArray = Array as! [[String : AnyObject]]
                    print(self.cityArray)
                    self.pickerCity.reloadAllComponents()
                })
            }
            //////////////////////////////////////////
            
        }
        else{
            
            let taluarray = UserData().getTalukaArrayDataDistrictWise(districtId:("\(districtID)")).globalTalukaArray
            print(taluarray)
            if taluarray.count != 0 {
                self.talukaArray = taluarray as! [[String : AnyObject]]
                pickerTaluka.reloadAllComponents()
            }
            else{
                Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please Check Your Internet Connection")
            }
            //////////////////////////////////////////
            
            let cityarray = UserData().getCityArrayDataDistrictWise(districtId:("\(districtID)")).globalCityArray
            print(cityarray)
            if cityarray.count != 0 {
                self.cityArray = cityarray as! [[String : AnyObject]]
                self.pickerCity.reloadAllComponents()
            }
            else{
                Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please Check Your Internet Connection")
            }
            
        }
    }
    func didTapCancel() {
        self.textfieldSpeciality.resignFirstResponder()
        self.txtSelectDistrict.resignFirstResponder()
        self.txtSelectTaluka.resignFirstResponder()
        self.txtSelectCity.resignFirstResponder()
        self.textfieldTypeOfSetup.resignFirstResponder()
    }
}

//
//  DesignButton.swift
//  ProjectModel
//
//  Created by ganesh on 12/07/18.
//  Copyright © 2018 ganesh. All rights reserved.
//

import UIKit

@IBDesignable class DesignButton: UIButton {

    //For Button BorderWidth
    @IBInspectable public var borderWidth : CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    //For Button BorderColor
    @IBInspectable public var borderColor : UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    //For Button CornerRadius
    @IBInspectable public var cornerRadius : CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = cornerRadius > 0
        }
    }
    
//    //For Button Alert
    @IBInspectable public var alertButton : Bool  = false {
        didSet {
            if alertButton {
                self.layer.backgroundColor = UIColor.red.cgColor
                self.layer.borderWidth = 3
                self.layer.borderColor = UIColor.black.cgColor
                self.layer.cornerRadius = 10
                self.layer.masksToBounds = true
            }else {
                self.layer.backgroundColor = UIColor.green.cgColor
                self.layer.borderWidth = 3
                self.layer.borderColor = UIColor.black.cgColor
                self.layer.cornerRadius = 10
                self.layer.masksToBounds = true
            }
        }
    }
    
    //For Button TintColor
    @IBInspectable public var fontColor: UIColor = UIColor.white {
        didSet {
            self.tintColor = fontColor
        }
    }
    
    //For Button ShadowOpacity
    @IBInspectable public var shadowOpacity : CGFloat = 0.0 {
        didSet {
            self.layer.shadowOpacity = Float(CGFloat(shadowOpacity))
        }
    }
    
    //For Button ShadowColor
    @IBInspectable public var shadowColor : UIColor = UIColor.clear {
        didSet {
            self.layer.shadowColor = shadowColor.cgColor
        }
    }
    
    //For Button ShadowRadius
    @IBInspectable public var shadowRadius : CGFloat = 0.0 {
        didSet {
            self.layer.shadowRadius = shadowRadius
        }
    }
    
    //For Button ShadowOffset
    @IBInspectable public var shadowOffsetY : CGFloat = 0.0 {
        didSet {
            self.layer.shadowOffset.height = shadowOffsetY
        }
    }
}

//
//  DesignTextField.swift
//  ProjectModel
//
//  Created by ganesh on 12/07/18.
//  Copyright © 2018 ganesh. All rights reserved.
//

import UIKit

class DesignTextField: UITextField {

    //For TextField CornerRadius
    @IBInspectable public var cornerRadius : CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = cornerRadius > 0
        }
    }
    
    //For TextField ShadowOpacity
    @IBInspectable public var shadowOpacity : CGFloat = 0.0 {
        didSet {
            self.layer.shadowOpacity = Float(CGFloat(shadowOpacity))
        }
    }
    
    //For TextField ShadowColor
    @IBInspectable public var shadowColor : UIColor = UIColor.clear {
        didSet {
            self.layer.shadowColor = shadowColor.cgColor
        }
    }
    
    //For TextField ShadowRadius
    @IBInspectable public var shadowRadius : CGFloat = 0.0 {
        didSet {
            self.layer.shadowRadius = shadowRadius
        }
    }
    
    //For TextField ShadowOffset
    @IBInspectable public var shadowOffsetY : CGFloat = 0.0 {
        didSet {
            self.layer.shadowOffset.height = shadowOffsetY
        }
    }
    
    //For TextField BorderWidth
    @IBInspectable public var borderWidth : CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    //For TextField BorderColor
    @IBInspectable public var borderColor : UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
}

//
//  Utils.swift
//  SignPost
//
//  Created by codeworx on 12/5/18.
//  Copyright © 2018 Avante Codeworx. All rights reserved.
//

import UIKit
import Alamofire

class Utils: NSObject {
    
    
    func longNumberToShort(value:Int) -> String{

        if(value == 0) {
            return "0"
        }
        else {
            
            if(value <= 999){
                    return ("\(value)")
            }
            // thousands
            else if(value >= 1000 && value <= 99999){
                    let val = Float(value) / 1000
                    let last = String(format: "%.1f", val)
                    return ("\(last)\(" K")")
            }
            // millions
            else if(value >= 100000 && value <= 9999999){
                    let val = Float(value) / 100000
                    let last = String(format: "%.1f", val)
                    return ("\(last)\(" Lacs")")
            }
            // billions
            else if(value >= 10000000 && value <= 999999999){
                    let val = Float(value) / 10000000
                    let last = String(format: "%.1f", val)
                    return ("\(last)\(" Cr")")
            }
            else{
                    return ("\(value)")
            }
         }
    }
        
    
    func downloadFile(urlString:String, completion: @escaping (NSURL)->())
    {
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            
            let documentsURL:NSURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as NSURL
            
            let fileURL = documentsURL.appendingPathComponent(urlString as String)
            return (fileURL!,[.removePreviousFile, .createIntermediateDirectories])
        }
        
        Alamofire.download(urlString, to: destination).downloadProgress(closure: { (prog) in
            
        }).response { response in
            
            if response.error == nil, let filePath = response.destinationURL?.path {

                let fileURL = URL(fileURLWithPath: filePath)
                completion(fileURL as NSURL)
            }
        }
    }
    
    func callImageUpload(data:Data, filename:String, mimeType:String, params: [String: String], completion: @escaping (NSDictionary)->())
    {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(data, withName:"fileUpload", fileName:filename, mimeType:mimeType)
        
                    for (key, value) in params {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
        
                }, to:"http://advertise.avantecodeworx.co.in/Handler/UploadFile.ashx")
                { (result) in
                    switch result {
                    case .success(let upload, _, _):
        
                        upload.uploadProgress(closure: { (Progress) in
                            print("Upload Progress: \(Progress.fractionCompleted)")
                        })
        
                        upload.responseJSON { response in
                                if let result: Any = response.result.value {
                                print("JSON: \(result)")
                                let JSON = result as! NSDictionary
                                print(JSON)
                                completion(JSON)
                            }
                        }
        
                    case .failure(let encodingError):
                        print(encodingError)
                    }
            }
    }
    
   
    func callUrlSession(urlString:String, method: String, params: [String: String], completion: @escaping (NSDictionary)->() )
    {
        
        Alamofire.request(urlString, method:HTTPMethod(rawValue: method)!,parameters:params)
            .responseJSON { response in
                
                if response.result.isSuccess
                {
                    if let result: Any = response.result.value
                    {
                        print(result)
                        
                        if result is NSArray
                        {
                            let dictionary = ["status":"Success","message":"Data found","output":result]
                            completion(dictionary as NSDictionary)
                        }
                        else if result is NSDictionary
                        {
                            completion(result as! NSDictionary)
                        }
                        else if result is String
                        {
                            let dictionary = ["status":"Success","message":"Data found","output":result]
                            completion(dictionary as NSDictionary)
                        }
                        else
                        {
                            let invalidObject = UserData().getInvalidDataPopup()
                            completion(invalidObject)
                        }
                        
                    }
                    else if response.result.isFailure
                    {
                        let failureResponse = UserData().getRequestTimeoutError()
                        completion(failureResponse)
                    }
                }
        }

    }
    
}

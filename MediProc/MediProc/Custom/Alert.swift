//
//  Alert.swift
//  SignPost
//
//  Created by codeworx on 12/5/18.
//  Copyright © 2018 Avante Codeworx. All rights reserved.
//

import UIKit

class Alert: NSObject {
    
    
    func showAlertMessage(vc: UIViewController, titleStr:String, messageStr:String) -> Void {
        
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: .alert )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    func showAlertMessageWithAction(vc: UIViewController,title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        vc.present(alert, animated: true, completion: nil)
    }
}

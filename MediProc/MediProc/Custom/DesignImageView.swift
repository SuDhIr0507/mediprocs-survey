//
//  DesignImageView.swift
//  ProjectModel
//
//  Created by ganesh on 12/07/18.
//  Copyright © 2018 ganesh. All rights reserved.
//

import UIKit

class DesignImageView: UIImageView {

    //For Image CornerRadius
    @IBInspectable public var cornerRadius : CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = cornerRadius > 0
        }
    }
    
    //For Image ShadowOpacity
    @IBInspectable public var shadowOpacity : CGFloat = 0.0 {
        didSet {
            self.layer.shadowOpacity = Float(CGFloat(shadowOpacity))
        }
    }
    
    //For Image ShadowColor
    @IBInspectable public var shadowColor : UIColor = UIColor.clear {
        didSet {
            self.layer.shadowColor = shadowColor.cgColor
        }
    }
    
    //For Image ShadowRadius
    @IBInspectable public var shadowRadius : CGFloat = 0.0 {
        didSet {
            self.layer.shadowRadius = shadowRadius
        }
    }
    
    //For Image ShadowOffset
    @IBInspectable public var shadowOffsetY : CGFloat = 0.0 {
        didSet {
            self.layer.shadowOffset.height = shadowOffsetY
        }
    }

}

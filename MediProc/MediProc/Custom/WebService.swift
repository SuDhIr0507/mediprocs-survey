//
//  WebService.swift
//  HLLConnect
//
//  Created by Avante on 19/12/18.
//  Copyright © 2018 Avante. All rights reserved.
//

import UIKit

class WebService: NSObject {
 
  //  Live url = "http://design.mediprocs.com/live/MediprocsClient.asmx/"
   // beta url = "http://design.mediprocs.com/beta/MediprocsClient.asmx/"
    
    ///////////// LIVE URL ///////////////////////////
    
  //  static let BASE_URL = "http://design.mediprocs.com/live/MediprocsClient.asmx/"
    
    ///////////// BETA URL ///////////////////////////
    
    static let BASE_URL = "http://design.mediprocs.com/beta/MediprocsClient.asmx/"
    
    
    static let BASE_URL_LIVE = "http://design.mediprocs.com/live/MediprocsClient.asmx/"
    static let BASE_URL_BETA = "http://design.mediprocs.com/beta/MediprocsClient.asmx/"
    static let USER_LOGIN = "UserLogin"
    static let FORGOT_PASSWORD = "http://design.mediprocs.com/live/api/ForgotPassword"
    static let RESET_PASSWORD = "http://design.mediprocs.com/live/api/ResetPassword"
    static let CHANGE_PASSWORD = "http://design.mediprocs.com/live/api/ChangePassword"
    static let REGISTER = "http://design.mediprocs.com/live/api/RegisterClient"
    static let GetAllDistrict = "GetAllDistrict"
    static let GetDoctorSpeciality = "GetDoctorSpeciality"
    static let GetTypeOfSetup = "GetTypeOfSetup"
    static let GetTalukaOnDistrictId = "GetTalukaOnDistrictId"
    static let GetAllCityOnDistrictID = "GetAllCityOnDistrictID"
    static let GetAllPatchOnDistrictID = "GetAllPatchOnDistrictID"
    static let GetAllChecklistSubQuestions = "GetAllChecklistSubQuestions"
    static let GetAllHospitalType = "GetAllHospitalType"
    static let INSERT = "http://design.mediprocs.com/beta/api/RegisterClient"  //beta api
    // static let INSERT = "http://design.mediprocs.com/live/api/RegisterClient" //Live api
    static let SearchSurveyClient = "SearchSurveyClient"
    static let GetAllVendorList = "GetAllVendorList"
    static let GetVendorTrainingMaterial = "GetVendorTrainingMaterial"
    static let Insertuser = "RegisterClient"
    static let USER_LOGIN_HLLCONNECT = "https://erp.hllconnect.in/webservice/HLLCONNECT.asmx/UserLoginApp"
    static let AddPhleboDetails = "http://design.mediprocs.com/beta/api/AddPhleboDetails"
    static let GetWishList = "http://design.mediprocs.com/beta/api/WishListProductList"
    static let ProductList = "http://design.mediprocs.com/beta/api/Product"
    // static let AddPhleboDetails = "http://design.mediprocs.com/live/api/AddPhleboDetails"
    
}

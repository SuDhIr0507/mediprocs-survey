//
//  DesignView.swift
//  ProjectModel
//
//  Created by ganesh on 12/07/18.
//  Copyright © 2018 ganesh. All rights reserved.
//

import UIKit

@IBDesignable class DesignLabel: UILabel {
    
    //For Label BorderWidth
    @IBInspectable public var borderWidth : CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    //For Label BorderColor
    @IBInspectable public var borderColor : UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    //For Label CornerRadius
    @IBInspectable public var cornerRadius : CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = cornerRadius > 0
        }
    }
    
    //For Label ShadowOpacity
    @IBInspectable public var shadowOpacity : CGFloat = 0.0 {
        didSet {
            self.layer.shadowOpacity = Float(CGFloat(shadowOpacity))
        }
    }
    
    //For Label ShadowRadius
    @IBInspectable public var shadowRadius : CGFloat = 0.0 {
        didSet {
            self.layer.shadowRadius = shadowRadius
        }
    }
    
    //For Label ShadowOffset
    @IBInspectable public var shadowOffsetY : CGFloat = 0.0 {
        didSet {
            self.layer.shadowOffset.height = shadowOffsetY
        }
    }
    
    
}

//
//  Color.swift
//  SignPost
//
//  Created by codeworx on 12/18/18.
//  Copyright © 2018 Avante Codeworx. All rights reserved.
//

import UIKit

class Color: NSObject {

    // Color Code //
    
    static let RED_COLOR = Color().hexStringToUIColor(hex:"#D33935")
    static let BLUE_COLOR = Color().hexStringToUIColor(hex:"#3C74F2")
    static let GREEN_COLOR = Color().hexStringToUIColor(hex:"#1C934C")
    static let PINK_COLOR = Color().hexStringToUIColor(hex: "#AC2C62")
    static let GREENSHADE_COLOR = Color().hexStringToUIColor(hex: "#51A54B")
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

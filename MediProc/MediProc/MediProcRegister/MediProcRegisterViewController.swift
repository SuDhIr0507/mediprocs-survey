//
//  MediProcRegisterViewController.swift
//  MediProc
//
//  Created by Avante on 01/03/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class MediProcRegisterViewController: UIViewController,UITextFieldDelegate,NVActivityIndicatorViewable {

 
    @IBOutlet weak var textFieldFirstName: UITextField!
    @IBOutlet weak var textFieldLastName: UITextField!
    @IBOutlet weak var textFieldMobile: UITextField!
    @IBOutlet weak var textFieldEmailId: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var textFieldConfirmPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    
    @IBOutlet weak var Registerbgview: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        Registerbgview.layer.cornerRadius = 20
        btnRegister.layer.cornerRadius = 20
        btnLogin.layer.cornerRadius = 20
        btnLogin.layer.borderWidth = 1.5
        btnLogin.layer.borderColor = Color().hexStringToUIColor(hex: "#1561AD").cgColor
        
        textFieldFirstName.settextfieldborder(color:Color().hexStringToUIColor(hex: "#9d9d9d"))
        textFieldLastName.settextfieldborder(color:Color().hexStringToUIColor(hex: "#9d9d9d"))
        textFieldMobile.settextfieldborder(color:Color().hexStringToUIColor(hex: "#9d9d9d"))
        textFieldEmailId.settextfieldborder(color:Color().hexStringToUIColor(hex: "#9d9d9d"))
        textFieldPassword.settextfieldborder(color:Color().hexStringToUIColor(hex: "#9d9d9d"))
        textFieldConfirmPassword.settextfieldborder(color:Color().hexStringToUIColor(hex: "#9d9d9d"))
        
        textFieldFirstName.settextfieldLeftviewmode(image:#imageLiteral(resourceName: "user1.png"))
        textFieldLastName.settextfieldLeftviewmode(image:#imageLiteral(resourceName: "user1.png"))
        textFieldEmailId.settextfieldLeftviewmode(image:#imageLiteral(resourceName: "mail1.png"))
        textFieldMobile.settextfieldLeftviewmode(image:#imageLiteral(resourceName: "mobile1.png"))
        textFieldPassword.settextfieldLeftviewmode(image:#imageLiteral(resourceName: "password1.png"))
        textFieldConfirmPassword.settextfieldLeftviewmode(image:#imageLiteral(resourceName: "password1.png"))
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == textFieldFirstName  {
            textFieldFirstName.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AC"))
        }
        if textField==textFieldLastName {
            textFieldLastName.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AC"))
        }
        if textField == textFieldMobile  {
            textFieldMobile.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AC"))
        }
        if textField == textFieldEmailId  {
            textFieldEmailId.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AC"))
        }
        if textField == textFieldPassword  {
            textFieldPassword.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AC"))
        }
        if textField == textFieldConfirmPassword  {
            textFieldConfirmPassword.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AC"))
        }
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == textFieldFirstName  {
            textFieldFirstName.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AC"))
        }
        if textField==textFieldLastName {
            textFieldLastName.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AC"))
        }
        if textField == textFieldMobile  {
            textFieldMobile.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AC"))
        }
        if textField == textFieldEmailId  {
            textFieldEmailId.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AC"))
        }
        if textField == textFieldPassword  {
            textFieldPassword.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AC"))
        }
        if textField == textFieldConfirmPassword  {
            textFieldConfirmPassword.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AC"))
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        textFieldFirstName.resignFirstResponder()
        textFieldLastName.resignFirstResponder()
        textFieldMobile.resignFirstResponder()
        textFieldEmailId.resignFirstResponder()
        textFieldPassword.resignFirstResponder()
        textFieldConfirmPassword.resignFirstResponder()
        
        return true
    }
    @IBAction func btnLoginClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcLoginVC") as! MediProcLoginViewController
        self.present(nextViewController, animated:false, completion:nil)
        
        
    }
    
    @IBAction func btnRegisterClicked(_ sender: Any) {
        
    }
    
    
    func RegisterApi()
    {
        startAnimating()
        
        let urlString = WebService.REGISTER
        let jsonObject:[String:String] = ["":""]
        
        Utils().callUrlSession(urlString: urlString, method: "POST", params:jsonObject , completion: { (response) in
            print(response)
            let status = response.value(forKey: "status") as! String
            let message = response.value(forKey: "message") as! String
            
            if status == "Success"
            {
                
                
                
                DispatchQueue.main.async
                    {
                        self.stopAnimating()
                      
                }
                
            }
            else if status == "Fail"
            {
                Alert().showAlertMessage(vc:self, titleStr: "Error", messageStr: message)
            }
        })
    }
}

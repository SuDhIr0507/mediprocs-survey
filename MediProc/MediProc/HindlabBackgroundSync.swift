//
//  HindlabBackgroundSync.swift
//  Hindlab
//
//  Created by codeworx on 6/13/18.
//  Copyright © 2018 Avante Codeworx. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class HindlabBackgroundSync: NSObject{

    
    var coreDictionary = NSDictionary()
    var timer = Timer()
    var checkInternetConnection = Timer()
    var surveyId = String()
    var surveyBackId = String()
    var coreDataObjectId = String()
    var latitude = Double()
    var longitude = Double()
    var cartArray = [AnyObject]()
    
    func callBackgroundSync()
    {
        
      checkInternetConnection = Timer.scheduledTimer(timeInterval:15, target: self, selector: #selector(checkInternetConnectionMethod), userInfo: nil, repeats: true)
        
    }

    @objc func checkInternetConnectionMethod()
    {
       
         if Reachability().isConnectedToNetwork() == true {

            callbgApiTimer()
            checkInternetConnection.invalidate()
        }
        
    }
    
    func callbgApiTimer(){
        
         timer = Timer.scheduledTimer(timeInterval:8, target: self, selector: #selector(checkSyncDataAndCallBgApi), userInfo: nil, repeats: true)
    }
    
    @objc func checkSyncDataAndCallBgApi()
    {
        let moContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Registration")
        
        do {
            
            let result = try moContext.fetch(request) as! [Registration]
            
            if result.count != 0 {
                
                for dict in result
                {
                    print(dict)
                    let data2 = (dict as AnyObject).value(forKey: "jsonObject") as! NSData
                    let unarchiveObject2 = NSKeyedUnarchiver.unarchiveObject(with: data2 as Data)
                    let jsonObject = unarchiveObject2 as! [String: Any]
                    let Id = (dict as AnyObject).value(forKey: "id") as! String
                    print(jsonObject)
                    
                    let urlString = WebService.INSERT
                    
                    Alamofire.request(urlString, method:.post,parameters:jsonObject)
                        .responseJSON { response in
                            
                            print("JSON",response)
                            
                            if response.result.isSuccess
                            {
                                if let result: Any = response.result.value
                                {
                                    print(result)
                                    
                                    if result as! String == "Client info saved successfully"{
                                        
                                        self.deleteFormLocalDatabase(id:Id)
                                    }
                                    else{
                                        
                                        let moContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                                        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Pending")
                                        
                                        do {
                                            
                                            let resultPending = try moContext.fetch(request) as! [Pending]
                                            
                                            if resultPending.count != 0 {
                                                
                                                self.cartArray = resultPending
                                            }
                                            
                                        } catch {
                                            print("Error")
                                        }
                                        
                                        
                                        if self.cartArray.count < 20
                                        {
                                            
                                            let storeDescription = NSEntityDescription.entity(forEntityName: "Pending", in:moContext)
                                            
                                            let pending = Pending(entity:storeDescription!,insertInto:moContext)
                                            
                                            let data = NSKeyedArchiver.archivedData(withRootObject:jsonObject)
                                            
                                            pending.setValue(data, forKey: "jsonObject")
                                            let uuid = UUID().uuidString
                                            print(uuid)
                                            pending.id = uuid
                                            pending.error = result as? String
                                            
                                            //let error: NSError?
                                            
                                            do {
                                                try moContext.save()
                                                
                                                print("Data Save Pending Successfully")
                                                self.deleteFormLocalDatabase(id:Id)
                                                
                                            } catch let error {
                                                print("Could not cache the response ")
                                            }
                                        }
                                        else
                                        {
                                            self.deleteFormLocalDatabase(id:Id)
//                                          Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "You can not register user more than 20")
                                        }
                                        
                                    }
                                    
                                }
                                else if response.result.isFailure
                                {
                                }
                                
                            }
                       }
                }
            }
            else
            {
                timer.invalidate()
            }
            
        } catch {
            print("Error")
        }
    
    }

    func deleteFormLocalDatabase(id:String)
    {
        let moContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Registration")
        do {
            
            let result = try moContext.fetch(request) as! [Registration]
        
            for dict in result {
            
                let deleteId = dict.value(forKey: "id") as! String
                print(id)
            
                if deleteId == id
                {
                   moContext.delete(dict)
                }
            }
        
           do {
            
                try moContext.save()
                print("saved!")
            
              } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
              } catch {
            
        }
        }
        catch {
            print("Error")
        }
        
    }
    
}

//
//  MediProcSearchClientsLabViewController.swift
//  MediProc
//
//  Created by Avante on 25/04/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class MediProcSearchClientsLabViewController: UIViewController,UITextViewDelegate,UITextFieldDelegate {

    @IBOutlet weak var textfieldLabName: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldLandlineNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldEmailId: SkyFloatingLabelTextField!
    @IBOutlet weak var textviewAddress: UITextView!
    @IBOutlet weak var textfieldDistrict: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldTaluka: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldCity: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldPatch: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldAvgPatientPerday: SkyFloatingLabelTextField!
    //@IBOutlet weak var textfieldTestOutsource: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldFirstname: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldMiddlename: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldLastname: SkyFloatingLabelTextField!
    @IBOutlet weak var btnmale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var textfieldOwnerMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldOwnerEmailId: SkyFloatingLabelTextField!
    var dict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        textviewAddress.layer.borderWidth = 0.5
        textviewAddress.layer.borderColor = UIColor.lightGray.cgColor
        print("dict data is ",dict)
        textfieldLabName.text = dict.value(forKey:"LabName") as? String
        textfieldCity.text = dict.value(forKey:"CityName") as? String
        textfieldTaluka.text = dict.value(forKey:"TALNAME") as? String
        textfieldDistrict.text = dict.value(forKey:"DistrictName") as? String
       // textfieldPatch.text = dict.value(forKey: "PatchName") as? String
        let landline = dict.value(forKey:"LabLandLine") as? NSNumber
        textfieldLandlineNumber.text = "\(landline)"
        textfieldMobileNumber.text = dict.value(forKey:"LabMobile") as? String
        textfieldEmailId.text = dict.value(forKey:"LabEmail") as? String
        textviewAddress.text = dict.value(forKey:"LabAddress") as? String
        let AvgPatientsPerDay = dict.value(forKey:"AvgPatientsPerDay") as? NSNumber
        textfieldAvgPatientPerday.text = "\(AvgPatientsPerDay ?? 0)"
      //  let AvgTestsPerDay = dict.value(forKey: "AvgTestsPerDay") as? NSNumber
       // textfieldTestOutsource.text = "\(AvgTestsPerDay ?? 0)"
        textfieldOwnerMobileNumber.text = dict.value(forKey:"MobileNo") as? String
        textfieldOwnerEmailId.text = dict.value(forKey:"EmailId") as? String
        textfieldFirstname.text = dict.value(forKey:"FirstName") as? String
        textfieldMiddlename.text = dict.value(forKey:"MidName") as? String
        textfieldLastname.text = dict.value(forKey:"LastName") as? String
        let gender = dict.value(forKey: "GenderId") as? Int
        
        if gender == 1
        {
            self.btnmale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
            self.btnFemale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        }
        else
        {
            self.btnmale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
            self.btnFemale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func btnOKClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcSearchClientsVC") as! MediProcSearchClientsViewController
        self.present(nextViewController, animated:false, completion:nil)
        
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcSearchClientsVC") as! MediProcSearchClientsViewController
        self.present(nextViewController, animated:false, completion:nil)
    }
}

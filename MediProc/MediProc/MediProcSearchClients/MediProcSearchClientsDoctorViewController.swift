//
//  MediProcSearchClientsDoctorViewController.swift
//  MediProc
//
//  Created by Avante on 25/04/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class MediProcSearchClientsDoctorViewController: UIViewController {

    @IBOutlet weak var personalinfoview: UIView!
    @IBOutlet weak var textfieldFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldMiddleName: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var btnmale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var textfieldMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldEmailId: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldPracticeSpeciality: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldSpeciality: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldTypeOfSetup: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldnameOfClinic: SkyFloatingLabelTextField!
    @IBOutlet weak var textviewAddress: UITextView!
    @IBOutlet weak var textfieldDistrict: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldTaluka: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldCity: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldPatch: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldClinicMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldLandlinenumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldClinicEmailId: SkyFloatingLabelTextField!
    
    var dict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        textviewAddress.layer.borderWidth = 0.5
        textviewAddress.layer.borderColor = UIColor.lightGray.cgColor
        print("dict data is ",dict)
        textfieldFirstName.text = dict.value(forKey: "FirstName") as? String
        textfieldMiddleName.text = dict.value(forKey: "MidName") as? String
        textfieldLastName.text = dict.value(forKey: "LastName") as? String
        let gender = dict.value(forKey: "GenderId") as? Int
        
        if gender == 1
        {
            self.btnmale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
            self.btnFemale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        }
        else
        {
            self.btnmale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
            self.btnFemale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
        }
        
        textfieldMobileNumber.text = dict.value(forKey: "MobileNo") as? String
        textfieldEmailId.text = dict.value(forKey: "EmailId") as? String
        textfieldPracticeSpeciality.text = dict.value(forKey: "Practice_Dr_Speciality") as? String
        textfieldSpeciality.text = dict.value(forKey: "Speciality") as? String
        textfieldTypeOfSetup.text = dict.value(forKey: "Type_SetUp") as? String
        textfieldnameOfClinic.text = dict.value(forKey: "ClinicName") as? String
        textfieldDistrict.text = dict.value(forKey: "DistrictName") as? String
        textfieldTaluka.text = dict.value(forKey: "TALNAME") as? String
        textfieldCity.text = dict.value(forKey: "CityName") as? String
        //textfieldPatch.text = dict.value(forKey: "PatchName") as? String
        textfieldClinicMobileNumber.text = dict.value(forKey: "ClinicMobile") as? String
        textfieldLandlinenumber.text = dict.value(forKey: "ClinicLandLine") as? String
        textfieldClinicEmailId.text = dict.value(forKey: "ClinicEmail") as? String
        textviewAddress.text = dict.value(forKey: "ClinicAddress")as? String
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnmaleClicked(_ sender: Any) {
        
    }
    
    @IBAction func btnFemaleClicked(_ sender: Any) {
        
    }
    
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcSearchClientsVC") as! MediProcSearchClientsViewController
        self.present(nextViewController, animated:false, completion:nil)
        
    }
    
    @IBAction func btnOKClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcSearchClientsVC") as! MediProcSearchClientsViewController
        self.present(nextViewController, animated:false, completion:nil)
        
    }
    
}

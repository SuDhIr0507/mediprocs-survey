 //
//  MediProcSearchClientsTableViewCell.swift
//  MediProc
//
//  Created by Avante on 24/04/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit

class MediProcSearchClientsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var bgview: UIView!
    @IBOutlet weak var lblClientName: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var lblClienttype: UILabel!
    @IBOutlet weak var lblFacilityName: UILabel!
    @IBOutlet weak var lblFacilityNameValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  MediProcSearchClientsViewController.swift
//  MediProc
//
//  Created by Avante on 24/04/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class MediProcSearchClientsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,NVActivityIndicatorViewable {

    
    @IBOutlet weak var Navview: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tableviewClient: UITableView!
    var ClientArray = [AnyObject]()
    var searchArray = [AnyObject]()
    var searchActive : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        tableviewClient.delegate = self
        tableviewClient.dataSource = self
        
        let bottomBorder = CALayer()
        
        bottomBorder.frame = CGRect(x:0, y: 64, width: Navview.frame.size.width, height:0.7)
        
        bottomBorder.backgroundColor = Color().hexStringToUIColor(hex: "#0060C5").cgColor  //UIColor(white: 0.8, alpha: 1.0).cgColor
        
        Navview.layer.addSublayer(bottomBorder)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability().isConnectedToNetwork() == true {
            print("Connected to the internet")
            
            SearchSurveyClient()
            
        } else {
            print("No internet connection")
            Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please Check Your Internet Connection")
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        //searchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        if searchBar.text == "" {
            searchActive = false
        }
        else{
            searchActive = true
            
        }
       
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchActive = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchActive = false
        searchBar.text = ""
        searchBar.endEditing(true)
     
        self.tableviewClient.reloadData()
    }
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        return ((searchBar.text?.count ?? 0) + text.count - Int(range.length) > 25) ? false : true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchArray.removeAll()
        
        if searchText.count != 0 {
            searchActive = true
            
            searchResult()
            
        } else {
            searchActive = false
        }
        
        tableviewClient.reloadData()
    }
    
    func searchResult()
    {
        let string = searchBar.text!
        
        let searchPredicate = NSPredicate(format: "(FirstName CONTAINS[cd] %@) OR (MobileNo CONTAINS[cd] %@) OR (ClinicName CONTAINS[cd] %@) OR (HospitalName CONTAINS[cd] %@) OR (LabName CONTAINS[cd] %@) OR (PharmacyName CONTAINS[cd] %@)",string,string,string,string,string,string)
        let array = (self.ClientArray as NSArray).filtered(using: searchPredicate)
        searchArray = array as [AnyObject]
        print("searchArray Data is",searchArray)
      // tableviewClient.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if searchActive == true{

            return searchArray.count
        }
        
        return self.ClientArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MediProcSearchClientsTableViewCell", for: indexPath) as! MediProcSearchClientsTableViewCell
        cell.bgview.layer.borderWidth = 0.5
        cell.bgview.layer.borderColor = UIColor.lightGray.cgColor
        
        if searchActive == true
        {
            var firstname = String()
            firstname = self.searchArray[indexPath.row]["FirstName"] as? String ?? ""
            var Midname = String()
            Midname = self.searchArray[indexPath.row]["MidName"] as? String ?? ""
            var lastname = String()
            lastname = self.searchArray[indexPath.row]["LastName"] as? String ?? ""
          
             cell.lblClientName.text = firstname  + Midname + lastname
             let mobno = self.searchArray[indexPath.row]["MobileNo"] as? String
             cell.lblMobileNo.text = self.searchArray[indexPath.row]["MobileNo"] as? String    //"\(mobno )"
        
             let RoleID = self.searchArray[indexPath.row]["RoleID"] as? Int
            
             if RoleID == 1
             {
                cell.lblClienttype.text = "Doctor"
              //cell.lblFacilityNameValue.isHidden = true
                //cell.lblFacilityName.isHidden = true
                cell.lblFacilityNameValue.text = self.searchArray[indexPath.row]["ClinicName"] as? String
             }
             else if RoleID == 2
             {
                cell.lblClienttype.text = "Hospital"
//                cell.lblFacilityNameValue.isHidden = false
//                cell.lblFacilityName.isHidden = false
                cell.lblFacilityNameValue.text = self.searchArray[indexPath.row]["HospitalName"] as? String
                
             }
             else if RoleID == 3
             {
                cell.lblClienttype.text = "Lab"
//                cell.lblFacilityNameValue.isHidden = false
//                cell.lblFacilityName.isHidden = false
                cell.lblFacilityNameValue.text = self.searchArray[indexPath.row]["LabName"] as? String
             }
             else if RoleID == 4
             {
                cell.lblClienttype.text = "Pharmacy"
//                cell.lblFacilityNameValue.isHidden = false
//                cell.lblFacilityName.isHidden = false
                cell.lblFacilityNameValue.text = self.searchArray[indexPath.row]["PharmacyName"] as? String
             }

            

        }
       else
       {
        var firstname = String()
        firstname = self.ClientArray[indexPath.row]["FirstName"] as? String ?? ""
        var Midname = String()
        Midname = self.ClientArray[indexPath.row]["MidName"] as? String ?? ""
        var lastname = String()
        lastname = self.ClientArray[indexPath.row]["LastName"] as? String ?? ""
        
        cell.lblClientName.text = firstname  + Midname + lastname
        let mobno = self.ClientArray[indexPath.row]["MobileNo"] as? String
        cell.lblMobileNo.text = self.ClientArray[indexPath.row]["MobileNo"] as? String    //"\(mobno )"
        
        let RoleID = self.ClientArray[indexPath.row]["RoleID"] as? Int
        
        if RoleID == 1
        {
            cell.lblClienttype.text = "Doctor"
//            cell.lblFacilityNameValue.isHidden = true
//            cell.lblFacilityName.isHidden = true
            cell.lblFacilityNameValue.text = self.ClientArray[indexPath.row]["ClinicName"] as? String
        }
        else if RoleID == 2
        {
            cell.lblClienttype.text = "Hospital"
//            cell.lblFacilityNameValue.isHidden = false
//            cell.lblFacilityName.isHidden = false
            cell.lblFacilityNameValue.text = self.ClientArray[indexPath.row]["HospitalName"] as? String
        }
        else if RoleID == 3
        {
            cell.lblClienttype.text = "Lab"
//            cell.lblFacilityNameValue.isHidden = false
//            cell.lblFacilityName.isHidden = false
            cell.lblFacilityNameValue.text = self.ClientArray[indexPath.row]["LabName"] as? String
        }
        else if RoleID == 4
        {
            cell.lblClienttype.text = "Pharmacy"
//            cell.lblFacilityNameValue.isHidden = false
//            cell.lblFacilityName.isHidden = false
            cell.lblFacilityNameValue.text = self.ClientArray[indexPath.row]["PharmacyName"] as? String
        }

       }
    
        cell.selectionStyle = .none
        tableviewClient.separatorColor = UIColor.clear
        tableviewClient.tableFooterView = UIView()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if searchActive == true
        {
            let type = searchArray[indexPath.row]["RoleID"] as? Int
            
            if type == 1
            {
                let dictionary = searchArray[indexPath.row] as? NSDictionary
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SearchClientsDoctorVC") as! MediProcSearchClientsDoctorViewController
                nextViewController.dict = dictionary!
                self.present(nextViewController, animated:false, completion:nil)
            }
                
            else if type == 2
            {
                let dictionary = searchArray[indexPath.row] as? NSDictionary
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SearchClientsHospitalVC") as! MediProcSearchClientsHospitalViewController
                nextViewController.dict = dictionary!
                self.present(nextViewController, animated:false, completion:nil)
            }
            else if type == 3
            {
                let dictionary = searchArray[indexPath.row] as? NSDictionary
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SearchClientsLabVC") as! MediProcSearchClientsLabViewController
                nextViewController.dict = dictionary!
                self.present(nextViewController, animated:false, completion:nil)
            }
            else if type == 4
            {
                let dictionary = searchArray[indexPath.row] as? NSDictionary
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SearchClientsPharmacyVC") as! MediProcSearchClientsPharmacyViewController
                nextViewController.dict = dictionary!
                self.present(nextViewController, animated:false, completion:nil)
            }
        }
        else
        {
            let type = ClientArray[indexPath.row]["RoleID"] as? Int
            
            if type == 1
            {
                let dictionary = ClientArray[indexPath.row] as? NSDictionary
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SearchClientsDoctorVC") as! MediProcSearchClientsDoctorViewController
                nextViewController.dict = dictionary!
                self.present(nextViewController, animated:false, completion:nil)
            }
                
            else if type == 2
            {
                let dictionary = ClientArray[indexPath.row] as? NSDictionary
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SearchClientsHospitalVC") as! MediProcSearchClientsHospitalViewController
                nextViewController.dict = dictionary!
                self.present(nextViewController, animated:false, completion:nil)
            }
            else if type == 3
            {
                let dictionary = ClientArray[indexPath.row] as? NSDictionary
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SearchClientsLabVC") as! MediProcSearchClientsLabViewController
                nextViewController.dict = dictionary!
                self.present(nextViewController, animated:false, completion:nil)
            }
            else if type == 4
            {
                let dictionary = ClientArray[indexPath.row] as? NSDictionary
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SearchClientsPharmacyVC") as! MediProcSearchClientsPharmacyViewController
                nextViewController.dict = dictionary!
                self.present(nextViewController, animated:false, completion:nil)
            }
        }
       //  let selectedcell = tableviewClient.cellForRow(at: indexPath)
 
    }

    @IBAction func btnBackClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcHomeVC") as! MediProcHomeViewController
        self.present(nextViewController, animated:false, completion:nil)
    }
    
    func SearchSurveyClient()
    {
        let urlString = WebService.BASE_URL + WebService.SearchSurveyClient
        //searchBar.text!
        
        let jsonObject:[String:String] = ["SearchText":""]
        print("search text is",jsonObject)
        startAnimating()
        Utils().callUrlSession(urlString: urlString, method: "POST", params:jsonObject , completion: { (response) in
            print(response)
            let status = response.value(forKey: "status") as! String
            let message = response.value(forKey: "message") as! String
            
            if status == "Success"
            {
               self.ClientArray = (((response.value(forKey: "output") as AnyObject) ) as! [AnyObject])
                print("ClientArray data is ",self.ClientArray)
              
                DispatchQueue.main.async {
                    
                    self.tableviewClient.reloadData()
                    self.stopAnimating()
                }
                
            }
            else if status == "Fail"
            {
                  Alert().showAlertMessage(vc:self, titleStr: "status", messageStr: message)
            }
        })
    }

}

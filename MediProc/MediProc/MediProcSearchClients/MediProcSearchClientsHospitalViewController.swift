//
//  MediProcSearchClientsHospitalViewController.swift
//  MediProc
//
//  Created by Avante on 25/04/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class MediProcSearchClientsHospitalViewController: UIViewController {

    
    @IBOutlet weak var HospitalInfoview: UIView!
    @IBOutlet weak var hospitalAdminInfoview: UIView!
    
    @IBOutlet weak var textfieldHospitalname: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldTypeofhospital: SkyFloatingLabelTextField!
    @IBOutlet weak var textviewAddress: UITextView!
    @IBOutlet weak var textfieldDistrict: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldTaluka: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldCity: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldPatch: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldLandlineNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldEmailID: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldNoOfBeds: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldAvgNoOfPatient: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldAvgNoOfTestOutsource: SkyFloatingLabelTextField!
    
    @IBOutlet weak var textfieldAdminFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldAdminMiddleName: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldAdminLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var textfieldAdminMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldAdminEmailId: SkyFloatingLabelTextField!
    
    var dict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        textviewAddress.layer.borderWidth = 0.5
        textviewAddress.layer.borderColor = UIColor.lightGray.cgColor
        print("Dicccccc",dict)
        textfieldHospitalname.text = dict.value(forKey: "HospitalName") as? String
        textfieldTypeofhospital.text = dict.value(forKey: "HospitalType") as? String
        textviewAddress.text = dict.value(forKey: "HospitalAddress") as? String
        textfieldDistrict.text = dict.value(forKey: "DistrictName") as? String
        textfieldTaluka.text = dict.value(forKey: "TALNAME") as? String
        textfieldCity.text = dict.value(forKey: "CityName") as? String
       // textfieldPatch.text = dict.value(forKey: "PatchName") as? String
        let landline = dict.value(forKey: "HospitalLandLine") as? NSNumber
        textfieldLandlineNumber.text = "\(landline)"
        textfieldNoOfBeds.text = dict.value(forKey: "BedsCount") as? String
        let patient = dict.value(forKey: "AvgPatientsPerDay") as? NSNumber
        textfieldAvgNoOfPatient.text = "\(patient ?? 0)"
        let outsource = dict.value(forKey: "AvgTestsPerDay") as? NSNumber
        textfieldAvgNoOfTestOutsource.text = "\(outsource ?? 0)"
        textfieldAdminFirstName.text = dict.value(forKey: "FirstName") as? String
        textfieldAdminMiddleName.text = dict.value(forKey: "MidName") as? String
        textfieldAdminLastName.text = dict.value(forKey: "LastName") as? String
        textfieldMobileNumber.text = dict.value(forKey: "MobileNo") as? String
        textfieldEmailID.text = dict.value(forKey: "EmailId") as? String
        textfieldAdminEmailId.text = dict.value(forKey: "HospitalEmail") as? String
        textfieldAdminMobileNumber.text = dict.value(forKey: "HospitalMobile") as? String
        
        let gender = dict.value(forKey: "GenderId") as? Int
        
        if gender == 1
        {
            self.btnMale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
            self.btnFemale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        }
        else
        {
            self.btnMale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
            self.btnFemale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
        }
        
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    @IBAction func btnBackClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcSearchClientsVC") as! MediProcSearchClientsViewController
        self.present(nextViewController, animated:false, completion:nil)
        
    }
    
    @IBAction func btnOKClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcSearchClientsVC") as! MediProcSearchClientsViewController
        self.present(nextViewController, animated:false, completion:nil)
        
    }
}

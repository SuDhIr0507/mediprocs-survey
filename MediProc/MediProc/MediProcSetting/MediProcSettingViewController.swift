//
//  MediProcSettingViewController.swift
//  MediProc
//
//  Created by Avante on 28/08/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit

class MediProcSettingViewController: UIViewController,CustomPopUpChangePaaswordDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func btnChangePasswordClicked(_ sender: Any) {
        
        let popvc = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "CustomPopupChangePaaswordVC") as! CustomPopUpChangePaaswordViewController
        self.addChild(popvc)
        popvc.view.frame = self.view.frame
        self.view.addSubview(popvc.view)
        popvc.didMove(toParent: self)
        popvc.delegate = self
        
    }
    
    @IBAction func btnPendingSurveyListClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "pendingSurvey") as! MediprocsPendingSurveySyncViewController
        self.present(nextViewController, animated:false, completion:nil)
        
    }
    
    func didTapFilter(_ sender: CustomPopUpChangePaaswordViewController, Flag: String) {
        
    }
    
    @IBAction func btnLogoutClicked(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: "Are you sure you want to Logout?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertAction.Style.default, handler: { action in
            
            
            UserDefaults.standard.set("logout", forKey: "status")
            UserDefaults.standard.set("",forKey: "UserID")
            UserDefaults.standard.set("",forKey: "HLLUserID")
            UserDefaults.standard.set(false, forKey: "IsHLLLogin")
            
            
            UserData().ClearUserData()
            HLLConnectUserData().ClearHLLConnectUserData()
            self.callLogOut()
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func callLogOut()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "MediProcLoginVC") as! MediProcLoginViewController
        self.present(nextViewController, animated:false, completion:nil)
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcHomeVC") as! MediProcHomeViewController
        self.present(nextViewController, animated:false, completion:nil)
        
    }
}

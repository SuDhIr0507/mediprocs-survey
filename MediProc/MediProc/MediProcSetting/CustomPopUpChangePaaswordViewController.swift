//
//  CustomPopUpChangePaaswordViewController.swift
//  MediProc
//
//  Created by Avante on 28/08/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

protocol CustomPopUpChangePaaswordDelegate : class {
    func didTapFilter(_ sender:CustomPopUpChangePaaswordViewController, Flag:String)
    
}

class CustomPopUpChangePaaswordViewController: UIViewController,NVActivityIndicatorViewable,UITextFieldDelegate {
    weak var delegate: CustomPopUpChangePaaswordDelegate?
    @IBOutlet weak var bgview: UIView!
    @IBOutlet weak var ResetPasswordview: UIView!
    @IBOutlet weak var textfieldNewPassword: UITextField!
    @IBOutlet weak var textfieldConfirmPassword: UITextField!
    
    @IBOutlet weak var textfieldOldPassword: UITextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnChangePassword: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
        bgview?.backgroundColor = UIColor.black.withAlphaComponent(0.5)  //UIColor(white: 1, alpha: 0.6)
        
        textfieldOldPassword.layer.borderWidth = 0.5
        textfieldOldPassword.layer.borderColor = UIColor.lightGray.cgColor
        textfieldNewPassword.layer.borderWidth = 0.5
        textfieldNewPassword.layer.borderColor = UIColor.lightGray.cgColor
        textfieldConfirmPassword.layer.borderWidth = 0.5
        textfieldConfirmPassword.layer.borderColor = UIColor.lightGray.cgColor
        
        btnChangePassword.layer.cornerRadius = 10
        btnCancel.layer.cornerRadius = 10
    
        ResetPasswordview.layer.shadowColor = UIColor.black.cgColor
        ResetPasswordview.layer.shadowOpacity = 1
        ResetPasswordview.layer.shadowOffset = CGSize.zero
        ResetPasswordview.layer.shadowRadius = 3
        ResetPasswordview.layer.cornerRadius = 10
        
        ResetPasswordview.isHidden = false
       
        
        showAnimate()
    }
    

    @IBAction func btnChangePasswordClicked(_ sender: Any) {
        if Reachability().isConnectedToNetwork() {
            print("Yes! internet is available.")
            
            self.callChangePasswordApi()
        }
        else
        {
            let alert = UIAlertController(title: "Error", message: "Please check your internet connection", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        
        self.ResetPasswordview.isHidden = true
        removeAnimate()
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textfieldConfirmPassword
        {
            if self.isPasswordSame(password: textfieldNewPassword.text!, confirmPassword: textfieldConfirmPassword.text!)
            {
                textfieldConfirmPassword.resignFirstResponder()
            }
            else
            {
                let errorAlert = UIAlertController(title: "Error", message: "Please enter correct password", preferredStyle: .alert)
                errorAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(errorAlert, animated: true, completion: nil)
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        textfieldOldPassword.resignFirstResponder()
        textfieldNewPassword.resignFirstResponder()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textfieldOldPassword
        {
            let maxLength = 16
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
        if textField == textfieldNewPassword
        {
            let maxLength = 16
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
        if textField == textfieldConfirmPassword
        {
            let maxLength = 16
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
       
        return false
        
    }
    
    func callChangePasswordApi()
    {
        if textfieldOldPassword.text == ""
        {
            let errorAlert = UIAlertController(title: "Error", message: "Please Enter Old Password", preferredStyle: .alert)
            errorAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(errorAlert, animated: true, completion: nil)
        }
        else if textfieldNewPassword.text == ""
        {
            let errorAlert = UIAlertController(title: "Error", message: "Please Enter New Password", preferredStyle: .alert)
            errorAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(errorAlert, animated: true, completion: nil)
        }
        else if textfieldConfirmPassword.text == ""
        {
            let errorAlert = UIAlertController(title: "Error", message: "Please Enter Confirm Password", preferredStyle: .alert)
            errorAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(errorAlert, animated: true, completion: nil)
        }
        else
        {
        startAnimating()
        let urlString = WebService.CHANGE_PASSWORD
        
        var userid = String()
        let IsHLLLogin = UserDefaults.standard.bool(forKey: "IsHLLLogin")
        
        if IsHLLLogin == true
        {
            userid = (UserDefaults.standard.value(forKey: "HLLUserID") as? String)!
            print("userid value is",userid)
        }
        else if IsHLLLogin == false
        {
            userid = UserData().getUserData().UserID
        }
        
        let jsonObject:[String:String] = ["UserId":userid,"OldPassword":textfieldOldPassword.text!,"NewPassword":textfieldNewPassword.text!]
        print("params are",jsonObject)
        Alamofire.request(urlString, method: .post, parameters:jsonObject)
            .responseJSON{ response in
                self.stopAnimating()
                print(response)
                let result: Any = response.result.value
                if result is String
                {
                
                    Alert().showAlertMessageWithAction(vc: self, title: "Status", message: result as? String, actionTitles:["OK"], actions:[{ action1 in
                        self.stopAnimating()
                        self.ResetPasswordview.isHidden = true
                        self.removeAnimate()
                        
                        UserDefaults.standard.set("logout", forKey: "status")
                        UserDefaults.standard.set("",forKey: "UserID")
                        UserDefaults.standard.set("",forKey: "HLLUserID")
                        UserDefaults.standard.set(false, forKey: "IsHLLLogin")
                        
                        
                        UserData().ClearUserData()
                        HLLConnectUserData().ClearHLLConnectUserData()
                        self.callLogOut()
                        
                        }])
                    
                }
                
                if result is NSDictionary
                {
                    //let message = (result as AnyObject).value(forKey: "Message") as! String
                    Alert().showAlertMessage(vc:self, titleStr:"Status", messageStr:"Your Old Password did not matched")
                    DispatchQueue.main.async {
                        self.stopAnimating()
                        //self.ResetPasswordview.isHidden = true
                        //self.ForgotPasswordview.isHidden = true
                        
                        //self.removeAnimate()
                    }
                }
                
        }
    }
}
    func callLogOut()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "MediProcLoginVC") as! MediProcLoginViewController
        self.present(nextViewController, animated:false, completion:nil)
    }
    
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@","^(?=.*[A-Z])(?=.*[!@#$&*]).{2,}$")
        return passwordTest.evaluate(with: password)
    }
    
    func isPasswordSame(password: String , confirmPassword : String) -> Bool {
        
        if password == confirmPassword{
            return true
        }else{
            return false
        }
    }
    
    func validateMobile(value: String) -> Bool
    {
        let PHONE_REGEX = "^[6-9][0-9]{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        print("result",result)
        return result
    }
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParent: nil)
                self.view.removeFromSuperview()
                self.removeFromParent()
            }
        })
    }
    
}

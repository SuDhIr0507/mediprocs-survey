//
//  MediProcHomeViewController.swift
//  MediProc
//
//  Created by Avante on 01/03/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import CoreData

class MediProcHomeViewController: UIViewController,NVActivityIndicatorViewable {

    let moContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var bgview: UIView!
    @IBOutlet weak var btnDoctor: UIButton!
    @IBOutlet weak var btnHospital: UIButton!
    @IBOutlet weak var btnLab: UIButton!
    @IBOutlet weak var btnPharmacy: UIButton!
    @IBOutlet weak var btnSearchClients: UIButton!
    @IBOutlet weak var btnTrainingMaterial: UIButton!
    
    var  gl:CAGradientLayer!
    var  gradient = CAGradientLayer()
    
    let webserviceCall = WebserviceCall()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        btnDoctor.layer.cornerRadius = 15
        btnDoctor.layer.borderWidth = 1
        btnDoctor.layer.borderColor = UIColor.white.cgColor
        btnDoctor.layer.shadowColor = UIColor.lightGray.cgColor
        btnDoctor.layer.shadowOpacity = 0.5
        btnDoctor.layer.shadowOffset = CGSize.zero
        btnDoctor.layer.shadowRadius = 10
        
        btnHospital.layer.cornerRadius = 15
        btnHospital.layer.borderWidth = 1
        btnHospital.layer.borderColor = UIColor.white.cgColor
        btnHospital.layer.shadowColor = UIColor.lightGray.cgColor
        btnHospital.layer.shadowOpacity = 0.5
        btnHospital.layer.shadowOffset = CGSize.zero
        btnHospital.layer.shadowRadius = 10
        
        btnLab.layer.cornerRadius = 15
        btnLab.layer.borderWidth = 1
        btnLab.layer.borderColor = UIColor.white.cgColor
        btnLab.layer.shadowColor = UIColor.lightGray.cgColor
        btnLab.layer.shadowOpacity = 0.5
        btnLab.layer.shadowOffset = CGSize.zero
        btnLab.layer.shadowRadius = 10
        
        btnPharmacy.layer.cornerRadius = 15
        btnPharmacy.layer.borderWidth = 1
        btnPharmacy.layer.borderColor = UIColor.white.cgColor
        btnPharmacy.layer.shadowColor = UIColor.lightGray.cgColor
        btnPharmacy.layer.shadowOpacity = 0.5
        btnPharmacy.layer.shadowOffset = CGSize.zero
        btnPharmacy.layer.shadowRadius = 10
        
        btnSearchClients.layer.cornerRadius = 15
        btnSearchClients.layer.borderWidth = 1
        btnSearchClients.layer.borderColor = UIColor.white.cgColor
        btnSearchClients.layer.shadowColor = UIColor.lightGray.cgColor
        btnSearchClients.layer.shadowOpacity = 0.5
        btnSearchClients.layer.shadowOffset = CGSize.zero
        btnSearchClients.layer.shadowRadius = 10
        
        
        btnTrainingMaterial.layer.cornerRadius = 15
        btnTrainingMaterial.layer.borderWidth = 1
        btnTrainingMaterial.layer.borderColor = UIColor.white.cgColor
        btnTrainingMaterial.layer.shadowColor = UIColor.lightGray.cgColor
        btnTrainingMaterial.layer.shadowOpacity = 0.5
        btnTrainingMaterial.layer.shadowOffset = CGSize.zero
        btnTrainingMaterial.layer.shadowRadius = 10
        
//        let bottomBorder = CALayer()
//
//        bottomBorder.frame = CGRect(x:navView.frame.origin.x, y: navView.frame.size.height+1 , width: navView.frame.size.width, height:0.5)
//        bottomBorder.backgroundColor = Color().hexStringToUIColor(hex: "#0060C5").cgColor  //UIColor(white: 0.8, alpha: 1.0).cgColor
//        navView.layer.addSublayer(bottomBorder)
//
       // applyGradientView()
        //applyGradient()
        
        
//        navView.layer.borderWidth = 0.5
//        navView.layer.borderColor = Color().hexStringToUIColor(hex: "#0060C5").cgColor

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        if Reachability().isConnectedToNetwork() == true {
            print("Connected to the internet")
            
            ////////// Get speciality list //////////////
            
            let specialityarray = UserData().getSpecialityArrayData().globalSpecialityArray
            print(specialityarray)
            if specialityarray.count == 0 {
                WebserviceCall().getDoctorSpeciality({ Array in
                })
            }
            
           ////////// Get setup type list //////////////
            
            let setuparray = UserData().getSetupTypeArrayData().globalSetupTypeArray
            print(setuparray)
            if setuparray.count == 0 {
                WebserviceCall().getTypeOfSetup({ Array in
                })
            }
            
            ////////// Get check list //////////////
            
            let checkarray = UserData().getCheckListArrayData().globalCheckListArray
            print(checkarray)
            if checkarray.count == 0 {
                WebserviceCall().getCheckListQuestions({ Array in
                })
            }
            
            ////////// Get wish list //////////////
            
            let wishlistarray = UserData().getWishListArrayData().globalWishListArray
            print(wishlistarray)
            if wishlistarray.count == 0 {
                WebserviceCall().getWishListFromServer({ Array in
                })
            }
            
            ////////// Get product list //////////////
            
            let productListArray = UserData().getProductListArrayData().globalProductListArray
            print(productListArray)
            if productListArray.count == 0 {
                WebserviceCall().getProductNameFromServer({ Array in
                })
            }
            
            ////////// Get district list //////////////
            
            let districtarray = UserData().getDistrictArrayData().globalDistrictArray
            print(districtarray)
            if districtarray.count == 0 {
                WebserviceCall().getAllDistrictFromServer({ Array in
                })
            }
            
            ////////////////////////////////////////////// 
            
        } else {
            print("No internet connection")
        }
    }
    
     func applyGradientView()
     {
        let GLlayer = CAGradientLayer()
        let hexclass = HexFromColorCodeClass()
        var  first = UIColor()
        var  second = UIColor()
    
        first = hexclass.hexStringToUIColor(hex:"#0060C5")
        second = hexclass.hexStringToUIColor(hex:"#2BB1FE")
        GLlayer.startPoint = CGPoint(x: 0, y: 0.5)
        GLlayer.endPoint = CGPoint(x: 1, y: 0.5)
        GLlayer.colors = [first,second]
        
        self.navView.layer.addSublayer(GLlayer)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func applyGradient() {
        
        let gradient = CAGradientLayer()
        let hexclass = HexFromColorCodeClass()
        
        let Start = hexclass.hexStringToUIColor(hex:"#0060C5").cgColor
        // let Center = hexclass.hexStringToUIColor(hex:"#2391FF").cgColor
        let End = hexclass.hexStringToUIColor(hex:"#2BB1FE").cgColor
        
        gradient.colors = [Start,End]
        gradient.locations = [0.0,0.5,1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.frame = CGRect(origin: CGPoint.zero, size: self.view.frame.size) //self.view.bounds
        self.view.layer.insertSublayer(gradient, at:0)
    }
    
    @IBAction func btnDoctorClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcDoctorInfoVC") as! MediProcDoctorInfoViewController
        UserDefaults.standard.set("Home", forKey: "call")
        self.present(nextViewController, animated:false, completion:nil)
    }
    
    @IBAction func btnHospitalClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcHospitalVC") as! MediProcHospitalViewController
           UserDefaults.standard.set("Hospital", forKey: "callHospital")
        self.present(nextViewController, animated:false, completion:nil)
    }
    
    @IBAction func btnLabClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcLabVC") as! MediProcLabViewController
        UserDefaults.standard.set("Lab", forKey: "callLab")
        self.present(nextViewController, animated:false, completion:nil)
    }
    
    @IBAction func btnPharmacyClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcPharmacyVC") as! MediProcPharmacyViewController
        UserDefaults.standard.set("Pharmacy", forKey: "callPharmacy")
        self.present(nextViewController, animated:false, completion:nil)
    }
    
    
    @IBAction func btnSearchClientsClicked(_ sender: Any) {
    
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcSearchClientsVC") as! MediProcSearchClientsViewController
        self.present(nextViewController, animated:false, completion:nil)
        
    }
    
    @IBAction func btnTrainingMaterialClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TrainingMaterialVC") as! MediProcTrainingMaterialViewController
        self.present(nextViewController, animated:false, completion:nil)
        
    }
    
    @IBAction func btnLogoutClicked(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "MediProcSettingVC") as! MediProcSettingViewController
        self.present(nextViewController, animated:false, completion:nil)

//        let alert = UIAlertController(title: nil, message: "Are you sure you want to Logout?", preferredStyle: UIAlertController.Style.alert)
//        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
//        alert.addAction(UIAlertAction(title: "Confirm", style: UIAlertAction.Style.default, handler: { action in
//
//            UserDefaults.standard.set("logout", forKey: "status")
//            UserDefaults.standard.set("",forKey: "UserID")
//            UserDefaults.standard.set("",forKey: "HLLUserID")
//            UserDefaults.standard.set(false, forKey: "IsHLLLogin")
//            UserData().ClearUserData()
//            HLLConnectUserData().ClearHLLConnectUserData()
//            self.callLogOut()
//
//        }))
//        self.present(alert, animated: true, completion: nil)
        
    }
    
    func callLogOut()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "MediProcLoginVC") as! MediProcLoginViewController
        self.present(nextViewController, animated:false, completion:nil)
    }
}


//
//  MediprocsPendingSurveySyncTableViewCell.swift
//  MediProc
//
//  Created by codeworx on 9/27/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit


protocol MediprocsPendingSurveySyncTableViewCellDelegate : class {
    func didTapSync(_ sender:MediprocsPendingSurveySyncTableViewCell)
}


class MediprocsPendingSurveySyncTableViewCell: UITableViewCell {

    weak var delegate: MediprocsPendingSurveySyncTableViewCellDelegate?
    
    @IBOutlet weak var lblSrNo: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnSync: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btnSyncClicked(_ sender: Any) {
        
        self.delegate?.didTapSync(self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

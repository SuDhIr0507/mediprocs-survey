//
//  PharmacySectionTableViewCell.swift
//  MediProc
//
//  Created by Avante on 11/04/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit

class PharmacySectionTableViewCell: UITableViewCell {

    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    var checkId = Int()
    
    @IBOutlet weak var headerview: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        headerview.backgroundColor = UIColor.white
        headerview.layer.borderWidth = 1
        headerview.layer.borderColor = Color().hexStringToUIColor(hex: "#ECECEC").cgColor
        headerview.layer.cornerRadius = 15
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

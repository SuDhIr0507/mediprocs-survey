//
//  MediprocsWishListPharmacyViewController.swift
//  MediProc
//
//  Created by codeworx on 9/24/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import CoreData
import Alamofire
import NVActivityIndicatorView

class MediprocsWishListPharmacyViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,NVActivityIndicatorViewable {
    
    let moContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    fileprivate let pickerProductName = ToolbarPickerView()
    fileprivate var productNameArray = [[String : AnyObject]]()
    var productID = Int()
    
    @IBOutlet weak var txtProductName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtProductSpecification: SkyFloatingLabelTextField!
    @IBOutlet weak var txtBrand: SkyFloatingLabelTextField!
    @IBOutlet weak var txtQuantity: SkyFloatingLabelTextField!
    
    @IBOutlet weak var btnClear: DesignButton!
    @IBOutlet weak var btnSave: DesignButton!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var tableWishList: UITableView!
    var wishlistArray = [AnyObject]()
    var storeObject = [String:Any]()
    var savedData = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtProductName.delegate = self
        txtProductSpecification.delegate = self
        txtBrand.delegate = self
        txtQuantity.delegate = self
        
        tableWishList.delegate = self
        tableWishList.dataSource = self
        
        addRightTextButon(viewText: txtProductName, imagename: "dropdown")
        
        self.pickerProductName.dataSource = self
        self.pickerProductName.delegate = self
        self.pickerProductName.toolbarDelegate = self
        
        self.txtProductName.inputView = self.pickerProductName
        self.txtProductName.inputAccessoryView = self.pickerProductName.toolbar
        
        applyGradientView()
        applyGradient()
    }
    func applyGradientView()
    {
        let GLlayer = CAGradientLayer()
        let hexclass = HexFromColorCodeClass()
        var  first = UIColor()
        var  second = UIColor()
        first = hexclass.hexStringToUIColor(hex:"#0060C5")
        second = hexclass.hexStringToUIColor(hex:"#2BB1FE")
        GLlayer.startPoint = CGPoint(x: 0, y: 0.5)
        GLlayer.endPoint = CGPoint(x: 1, y: 0.5)
        GLlayer.colors = [first,second]
        
        self.navView.layer.addSublayer(GLlayer)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func applyGradient() {
        
        let gradient = CAGradientLayer()
        let hexclass = HexFromColorCodeClass()
        
        let Start = hexclass.hexStringToUIColor(hex:"#0060C5").cgColor
        let End = hexclass.hexStringToUIColor(hex:"#2BB1FE").cgColor
        
        gradient.colors = [Start,End]
        gradient.locations = [0.0,0.5,1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.frame = CGRect(origin: CGPoint.zero, size: self.view.frame.size) //self.view.bounds
        self.view.layer.insertSublayer(gradient, at:0)
    }
    @IBAction func btnClearClicked(_ sender: Any) {
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability().isConnectedToNetwork() == true {
            print("Connected to the internet")
            
            ////////// Get product list //////////////
            
            let productNameArray = UserData().getProductListArrayData().globalProductListArray
            if productNameArray.count != 0 {
                self.productNameArray = productNameArray as! [[String : AnyObject]]
                pickerProductName.reloadAllComponents()
            }
            else{
                WebserviceCall().getProductNameFromServer({ Array in
                    self.productNameArray = Array as! [[String : AnyObject]]
                    self.pickerProductName.reloadAllComponents()
                })
            }
            
            ////////// Get wish list //////////////
            
            let wishlistarray = UserData().getWishListArrayData().globalWishListArray
            if wishlistarray.count != 0 {
                self.wishlistArray = wishlistarray as! [[String : AnyObject]] as [AnyObject]
                tableWishList.reloadData()
            }
            else{
                WebserviceCall().getDoctorSpeciality({ Array in
                    self.wishlistArray = Array as! [[String : AnyObject]] as [AnyObject]
                    self.tableWishList.reloadData()
                })
            }
            
        } else {
            
            print("No internet connection")
            
            ////////// Get product list //////////////
            
            let productNameArray = UserData().getProductListArrayData().globalProductListArray
            if productNameArray.count != 0 {
                self.productNameArray = productNameArray as! [[String : AnyObject]]
                pickerProductName.reloadAllComponents()
            }
            else{
                Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please Check Your Internet Connection")
            }
            
            ////////// Get wish list //////////////
            
            let wishlistarray = UserData().getWishListArrayData().globalWishListArray
            if wishlistarray.count != 0 {
                self.wishlistArray = wishlistarray as! [[String : AnyObject]] as [AnyObject]
                tableWishList.reloadData()
            }
            else{
                Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please Check Your Internet Connection !")
            }
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wishlistArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MediprocsWishListPharmacyTableViewCell
        
        cell.lblProductName.text = wishlistArray[indexPath.row]["ProductName"] as? String ?? "-"
        cell.lblSpecification.text = wishlistArray[indexPath.row]["Specification"] as? String ?? "-"
        cell.lblBrandName.text = wishlistArray[indexPath.row]["BrandName"] as? String ?? "-"
        let QT = wishlistArray[indexPath.row]["Quantity"] as? NSNumber
        cell.lblQuantity.text = ("\(QT ?? 0)")
        
        
        cell.selectionStyle = .none
        tableWishList.separatorColor = .clear
        tableWishList.tableFooterView = UIView()
        
        return cell
    }
    @IBAction func btnSaveNextClicked(_ sender: Any) {
        
        let jsonPharmacy = (storeObject as AnyObject).value(forKey: "Regd_json")
        let jsonChecklist = (storeObject as AnyObject).value(forKey: "CheckList_JSON") as! NSDictionary
        
        var jsonStrLab = String()
        var jsonStrChecklist = String()
        
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject:jsonPharmacy as Any, options: [])
            if let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) {
                jsonStrLab = jsonString
            }
        } catch {
            print(error)
        }
        
        ////////////////////////////////////////////////////////////////////////////////////
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject:jsonChecklist, options: [])
            if let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) {
                jsonStrChecklist = jsonString
            }
        } catch {
            print(error)
        }
        
        ///////////////////////////////////////////////////////////////////////////
        
        
        let userid = UserData().getUserData().UserID
        
        let object = ["ProductName":txtProductName.text!,"Specification":txtProductSpecification.text!,"BrandName":txtBrand.text!,"Quantity":txtQuantity.text!,"CreatedBY":userid] as [String : Any]
        
        
        var jsonStrWishListlist = String()
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject:object, options: [])
            if let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) {
                print(jsonString)
                jsonStrWishListlist = jsonString
            }
        } catch {
            print(error)
        }
        
        var jsonObject = [String:String]()
        jsonObject = ["Regd_json":jsonStrLab,"DR_LAB_json":"","CheckList_JSON": jsonStrChecklist,"WishList_JSON":jsonStrWishListlist]
        
        print("jsonObject of Doctor",jsonObject)
        
        if Reachability().isConnectedToNetwork() == true {
            print("Connected to the internet")
            
            startAnimating()
            
            let urlString = WebService.INSERT
            
            Alamofire.request(urlString, method:.post,parameters:jsonObject)
                .responseJSON { response in
                    print("JSON",response)
                    
                    if response.result.isSuccess
                    {
                        self.stopAnimating()
                        
                        if let result: Any = response.result.value
                        {
                            let JSON = result as? NSDictionary
                            print(result)
                            
                            let alert = UIAlertController(title: "Status", message: result as? String, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                                
                                self.storeObject.removeAll()
                                
                                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcHomeVC") as! MediProcHomeViewController
                                self.present(nextViewController, animated:false, completion:nil)
                                
                            }))
                            // alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                        else if response.result.isFailure
                        {
                            let alert = UIAlertController(title:"Error", message:response.result as? String, preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    
            }
            
        } else {
            print("No internet connection")
            userRegistrationDataSaveLocally(json: jsonObject)
        }
        
    }
    func userRegistrationDataSaveLocally(json:[String:Any]){
        
        var cartArray = [AnyObject]()
        var alreadyExist = Bool()
        alreadyExist = false
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Registration")
        
        do {
            
            let result = try self.moContext.fetch(request) as! [Registration]
            
            if result.count != 0 {
                
                cartArray = result
            }
            
        } catch {
            
            print("Error")
            
        }
        
        //////////////////////////////////////////////////////////////////////////////////////
        
        let request2 = NSFetchRequest<NSFetchRequestResult>(entityName: "Registration")
        
        do {
            
            let result2 = try self.moContext.fetch(request2) as! [Registration]
            
            if result2.count != 0 {
                
                let arry = result2 as NSArray
                var jsonData = NSDictionary()
                
                for dict in arry
                {
                    let data2 = (dict as AnyObject).value(forKey: "jsonObject") as! NSData
                    let unarchiveObject2 = NSKeyedUnarchiver.unarchiveObject(with: data2 as Data)
                    let arrayObject2 = unarchiveObject2 as! [String: Any]
                    let regdString = (arrayObject2 as AnyObject).value(forKey: "Regd_json") as! String
                    
                    print(arrayObject2)
                    
                    let data = regdString.data(using: .utf8)!
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? NSDictionary
                        {
                            print(json) // use the json here
                            jsonData = json
                        } else {
                            print("bad json")
                        }
                    } catch let error as NSError {
                        print(error)
                    }
                    
                    print(savedData)
                    let mobileNoSaveData = (savedData as AnyObject).value(forKey: "MobileNo") as! String
                    let emailIdSaveData = (savedData as AnyObject).value(forKey: "EmailId") as! String
                    
                    
                    let mobileNo = jsonData.value(forKey: "MobileNo") as! String
                    let emailId = jsonData.value(forKey: "EmailId") as! String
                    let RoleID = jsonData.value(forKey: "RoleID") as! String
                    
                    if RoleID == "4"{
                        
                        if mobileNo == mobileNoSaveData || emailId == emailIdSaveData
                        {
                            alreadyExist = true
                            break
                        }
                    }
                    
                }
                
            }
            
            
        } catch {
            print("Error")
        }
        
        if alreadyExist == true
        {
            Alert().showAlertMessageWithAction(vc: self, title: "Warning !", message: "User is already exist in local database.", actionTitles:["OK"], actions:[{ action1 in
                
                self.savedData.removeAll()
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcHomeVC") as! MediProcHomeViewController
                self.present(nextViewController, animated:false, completion:nil)
                
                }])
        }
        else
        {
            
            if cartArray.count < 20
            {
                
                let storeDescription = NSEntityDescription.entity(forEntityName: "Registration", in: self.moContext)
                
                let registration = Registration(entity:storeDescription!,insertInto:self.moContext)
                
                let data = NSKeyedArchiver.archivedData(withRootObject:json)
                
                registration.setValue(data, forKey: "jsonObject")
                let uuid = UUID().uuidString
                print(uuid)
                registration.id = uuid
                
                //let error: NSError?
                
                do {
                    try self.moContext.save()
                    
                    print("Data Save Successfully")
                    
                    Alert().showAlertMessageWithAction(vc: self, title: "Status", message:"Successfully register user to the local database.", actionTitles:["OK"], actions:[{ action1 in
                        
                        self.savedData.removeAll()
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcHomeVC") as! MediProcHomeViewController
                        self.present(nextViewController, animated:false, completion:nil)
                        
                        }])
                    
                    
                } catch let error {
                    print("Could not cache the response ")
                }
            }
            else
            {
                Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "You can not register user more than 20")
            }
        }
    }
    
    func addRightTextButon(viewText:UITextField, imagename: String) {
        viewText.rightViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        let image = UIImage(named: imagename)
        imageView.image = image
        viewText.rightView = imageView
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcHomeVC") as! MediProcHomeViewController
        self.present(nextViewController, animated:false, completion:nil)
    }
    
}

extension MediprocsWishListPharmacyViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.productNameArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return self.productNameArray[row]["ProductName"] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.txtProductName.text = self.productNameArray[row]["ProductName"] as? String
        productID = self.productNameArray[row]["ProductID"] as? Int ?? 0
    }
}

extension MediprocsWishListPharmacyViewController : ToolbarPickerViewDelegate {
    
    func didTapDone() {
        
        if productNameArray.count > 0{
            let selectedIndex = pickerProductName.selectedRow(inComponent: 0)
            self.txtProductName.text = self.productNameArray[selectedIndex]["ProductName"] as? String
            productID = self.productNameArray[selectedIndex]["ProductID"] as? Int ?? 0
            txtProductName.resignFirstResponder()
        }
        else{
            self.txtProductName.resignFirstResponder()
        }
    }
    
    func didTapCancel() {
        self.txtProductName.resignFirstResponder()
    }
}

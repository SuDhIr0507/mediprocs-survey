//
//  WebserviceCall.swift
//  MediProc
//
//  Created by codeworx on 9/4/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import CoreData

class WebserviceCall: NSObject {

    let moContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    var SpecialityArray = [AnyObject]()
    var SetupTypeArray = [AnyObject]()
    var CheckListArray = [AnyObject]()
    var DistrictArray = [AnyObject]()
    var TalukaArray = [AnyObject]()
    var CityArray = [AnyObject]()
    var HospitalTypeArray = [AnyObject]()
    var WishListArray = [AnyObject]()
    var ProductListArray = [AnyObject]()
    
    
    
    func getDoctorSpeciality(_ completion: @escaping ([AnyObject]) -> ())
    {
        let urlString = WebService.BASE_URL + WebService.GetDoctorSpeciality
        let jsonObject:[String:String] = ["":""]
        
        Utils().callUrlSession(urlString: urlString, method: "POST", params:jsonObject , completion: { (response) in
            print(response)
            let status = response.value(forKey: "status") as! String
            
            if status == "Success"
            {
                self.SpecialityArray = ((response.value(forKey: "output") as AnyObject) as! [AnyObject])
                DispatchQueue.main.async {
                    UserData().saveSpecialityArrayData(array: self.SpecialityArray)
                    completion(self.SpecialityArray)
                }
            }
            else if status == "Fail"
            {
            }
        })
    }
    
    func getTypeOfSetup(_ completion: @escaping ([AnyObject]) -> ())
    {
        let urlString = WebService.BASE_URL + WebService.GetTypeOfSetup
        let jsonObject:[String:String] = ["":""]
        
        Utils().callUrlSession(urlString: urlString, method: "POST", params:jsonObject , completion: { (response) in
            print(response)
            let status = response.value(forKey: "status") as! String
            if status == "Success"
            {
                self.SetupTypeArray = (response.value(forKey: "output") as AnyObject) as! [AnyObject]
                
                DispatchQueue.main.async {
                    UserData().saveSetupTypeArrayData(array: self.SetupTypeArray)
                    completion(self.SetupTypeArray)
                }
                
            }
            else if status == "Fail"
            {
            }
        })
        
    }
    
    func getCheckListQuestions(_ completion: @escaping ([AnyObject]) -> ())
    {
        let urlString = WebService.BASE_URL + WebService.GetAllChecklistSubQuestions
        let jsonObject:[String:String] = ["":""]
        
        Utils().callUrlSession(urlString: urlString, method: "POST", params:jsonObject , completion: { (response) in
            print(response)
            let status = response.value(forKey: "status") as! String
            if status == "Success"
            {
                self.CheckListArray = (((response.value(forKey: "lstChecklist") as AnyObject) ) as! [AnyObject])
                
                DispatchQueue.main.async {
                    UserData().saveCheckListArrayData(array: self.CheckListArray)
                    completion(self.CheckListArray)
                }
                
            }
            else if status == "Fail"
            {
            }
        })
        
    }
    
    func getWishListFromServer(_ completion: @escaping ([AnyObject]) -> ())
    {
        let urlString = WebService.GetWishList
        let jsonObject:[String:String] = ["UserID":UserData().getUserData().UserID]
        Utils().callUrlSession(urlString: urlString, method: "GET", params: jsonObject, completion: { (response) in
            print(response)
            let status = response.value(forKey: "Status") as! String
            if status == "Success" || status == "success" {
                
                self.WishListArray = (response.value(forKey: "OUTPUT") as AnyObject)  as! [AnyObject]
                
                DispatchQueue.main.async {
                    UserData().saveWishListArrayData(array: self.WishListArray)
                    completion(self.WishListArray)
                }
            }
            else if status == "Fail"{
            }
        })
    }
    
    func getAllDistrictFromServer(_ completion: @escaping ([AnyObject]) -> ())
    {
        let urlString = WebService.BASE_URL + WebService.GetAllDistrict
        let jsonObject:[String:String] = ["":""]
        
        Utils().callUrlSession(urlString: urlString, method: "POST", params:jsonObject , completion: { (response) in
            print(response)
            let status = response.value(forKey: "status") as! String
            if status == "Success"
            {
                
                self.DistrictArray = (response.value(forKey: "output") as AnyObject) as! [AnyObject]
                
                DispatchQueue.main.async {
                    UserData().saveDistrictArrayData(array: self.DistrictArray)
                    completion(self.DistrictArray)
                }
                
            }
            else if status == "Fail"
            {
            }
        })
        
    }
    
    func getAllTalukaOnDistrict(districtCode:String, _ completion: @escaping ([AnyObject]) -> ())
    {
        let urlString = WebService.BASE_URL + WebService.GetTalukaOnDistrictId
        let jsonObject:[String:String] = ["DistrictID":districtCode]
        
        Utils().callUrlSession(urlString: urlString, method: "POST", params:jsonObject , completion: { (response) in
            print(response)
            let status = response.value(forKey: "status") as! String
            if status == "Success"
            {
                self.TalukaArray = (response.value(forKey: "output") as AnyObject) as! [AnyObject]
                
                DispatchQueue.main.async {
                    
                    UserData().saveTalukaArrayDataDistrictWise(array: self.TalukaArray, districtId: districtCode)
                    completion(self.TalukaArray)
                }
                
            }
            else if status == "Fail"
            {
            }
        })
    }
    
    func getAllCityOnDistrict(districtCode:String, _ completion: @escaping ([AnyObject]) -> ())
    {
        let urlString = WebService.BASE_URL + WebService.GetAllCityOnDistrictID
        let jsonObject:[String:String] = ["DistrictID":districtCode]
        
        Utils().callUrlSession(urlString: urlString, method: "POST", params:jsonObject , completion: { (response) in
            print(response)
            let status = response.value(forKey: "status") as! String
            if status == "Success"
            {
                self.CityArray = (response.value(forKey: "output") as AnyObject) as! [AnyObject]
                
                DispatchQueue.main.async {
                    
                    UserData().saveCityArrayDataDistrictWise(array: self.CityArray, districtId: districtCode)
                    completion(self.CityArray)
                }
            }
            else if status == "Fail"
            {
            }
        })
    }
    
    func getHospitalType(_ completion: @escaping ([AnyObject]) -> ())
    {
        let urlString = WebService.BASE_URL + WebService.GetAllHospitalType
        let jsonObject:[String:String] = ["":""]
        
        Utils().callUrlSession(urlString: urlString, method: "POST", params:jsonObject , completion: { (response) in
            print(response)
            let status = response.value(forKey: "status") as! String            
            if status == "Success"
            {
                
                self.HospitalTypeArray = (response.value(forKey: "output") as AnyObject) as! [AnyObject]
                
                DispatchQueue.main.async {
                    UserData().saveHospitalTypeListArrayData(array: self.HospitalTypeArray)
                    completion(self.HospitalTypeArray)
                }
                
            }
            else if status == "Fail"
            {
            }
        })
    }
    
    func getProductNameFromServer(_ completion: @escaping ([AnyObject]) -> ())
    {
        let urlString = WebService.ProductList
        let jsonObject:[String:String] = ["RoleId":"0","IsNewLaunch":"0","Category":"0","SubCategory":"0","Brand":"0","FromPrice":"0","ToPrice":"","FromDisc":"0","ToDisc":"","ProductName":"","ProductId":"0","VendorID":"0","ClientId":"0"]
        
        Utils().callUrlSession(urlString: urlString, method: "GET", params:jsonObject , completion: { (response) in
            print(response)
            
            let status = response.value(forKey: "status") as! String
            
            if status == "Success"
            {
                self.ProductListArray = ((response.value(forKey: "output") as AnyObject) as! [AnyObject])
                DispatchQueue.main.async {
                    UserData().saveProductListArrayData(array: self.ProductListArray)
                    completion(self.ProductListArray)
                }
            }
            else if status == "Fail"
            {
            }
        })
    }
    
//    func GetProduct()
//    {
//
//        let urlString = WebService.BASE_URL_BETA + WebService.Product
//        print("Web service url",urlString)
//        startAnimating()
//        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.darkGray
//        let params: Parameters = ["RoleId":"0","IsNewLaunch":"0","Category":"0","SubCategory":"0","Brand":"0","FromPrice":"0","ToPrice":"","FromDisc":"0","ToDisc":"","ProductName":"","ProductId":"0","VendorID":"0","ClientId":"0"]
//        print("jsonObject",params)
//        Alamofire.request(urlString, method: .get, parameters: params)
//            .responseJSON { response in
//                print(response)
//                let status = response.response?.statusCode
//                print("status code",status)
//
//                let result: AnyObject = response.result.value as AnyObject
//                if result is NSArray
//                {
//                    self.productarray = (result as? [AnyObject])!
//                    self.Finalproductarray = Array(self.productarray.prefix(4))
//                    self.CollectionviewProduct.reloadData()
//                    self.stopAnimating()
//                }
//                else if result is NSDictionary
//                {
//                    self.stopAnimating()
//                    self.CollectionviewProduct.reloadData()
//                    let msg = result.value(forKey: "Message") as! String
//                    Alert().showAlertMessage(vc:self, titleStr: "Status", messageStr: msg)
//                }
//                //                else
//                //                {
//                //                    self.stopAnimating()
//                //                    Alert().showAlertMessage(vc:self, titleStr: "Error", messageStr: result as! String)
//                //                }
//
//
//        }
//    }
}

//
//  HLLConnectUserDetail.swift
//  MediProc
//
//  Created by Avante on 22/08/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit

class HLLConnectUserDetailsData {
    
    var Aadhar : String? = ""
    var BankId : String? = ""
    var CAddress : String = ""
    var Children : String = ""
    var CityLGDcode : String = ""
    var CountryLGDcode : String = ""
    var DESGID : String = ""
    var DepotID : String = ""
    var Depot_Code : String = ""
    var Depot_Name : String = ""
    var Designation : String = ""
    var EMPCODE : String = ""
    var FatherName : String = ""
    var FirstName : String = ""
    var ImagePath : String = ""
    var LastName : String = ""
    var Maritialstatus : String = ""
    var MaritialstatusId : Int = 0
    var MiddleName : String = ""
    var MotherName : String = ""
    var OrgId : String = ""
    var OrgName : String = ""
    var PAddress : String = ""
    var STATELGDCODE : String = ""
    var SpouseName : String = ""
    var SubOrgId : String = ""
    var SubOrgWiseDesgId : String = ""
    var USERNAME : String = ""
    var VoterId : String = ""
    var accountno : String = ""
    var anniversarydate : String = ""
    var bankname : String = ""
    var bloodgroup : String = ""
    var BLOODGROUPId : Int = 0
    var branchname : String = ""
    var bus_PhoneNo : String = ""
    var bus_address : String = ""
    var bus_company : String = ""
    var bus_email : String = ""
    var cast : String = ""
    var category : String = ""
    var dob : String = ""
    var education : String = ""
    var gender : String = ""
    var GENDERID : Int = 0
    var ifsccode : String = ""
    var joiningdate : String = ""
    var name : String = ""
    var pancard : String = ""
    var per_email : String = ""
    var per_mobile : String = ""
    var qualification : String = ""
    var religion : String = ""
    
}

class HLLConnectUserDetail: NSObject {
    
    func saveUserDetailsOfHLLConnect(userObject:NSDictionary)
    {
        
        UserDefaults.standard.set(userObject.value(forKey: "Aadhar"), forKey: "Aadhar")
        UserDefaults.standard.set(userObject.value(forKey: "BankId"), forKey: "BankId")
        UserDefaults.standard.set(userObject.value(forKey: "CAddress"), forKey: "CAddress")
        UserDefaults.standard.set(userObject.value(forKey: "Children"), forKey: "Children")
        UserDefaults.standard.set(userObject.value(forKey: "CityLGDcode"), forKey: "CityLGDcode")
        UserDefaults.standard.set(userObject.value(forKey: "CountryLGDcode"), forKey: "CountryLGDcode")
        UserDefaults.standard.set(userObject.value(forKey: "DESGID"), forKey: "DESGID")
        UserDefaults.standard.set(userObject.value(forKey: "DepotID"), forKey: "DepotID")
        UserDefaults.standard.set(userObject.value(forKey: "Depot_Code"), forKey: "Depot_Code")
        UserDefaults.standard.set(userObject.value(forKey: "Depot_Name"), forKey: "Depot_Name")
        UserDefaults.standard.set(userObject.value(forKey: "Designation"), forKey: "Designation")
        UserDefaults.standard.set(userObject.value(forKey: "EMPCODE"), forKey: "EMPCODE")
        UserDefaults.standard.set(userObject.value(forKey: "FatherName"), forKey: "FatherName")
        UserDefaults.standard.set(userObject.value(forKey: "FirstName"), forKey: "FirstName")
        UserDefaults.standard.set(userObject.value(forKey: "ImagePath"), forKey: "ImagePath")
        UserDefaults.standard.set(userObject.value(forKey: "LastName"), forKey: "LastName")
        UserDefaults.standard.set(userObject.value(forKey: "Maritialstatus"), forKey: "Maritialstatus")
        UserDefaults.standard.set(userObject.value(forKey: "MaritialstatusId"), forKey: "MaritialstatusId")
        UserDefaults.standard.set(userObject.value(forKey: "MiddleName"), forKey: "MiddleName")
        UserDefaults.standard.set(userObject.value(forKey: "MotherName"), forKey: "MotherName")
        UserDefaults.standard.set(userObject.value(forKey: "OrgId"), forKey: "OrgId")
        UserDefaults.standard.set(userObject.value(forKey: "OrgName"), forKey: "OrgName")
        UserDefaults.standard.set(userObject.value(forKey: "PAddress"), forKey: "PAddress")
        UserDefaults.standard.set(userObject.value(forKey: "STATELGDCODE"), forKey: "STATELGDCODE")
        UserDefaults.standard.set(userObject.value(forKey: "SpouseName"), forKey: "SpouseName")
        UserDefaults.standard.set(userObject.value(forKey: "SubOrgId"), forKey: "SubOrgId")
        UserDefaults.standard.set(userObject.value(forKey: "SubOrgWiseDesgId"), forKey: "SubOrgWiseDesgId")
        UserDefaults.standard.set(userObject.value(forKey: "USERNAME"), forKey: "USERNAME")
        UserDefaults.standard.set(userObject.value(forKey: "VoterId"), forKey: "VoterId")
        UserDefaults.standard.set(userObject.value(forKey: "accountno"), forKey: "accountno")
        UserDefaults.standard.set(userObject.value(forKey: "anniversarydate"), forKey: "anniversarydate")
        UserDefaults.standard.set(userObject.value(forKey: "bankname"), forKey: "bankname")
        UserDefaults.standard.set(userObject.value(forKey: "bloodgroup"), forKey: "bloodgroup")
        UserDefaults.standard.set(userObject.value(forKey: "BLOODGROUPId"), forKey: "BLOODGROUPId")
        UserDefaults.standard.set(userObject.value(forKey: "branchname"), forKey: "branchname")
        UserDefaults.standard.set(userObject.value(forKey: "bus_PhoneNo"), forKey: "bus_PhoneNo")
        UserDefaults.standard.set(userObject.value(forKey: "bus_address"), forKey: "bus_address")
        UserDefaults.standard.set(userObject.value(forKey: "bus_company"), forKey: "bus_company")
        UserDefaults.standard.set(userObject.value(forKey: "bus_email"), forKey: "bus_email")
        UserDefaults.standard.set(userObject.value(forKey: "cast"), forKey: "cast")
        UserDefaults.standard.set(userObject.value(forKey: "category"), forKey: "category")
        UserDefaults.standard.set(userObject.value(forKey: "dob"), forKey: "dob")
        UserDefaults.standard.set(userObject.value(forKey: "education"), forKey: "education")
        UserDefaults.standard.set(userObject.value(forKey: "gender"), forKey: "gender")
        UserDefaults.standard.set(userObject.value(forKey: "GENDERID"), forKey: "GENDERID")
        UserDefaults.standard.set(userObject.value(forKey: "ifsccode"), forKey: "ifsccode")
        UserDefaults.standard.set(userObject.value(forKey: "joiningdate"), forKey: "joiningdate")
        UserDefaults.standard.set(userObject.value(forKey: "name"), forKey: "name")
        UserDefaults.standard.set(userObject.value(forKey: "pancard"), forKey: "pancard")
        UserDefaults.standard.set(userObject.value(forKey: "per_email"), forKey: "per_email")
        UserDefaults.standard.set(userObject.value(forKey: "per_mobile"), forKey: "per_mobile")
        UserDefaults.standard.set(userObject.value(forKey: "qualification"), forKey: "qualification")
        UserDefaults.standard.set(userObject.value(forKey: "religion"), forKey: "religion")
        
    }
    
    func getUserDetailsOfHLLConnect() -> HLLConnectUserDetailsData
    {
        
        let UserDetail = HLLConnectUserDetailsData()
        
        UserDetail.Aadhar = UserDefaults.standard.string(forKey: "Aadhar") ?? ""
        UserDetail.BankId = UserDefaults.standard.string(forKey: "BankId") ?? ""
        UserDetail.CAddress = UserDefaults.standard.string(forKey: "CAddress") ?? ""
        UserDetail.Children = UserDefaults.standard.string(forKey: "Children") ?? ""
        UserDetail.CityLGDcode = UserDefaults.standard.string(forKey: "CityLGDcode") ?? ""
        UserDetail.CountryLGDcode = UserDefaults.standard.string(forKey: "CountryLGDcode") ?? ""
        UserDetail.DESGID = UserDefaults.standard.string(forKey: "DESGID") ?? ""
        UserDetail.DepotID = UserDefaults.standard.string(forKey: "DepotID") ?? ""
        UserDetail.Depot_Code = UserDefaults.standard.string(forKey: "Depot_Code") ?? ""
        UserDetail.Depot_Name = UserDefaults.standard.string(forKey: "Depot_Name") ?? ""
        UserDetail.Designation = UserDefaults.standard.string(forKey: "Designation") ?? ""
        UserDetail.EMPCODE = UserDefaults.standard.string(forKey: "EMPCODE") ?? ""
        UserDetail.FatherName = UserDefaults.standard.string(forKey: "FatherName") ?? ""
        UserDetail.FirstName = UserDefaults.standard.string(forKey: "FirstName") ?? ""
        UserDetail.ImagePath = UserDefaults.standard.string(forKey: "ImagePath") ?? ""
        UserDetail.LastName = UserDefaults.standard.string(forKey: "LastName") ?? ""
        UserDetail.Maritialstatus = UserDefaults.standard.string(forKey: "Maritialstatus") ?? ""
        UserDetail.MaritialstatusId = UserDefaults.standard.integer(forKey: "MaritialstatusId")
        UserDetail.MiddleName = UserDefaults.standard.string(forKey: "MiddleName") ?? ""
        UserDetail.MotherName = UserDefaults.standard.string(forKey: "MotherName") ?? ""
        UserDetail.OrgId = UserDefaults.standard.string(forKey: "OrgId") ?? ""
        UserDetail.OrgName = UserDefaults.standard.string(forKey: "OrgName") ?? ""
        UserDetail.PAddress = UserDefaults.standard.string(forKey: "PAddress") ?? ""
        UserDetail.STATELGDCODE = UserDefaults.standard.string(forKey: "STATELGDCODE") ?? ""
        UserDetail.SpouseName = UserDefaults.standard.string(forKey: "SpouseName") ?? ""
        UserDetail.SubOrgId = UserDefaults.standard.string(forKey: "SubOrgId") ?? ""
        UserDetail.SubOrgWiseDesgId = UserDefaults.standard.string(forKey: "SubOrgWiseDesgId") ?? ""
        UserDetail.USERNAME = UserDefaults.standard.string(forKey: "USERNAME") ?? ""
        UserDetail.VoterId = UserDefaults.standard.string(forKey: "VoterId") ?? ""
        UserDetail.accountno = UserDefaults.standard.string(forKey: "accountno") ?? ""
        UserDetail.anniversarydate = UserDefaults.standard.string(forKey: "anniversarydate") ?? ""
        UserDetail.bankname = UserDefaults.standard.string(forKey: "bankname") ?? ""
        UserDetail.bloodgroup = UserDefaults.standard.string(forKey: "bloodgroup") ?? ""
        UserDetail.BLOODGROUPId = UserDefaults.standard.integer(forKey: "BLOODGROUPId")
        UserDetail.branchname = UserDefaults.standard.string(forKey: "branchname") ?? ""
        UserDetail.bus_PhoneNo = UserDefaults.standard.string(forKey: "bus_PhoneNo") ?? ""
        UserDetail.bus_address = UserDefaults.standard.string(forKey: "bus_address") ?? ""
        UserDetail.bus_company = UserDefaults.standard.string(forKey: "bus_company") ?? ""
        UserDetail.bus_email = UserDefaults.standard.string(forKey: "bus_email") ?? ""
        UserDetail.cast = UserDefaults.standard.string(forKey: "cast") ?? ""
        UserDetail.category = UserDefaults.standard.string(forKey: "category") ?? ""
        UserDetail.dob = UserDefaults.standard.string(forKey: "dob") ?? ""
        UserDetail.education = UserDefaults.standard.string(forKey: "education") ?? ""
        UserDetail.gender = UserDefaults.standard.string(forKey: "gender") ?? ""
        UserDetail.GENDERID = UserDefaults.standard.integer(forKey: "GENDERID")
        UserDetail.ifsccode = UserDefaults.standard.string(forKey: "ifsccode") ?? ""
        UserDetail.joiningdate = UserDefaults.standard.string(forKey: "joiningdate") ?? ""
        UserDetail.name = UserDefaults.standard.string(forKey: "name") ?? ""
        UserDetail.pancard = UserDefaults.standard.string(forKey: "pancard") ?? ""
        UserDetail.per_email = UserDefaults.standard.string(forKey: "per_email") ?? ""
        UserDetail.per_mobile = UserDefaults.standard.string(forKey: "per_mobile") ?? ""
        UserDetail.qualification = UserDefaults.standard.string(forKey: "qualification") ?? ""
        UserDetail.religion = UserDefaults.standard.string(forKey: "religion") ?? ""
        
        
        return UserDetail
        
    }
    
}


//
//  UserData.swift
//  SignPost
//
//  Created by codeworx on 12/5/18.
//  Copyright © 2018 Avante Codeworx. All rights reserved.
//

import UIKit
import CoreData


class UserInfo {
    
    var IsActive : String = ""
    var PersonName : String = ""
    var UserID : String = ""
    var UserLoginID : String = ""
    var UserRoleID : String = ""
    
}

class commonArray{
    
    var globalSpecialityArray : [AnyObject]  = []
    var globalSetupTypeArray : [AnyObject] = []
    var globalCheckListArray : [AnyObject] = []
    var globalDistrictArray : [AnyObject] = []
    var globalTalukaArray : [AnyObject] = []
    var globalCityArray : [AnyObject] = []
    var globalHospitalTypeArray : [AnyObject] = []
    var globalWishListArray : [AnyObject] = []
    var globalProductListArray : [AnyObject] = []
}

class UserData: NSObject {
    
    let moContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    /////////////////////////////// Comman Array /////////////////////////////////////
    
    func saveSpecialityArrayData(array:[AnyObject]){
        
        let data = NSKeyedArchiver.archivedData(withRootObject: array)
        UserDefaults.standard.set(data, forKey: "globalSpecialityArray")
    }
    func getSpecialityArrayData() -> commonArray{
        
        let comman = commonArray()
        let placesData = UserDefaults.standard.object(forKey: "globalSpecialityArray") as? NSData
        if let placesData = placesData {
            comman.globalSpecialityArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as! [AnyObject]
        }
        return comman
    }
    
    func saveProductListArrayData(array:[AnyObject]){
        
        let data = NSKeyedArchiver.archivedData(withRootObject: array)
        UserDefaults.standard.set(data, forKey: "globalProductListArray")
    }
    func getProductListArrayData() -> commonArray{
        
        let comman = commonArray()
        let placesData = UserDefaults.standard.object(forKey: "globalProductListArray") as? NSData
        if let placesData = placesData {
            comman.globalProductListArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as! [AnyObject]
        }
        return comman
    }
    
    func saveSetupTypeArrayData(array:[AnyObject]){
        let data = NSKeyedArchiver.archivedData(withRootObject: array)
        UserDefaults.standard.set(data, forKey: "globalSetupTypeArray")
    }
    func getSetupTypeArrayData() -> commonArray{
        
        let comman = commonArray()
        let placesData = UserDefaults.standard.object(forKey: "globalSetupTypeArray") as? NSData
        if let placesData = placesData {
            comman.globalSetupTypeArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as! [AnyObject]
        }
        return comman
        
    }
    
    func saveCheckListArrayData(array:[AnyObject]){
        let data = NSKeyedArchiver.archivedData(withRootObject: array)
        UserDefaults.standard.set(data, forKey: "globalCheckListArray")
    }
    func getCheckListArrayData() -> commonArray{
        
        let comman = commonArray()
        let placesData = UserDefaults.standard.object(forKey: "globalCheckListArray") as? NSData
        if let placesData = placesData {
            comman.globalCheckListArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as! [AnyObject]
        }
        return comman
    }
    
    func saveWishListArrayData(array:[AnyObject]){
        let data = NSKeyedArchiver.archivedData(withRootObject: array)
        UserDefaults.standard.set(data, forKey: "globalWishListArray")
    }
    func getWishListArrayData() -> commonArray{
        
        let comman = commonArray()
        let placesData = UserDefaults.standard.object(forKey: "globalWishListArray") as? NSData
        if let placesData = placesData {
            comman.globalWishListArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as! [AnyObject]
        }
        return comman
    }
    
    func saveHospitalTypeListArrayData(array:[AnyObject]){
        let data = NSKeyedArchiver.archivedData(withRootObject: array)
        UserDefaults.standard.set(data, forKey: "globalHospitalTypeArray")
    }
    func getHospitalTypeListArrayData() -> commonArray{
        
        let comman = commonArray()
        let placesData = UserDefaults.standard.object(forKey: "globalHospitalTypeArray") as? NSData
        if let placesData = placesData {
            comman.globalHospitalTypeArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as! [AnyObject]
        }
        return comman
    }
    
    func saveDistrictArrayData(array:[AnyObject]){
        let data = NSKeyedArchiver.archivedData(withRootObject: array)
        UserDefaults.standard.set(data, forKey: "globalDistrictArray")
    }
    func getDistrictArrayData() -> commonArray{
        
        let comman = commonArray()
        let placesData = UserDefaults.standard.object(forKey: "globalDistrictArray") as? NSData
        if let placesData = placesData {
            comman.globalDistrictArray = NSKeyedUnarchiver.unarchiveObject(with: placesData as Data) as! [AnyObject]
        }
        return comman
    }
    
    func saveTalukaArrayDataDistrictWise(array:[AnyObject], districtId:String){
        
        var newArray = Bool()
        newArray = false
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Taluka")
        let result = try? self.moContext.fetch(request) as! [Taluka]
        
        for result in result! {
            
            let id = result.value(forKey: "id") as! String
            
            if districtId == id
            {
                let data = NSKeyedArchiver.archivedData(withRootObject:array)
                result.setValue(data, forKey: "talukaArray")
                result.id = districtId
                
                newArray = true
                break
            }
        }
        
        do {
            
            try self.moContext.save()
            print("saved!")
            
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        if newArray == false{
            
            let storeDescription = NSEntityDescription.entity(forEntityName: "Taluka", in: self.moContext)
            let talukaObj = Taluka(entity:storeDescription!,insertInto:self.moContext)
            let data = NSKeyedArchiver.archivedData(withRootObject:array)
            
            talukaObj.setValue(data, forKey: "talukaArray")
            talukaObj.id = districtId
            
            do {
                try self.moContext.save()
                
                print("Data Save Successfully")
                
            } catch _ {
                print("Could not cache the response ")
            }
        }
    }
    
    func getTalukaArrayDataDistrictWise(districtId:String) -> commonArray{
        
        let comman = commonArray()
        let request2 = NSFetchRequest<NSFetchRequestResult>(entityName: "Taluka")
        
        do {
            
            let result2 = try self.moContext.fetch(request2) as! [Taluka]
            
            if result2.count != 0 {
                
                let arry = result2 as NSArray                
                for dict in arry
                {
                    let talukaArr = (dict as AnyObject).value(forKey: "talukaArray") as! NSData
                    let distId = (dict as AnyObject).value(forKey: "id") as! String
                    let unarchiveArray = NSKeyedUnarchiver.unarchiveObject(with: talukaArr as Data)
                    let talArray = unarchiveArray as! [AnyObject]
                    
                    if districtId == distId{
                        
                        comman.globalTalukaArray = talArray
                    }
                }
            }
            
        } catch {
            print("Error")
        }
        
        return comman
    }
    
    func saveCityArrayDataDistrictWise(array:[AnyObject], districtId:String){
        
        var newArray = Bool()
        newArray = false
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        let result = try? self.moContext.fetch(request) as! [City]
        
        for result in result! {
            
            let id = result.value(forKey: "id") as! String
            
            if districtId == id
            {
                let data = NSKeyedArchiver.archivedData(withRootObject:array)
                result.setValue(data, forKey: "cityArray")
                result.id = districtId
                
                newArray = true
                break
            }
        }
        
        do {
            
            try self.moContext.save()
            print("saved!")
            
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        if newArray == false{
            
            let storeDescription = NSEntityDescription.entity(forEntityName: "City", in: self.moContext)
            let cityObj = City(entity:storeDescription!,insertInto:self.moContext)
            let data = NSKeyedArchiver.archivedData(withRootObject:array)
            
            cityObj.setValue(data, forKey: "cityArray")
            cityObj.id = districtId
            
            do {
                try self.moContext.save()
                
                print("Data Save Successfully")
                
            } catch _ {
                print("Could not cache the response ")
            }
        }
    }
    
    func getCityArrayDataDistrictWise(districtId:String) -> commonArray{
        
        let comman = commonArray()
        let request2 = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        
        do {
            
            let result2 = try self.moContext.fetch(request2) as! [City]
            
            if result2.count != 0 {
                
                let arry = result2 as NSArray
                for dict in arry
                {
                    let cityArr = (dict as AnyObject).value(forKey: "cityArray") as! NSData
                    let distId = (dict as AnyObject).value(forKey: "id") as! String
                    let unarchiveArray = NSKeyedUnarchiver.unarchiveObject(with: cityArr as Data)
                    let cityArray = unarchiveArray as! [AnyObject]
                    
                    if districtId == distId{
                        
                        comman.globalCityArray = cityArray
                    }
                }
            }
            
        } catch {
            print("Error")
        }
        
        return comman
    }
    
    func clearGlobalArrayData()
    {
        UserDefaults.standard.set(" ",forKey: "globalSpecialityArray")
        UserDefaults.standard.set(" ",forKey: "globalSetupTypeArray")
        UserDefaults.standard.set(" ",forKey: "globalCheckListArray")
        UserDefaults.standard.set(" ",forKey: "globalDistrictArray")
        UserDefaults.standard.set(" ",forKey: "globalHospitalTypeArray")
        UserDefaults.standard.set(" ",forKey: "globalWishListArray")
        UserDefaults.standard.set(" ",forKey: "globalProductListArray")
    }
    
    
    //////////////////////////////// User Info ///////////////////////////////////////
    
    func saveUserData(userObject:NSDictionary)
    {
        UserDefaults.standard.set(userObject.value(forKey: "IsActive"), forKey: "IsActive")
        UserDefaults.standard.set(userObject.value(forKey: "PersonName"), forKey: "PersonName")
        UserDefaults.standard.set(userObject.value(forKey: "UserID"), forKey: "UserID")
        UserDefaults.standard.set(userObject.value(forKey: "UserLoginID"), forKey: "UserLoginID")
        UserDefaults.standard.set(userObject.value(forKey: "UserRoleID"), forKey: "UserRoleID")
        UserDefaults.standard.set("login", forKey: "loginStatus")
        
    }
    func ClearUserData()
    {
        UserDefaults.standard.set(" ",forKey: "IsActive")
        UserDefaults.standard.set(" ",forKey: "PersonName")
        UserDefaults.standard.set(" ",forKey: "UserID")
        UserDefaults.standard.set(" ",forKey: "UserLoginID")
        UserDefaults.standard.set(" ",forKey: "UserRoleID")
    }


    func getUserData() -> UserInfo
    {

        let userData = UserInfo()
        
        userData.IsActive = UserDefaults.standard.string(forKey: "IsActive") ?? ""
        userData.PersonName = UserDefaults.standard.string(forKey: "PersonName") ?? ""
        userData.UserID = UserDefaults.standard.string(forKey: "UserID") ?? ""
        userData.UserLoginID = UserDefaults.standard.string(forKey: "UserLoginID") ?? ""
        userData.UserRoleID = UserDefaults.standard.string(forKey: "UserRoleID") ?? ""
      
        
       return userData
        
    }
    
    func getInvalidDataPopup() -> NSDictionary{
        
        let dictionary = ["status":"Fail","massage":"Invalid data found"]
        
        return dictionary as NSDictionary
    }
    
    func getRequestTimeoutError() -> NSDictionary{
        
        let dictionary = ["status":"Fail","massage":"Request timeout please try after sometime !"]
        
        return dictionary as NSDictionary
    }
    

}

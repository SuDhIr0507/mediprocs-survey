//
//  HLLConnectUserInfo.swift
//  MediProc
//
//  Created by Avante on 22/08/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit


class HLLConnectUserInfo {
    
    var Aadhar : String = ""
    var BAddress : String = ""
    var BCompany : String = ""
    var BEmail : String = ""
    var BMobile : String = ""
    var CAddress : String = ""
    var CategoryId : String = ""
    var CenterID : String = ""
    var Children : String = ""
    var CityCode : String = ""
    var CityName : String = ""
    var DESGID : String = ""
    var DESGLEVELID : String = ""
    var DISTLGDCODE : String = ""
    var DISTNAME : String = ""
    var Designation : String = ""
    var EmpCode : String = ""
    var FacilityCode : String = ""
    var FacilityName : String = ""
    var FatherName : String = ""
    var FirstName : String = ""
    var GPLGDCODE : String = ""
    var HLLDISTRICTID : String = ""
    var IPADDRESS : String = ""
    var ImagePath : String = ""
    var LabName : String = ""
    var Labcode : String = ""
    var Langitude : String = ""
    var LastName : String = ""
    var Latitude : String = ""
    var Maritialstatus : String = ""
    var MaritialstatusId : String = ""
    var MiddleName : String = ""
    var MotherName : String = ""
    var OMTCSCID : String = ""
    var OrgId : String = ""
    var OrgName : String = ""
    var PAddress : String = ""
    var ProjectId : String = ""
    var ProjectName : String = ""
    var ReligionId : String = ""
    var STATELGDCODE : String = ""
    var SUBORGID : String = ""
    var SUBORGWISEDESGID : String = ""
    var SpouseName : String = ""
    var SubOrgName : String = ""
    var TALLGDCODE : String = ""
    var USERNAME : String = ""
    var UpdateLocation : String = ""
    var VoterId : String = ""
    var WorkLocationName : String = ""
    var ZipCode : String = ""
    var accountno : String = ""
    var anniversarydate : String = ""
    var bankname : String = ""
    var bloodgroup : String = ""
    var branchname : String = ""
    var cast : String = ""
    var category : String = ""
    var dob : String = ""
    var edu : String = ""
    var gender : String = ""
    var genderID : String = ""
    var ifsccode : String = ""
    var joiningdate : String = ""
    var name : String = ""
    var pancard : String = ""
    var per_email : String = ""
    var per_mobile : String = ""
    var qualification : String = ""
    var religion : String = ""
}

class HLLConnectUserData: NSObject {
    
    
    func HLLConnectsaveUserData(userObject:NSDictionary)
    {
        UserDefaults.standard.set(userObject.value(forKey: "Aadhar"), forKey: "Aadhar")
        UserDefaults.standard.set(userObject.value(forKey: "BAddress"), forKey: "BAddress")
        UserDefaults.standard.set(userObject.value(forKey: "BCompany"), forKey: "BCompany")
        UserDefaults.standard.set(userObject.value(forKey: "BEmail"), forKey: "BEmail")
        UserDefaults.standard.set(userObject.value(forKey: "BMobile"), forKey: "BMobile")
        UserDefaults.standard.set(userObject.value(forKey: "CAddress"), forKey: "CAddress")
        UserDefaults.standard.set(userObject.value(forKey: "CategoryId"), forKey: "CategoryId")
        UserDefaults.standard.set(userObject.value(forKey: "CenterID"), forKey: "CenterID")
        UserDefaults.standard.set(userObject.value(forKey: "Children"), forKey: "Children")
        UserDefaults.standard.set(userObject.value(forKey: "CityCode"), forKey: "CityCode")
        UserDefaults.standard.set(userObject.value(forKey: "CityName"), forKey: "CityName")
        UserDefaults.standard.set(userObject.value(forKey: "DESGID"), forKey: "DESGID")
        UserDefaults.standard.set(userObject.value(forKey: "DESGLEVELID"), forKey: "DESGLEVELID")
        UserDefaults.standard.set(userObject.value(forKey: "DISTLGDCODE"), forKey: "DISTLGDCODE")
        UserDefaults.standard.set(userObject.value(forKey: "DISTNAME"), forKey: "DISTNAME")
        UserDefaults.standard.set(userObject.value(forKey: "Designation"), forKey: "Designation")
        UserDefaults.standard.set(userObject.value(forKey: "EmpCode"), forKey: "EmpCode")
        UserDefaults.standard.set(userObject.value(forKey: "FacilityCode"), forKey: "FacilityCode")
        UserDefaults.standard.set(userObject.value(forKey: "FacilityName"), forKey: "FacilityName")
        UserDefaults.standard.set(userObject.value(forKey: "FatherName"), forKey: "FatherName")
        UserDefaults.standard.set(userObject.value(forKey: "FirstName"), forKey: "FirstName")
        UserDefaults.standard.set(userObject.value(forKey: "GPLGDCODE"), forKey: "GPLGDCODE")
        UserDefaults.standard.set(userObject.value(forKey: "HLLDISTRICTID"), forKey: "HLLDISTRICTID")
        UserDefaults.standard.set(userObject.value(forKey: "IPADDRESS"), forKey: "IPADDRESS")
        UserDefaults.standard.set(userObject.value(forKey: "ImagePath"), forKey: "ImagePath")
        UserDefaults.standard.set(userObject.value(forKey: "LabName"), forKey: "LabName")
        UserDefaults.standard.set(userObject.value(forKey: "Labcode"), forKey: "Labcode")
        UserDefaults.standard.set(userObject.value(forKey: "Langitude"), forKey: "Langitude")
        UserDefaults.standard.set(userObject.value(forKey: "LastName"), forKey: "LastName")
        UserDefaults.standard.set(userObject.value(forKey: "Latitude"), forKey: "Latitude")
        UserDefaults.standard.set(userObject.value(forKey: "Maritialstatus"), forKey: "Maritialstatus")
        UserDefaults.standard.set(userObject.value(forKey: "MaritialstatusId"), forKey: "MaritialstatusId")
        UserDefaults.standard.set(userObject.value(forKey: "MiddleName"), forKey: "MiddleName")
        UserDefaults.standard.set(userObject.value(forKey: "MotherName"), forKey: "MotherName")
        UserDefaults.standard.set(userObject.value(forKey: "OMTCSCID"), forKey: "OMTCSCID")
        UserDefaults.standard.set(userObject.value(forKey: "OrgId"), forKey: "OrgId")
        UserDefaults.standard.set(userObject.value(forKey: "OrgName"), forKey: "OrgName")
        UserDefaults.standard.set(userObject.value(forKey: "PAddress"), forKey: "PAddress")
        UserDefaults.standard.set(userObject.value(forKey: "ProjectId"), forKey: "ProjectId")
        UserDefaults.standard.set(userObject.value(forKey: "ProjectName"), forKey: "ProjectName")
        UserDefaults.standard.set(userObject.value(forKey: "ReligionId"), forKey: "ReligionId")
        UserDefaults.standard.set(userObject.value(forKey: "STATELGDCODE"), forKey: "STATELGDCODE")
        UserDefaults.standard.set(userObject.value(forKey: "SUBORGID"), forKey: "SUBORGID")
        UserDefaults.standard.set(userObject.value(forKey: "SUBORGWISEDESGID"), forKey: "SUBORGWISEDESGID")
        UserDefaults.standard.set(userObject.value(forKey: "SpouseName"), forKey: "SpouseName")
        UserDefaults.standard.set(userObject.value(forKey: "SubOrgName"), forKey: "SubOrgName")
        UserDefaults.standard.set(userObject.value(forKey: "TALLGDCODE"), forKey: "TALLGDCODE")
        UserDefaults.standard.set(userObject.value(forKey: "USERNAME"), forKey: "USERNAME")
        UserDefaults.standard.set(userObject.value(forKey: "UpdateLocation"), forKey: "UpdateLocation")
        UserDefaults.standard.set(userObject.value(forKey: "VoterId"), forKey: "VoterId")
        UserDefaults.standard.set(userObject.value(forKey: "WorkLocationName"), forKey: "WorkLocationName")
        UserDefaults.standard.set(userObject.value(forKey: "ZipCode"), forKey: "ZipCode")
        UserDefaults.standard.set(userObject.value(forKey: "accountno"), forKey: "accountno")
        UserDefaults.standard.set(userObject.value(forKey: "anniversarydate"), forKey: "anniversarydate")
        UserDefaults.standard.set(userObject.value(forKey: "bankname"), forKey: "bankname")
        UserDefaults.standard.set(userObject.value(forKey: "bloodgroup"), forKey: "bloodgroup")
        UserDefaults.standard.set(userObject.value(forKey: "branchname"), forKey: "branchname")
        UserDefaults.standard.set(userObject.value(forKey: "cast"), forKey: "cast")
        UserDefaults.standard.set(userObject.value(forKey: "category"), forKey: "category")
        UserDefaults.standard.set(userObject.value(forKey: "dob"), forKey: "dob")
        UserDefaults.standard.set(userObject.value(forKey: "edu"), forKey: "edu")
        UserDefaults.standard.set(userObject.value(forKey: "gender"), forKey: "gender")
        UserDefaults.standard.set(userObject.value(forKey: "genderID"), forKey: "genderID")
        UserDefaults.standard.set(userObject.value(forKey: "ifsccode"), forKey: "ifsccode")
        UserDefaults.standard.set(userObject.value(forKey: "joiningdate"), forKey: "joiningdate")
        UserDefaults.standard.set(userObject.value(forKey: "name"), forKey: "name")
        UserDefaults.standard.set(userObject.value(forKey: "pancard"), forKey: "pancard")
        UserDefaults.standard.set(userObject.value(forKey: "per_email"), forKey: "per_email")
        UserDefaults.standard.set(userObject.value(forKey: "per_mobile"), forKey: "per_mobile")
        UserDefaults.standard.set(userObject.value(forKey: "qualification"), forKey: "qualification")
        UserDefaults.standard.set(userObject.value(forKey: "religion"), forKey: "religion")
        UserDefaults.standard.set("login", forKey: "loginStatus")
        
    }
    func ClearHLLConnectUserData()
    {
        
        UserDefaults.standard.set(" ",forKey: "Aadhar")
        UserDefaults.standard.set(" ",forKey: "BAddress")
        UserDefaults.standard.set(" ",forKey: "BCompany")
        UserDefaults.standard.set(" ",forKey: "BEmail")
        UserDefaults.standard.set(" ",forKey: "BMobile")
        UserDefaults.standard.set(" ",forKey: "CAddress")
        UserDefaults.standard.set(" ",forKey: "CategoryId")
        UserDefaults.standard.set(" ",forKey: "CenterID")
        UserDefaults.standard.set(" ",forKey: "Children")
        UserDefaults.standard.set(" ",forKey: "CityCode")
        UserDefaults.standard.set(" ",forKey: "CityName")
        UserDefaults.standard.set(" ",forKey: "DESGID")
        UserDefaults.standard.set(" ",forKey: "DESGLEVELID")
        UserDefaults.standard.set(" ",forKey: "DISTLGDCODE")
        UserDefaults.standard.set(" ",forKey: "DISTNAME")
        UserDefaults.standard.set(" ",forKey: "Designation")
        UserDefaults.standard.set(" ",forKey: "EmpCode")
        UserDefaults.standard.set(" ",forKey: "FacilityCode")
        UserDefaults.standard.set(" ",forKey: "FacilityName")
        UserDefaults.standard.set(" ",forKey: "FatherName")
        UserDefaults.standard.set(" ",forKey: "FirstName")
        UserDefaults.standard.set(" ",forKey: "GPLGDCODE")
        UserDefaults.standard.set(" ",forKey: "HLLDISTRICTID")
        UserDefaults.standard.set(" ",forKey: "IPADDRESS")
        UserDefaults.standard.set(" ",forKey: "ImagePath")
        UserDefaults.standard.set(" ",forKey: "LabName")
        UserDefaults.standard.set(" ",forKey: "Labcode")
        UserDefaults.standard.set(" ",forKey: "Langitude")
        UserDefaults.standard.set(" ",forKey: "LastName")
        UserDefaults.standard.set(" ",forKey: "Latitude")
        UserDefaults.standard.set(" ",forKey: "Maritialstatus")
        UserDefaults.standard.set(" ",forKey: "MaritialstatusId")
        UserDefaults.standard.set(" ",forKey: "MiddleName")
        UserDefaults.standard.set(" ",forKey: "MotherName")
        UserDefaults.standard.set(" ",forKey: "OMTCSCID")
        UserDefaults.standard.set(" ",forKey: "OrgId")
        UserDefaults.standard.set(" ",forKey: "OrgName")
        UserDefaults.standard.set(" ",forKey: "PAddress")
        UserDefaults.standard.set(" ",forKey: "ProjectId")
        UserDefaults.standard.set(" ",forKey: "ProjectName")
        UserDefaults.standard.set(" ",forKey: "ReligionId")
        UserDefaults.standard.set(" ",forKey: "STATELGDCODE")
        UserDefaults.standard.set(" ",forKey: "SUBORGID")
        UserDefaults.standard.set(" ",forKey: "SUBORGWISEDESGID")
        UserDefaults.standard.set(" ",forKey: "SpouseName")
        UserDefaults.standard.set(" ",forKey: "SubOrgName")
        UserDefaults.standard.set(" ",forKey: "TALLGDCODE")
        UserDefaults.standard.set(" ",forKey: "USERNAME")
        UserDefaults.standard.set(" ",forKey: "UpdateLocation")
        UserDefaults.standard.set(" ",forKey: "VoterId")
        UserDefaults.standard.set(" ",forKey: "WorkLocationName")
        UserDefaults.standard.set(" ",forKey: "ZipCode")
        UserDefaults.standard.set(" ",forKey: "accountno")
        UserDefaults.standard.set(" ",forKey: "anniversarydate")
        UserDefaults.standard.set(" ",forKey: "bankname")
        UserDefaults.standard.set(" ",forKey: "bloodgroup")
        UserDefaults.standard.set(" ",forKey: "branchname")
        UserDefaults.standard.set(" ",forKey: "cast")
        UserDefaults.standard.set(" ",forKey: "category")
        UserDefaults.standard.set(" ",forKey: "dob")
        UserDefaults.standard.set(" ",forKey: "edu")
        UserDefaults.standard.set(" ",forKey: "gender")
        UserDefaults.standard.set(" ",forKey: "genderID")
        UserDefaults.standard.set(" ",forKey: "ifsccode")
        UserDefaults.standard.set(" ",forKey: "joiningdate")
        UserDefaults.standard.set(" ",forKey: "name")
        UserDefaults.standard.set(" ",forKey: "pancard")
        UserDefaults.standard.set(" ",forKey: "per_email")
        UserDefaults.standard.set(" ",forKey: "per_mobile")
        UserDefaults.standard.set(" ",forKey: "qualification")
        UserDefaults.standard.set(" ",forKey: "religion")
        
        
    }
    
    
    func getHLLConnectUserData() -> HLLConnectUserInfo
    {
        
        let userData = HLLConnectUserInfo()
        
        userData.Aadhar = UserDefaults.standard.string(forKey: "Aadhar") ?? ""
        userData.BAddress = UserDefaults.standard.string(forKey: "BAddress") ?? ""
        userData.BCompany = UserDefaults.standard.string(forKey: "BCompany") ?? ""
        userData.BEmail = UserDefaults.standard.string(forKey: "BEmail") ?? ""
        userData.BMobile = UserDefaults.standard.string(forKey: "BMobile") ?? ""
        userData.CAddress = UserDefaults.standard.string(forKey: "CAddress") ?? ""
        userData.CategoryId = UserDefaults.standard.string(forKey: "CategoryId") ?? ""
        userData.CenterID = UserDefaults.standard.string(forKey: "CenterID") ?? ""
        userData.Children = UserDefaults.standard.string(forKey: "Children") ?? ""
        userData.CityCode = UserDefaults.standard.string(forKey: "CityCode") ?? ""
        userData.CityName = UserDefaults.standard.string(forKey: "CityName") ?? ""
        userData.DESGID = UserDefaults.standard.string(forKey: "DESGID") ?? ""
        userData.DESGLEVELID = UserDefaults.standard.string(forKey: "DESGLEVELID") ?? ""
        userData.DISTLGDCODE = UserDefaults.standard.string(forKey: "DISTLGDCODE") ?? ""
        userData.DISTNAME = UserDefaults.standard.string(forKey: "DISTNAME") ?? ""
        userData.Designation = UserDefaults.standard.string(forKey: "Designation") ?? ""
        userData.EmpCode = UserDefaults.standard.string(forKey: "EmpCode") ?? ""
        userData.FacilityCode = UserDefaults.standard.string(forKey: "FacilityCode") ?? ""
        userData.FacilityName = UserDefaults.standard.string(forKey: "FacilityName") ?? ""
        userData.FatherName = UserDefaults.standard.string(forKey: "FatherName") ?? ""
        userData.FirstName = UserDefaults.standard.string(forKey: "FirstName") ?? ""
        userData.GPLGDCODE = UserDefaults.standard.string(forKey: "GPLGDCODE") ?? ""
        userData.HLLDISTRICTID = UserDefaults.standard.string(forKey: "HLLDISTRICTID") ?? ""
        userData.IPADDRESS = UserDefaults.standard.string(forKey: "IPADDRESS") ?? ""
        userData.ImagePath = UserDefaults.standard.string(forKey: "ImagePath") ?? ""
        userData.LabName = UserDefaults.standard.string(forKey: "LabName") ?? ""
        userData.Labcode = UserDefaults.standard.string(forKey: "Labcode") ?? ""
        userData.Langitude = UserDefaults.standard.string(forKey: "Langitude") ?? ""
        userData.LastName = UserDefaults.standard.string(forKey: "LastName") ?? ""
        userData.Latitude = UserDefaults.standard.string(forKey: "Latitude") ?? ""
        userData.Maritialstatus = UserDefaults.standard.string(forKey: "Maritialstatus") ?? ""
        userData.MaritialstatusId = UserDefaults.standard.string(forKey: "MaritialstatusId") ?? ""
        userData.MiddleName = UserDefaults.standard.string(forKey: "MiddleName") ?? ""
        userData.MotherName = UserDefaults.standard.string(forKey: "MotherName") ?? ""
        userData.FirstName = UserDefaults.standard.string(forKey: "FirstName") ?? ""
        userData.OMTCSCID = UserDefaults.standard.string(forKey: "OMTCSCID") ?? ""
        userData.OrgId = UserDefaults.standard.string(forKey: "OrgId") ?? ""
        userData.OrgName = UserDefaults.standard.string(forKey: "OrgName") ?? ""
        userData.PAddress = UserDefaults.standard.string(forKey: "PAddress") ?? ""
        userData.ProjectId = UserDefaults.standard.string(forKey: "ProjectId") ?? ""
        userData.ProjectName = UserDefaults.standard.string(forKey: "ProjectName") ?? ""
        userData.ReligionId = UserDefaults.standard.string(forKey: "ReligionId") ?? ""
        userData.STATELGDCODE = UserDefaults.standard.string(forKey: "STATELGDCODE") ?? ""
        userData.SUBORGID = UserDefaults.standard.string(forKey: "SUBORGID") ?? ""
        userData.SUBORGWISEDESGID = UserDefaults.standard.string(forKey: "SUBORGWISEDESGID") ?? ""
        userData.SpouseName = UserDefaults.standard.string(forKey: "SpouseName") ?? ""
        userData.SubOrgName = UserDefaults.standard.string(forKey: "SubOrgName") ?? ""
        userData.TALLGDCODE = UserDefaults.standard.string(forKey: "TALLGDCODE") ?? ""
        userData.USERNAME = UserDefaults.standard.string(forKey: "USERNAME") ?? ""
        userData.UpdateLocation = UserDefaults.standard.string(forKey: "UpdateLocation") ?? ""
        userData.VoterId = UserDefaults.standard.string(forKey: "VoterId") ?? ""
        userData.WorkLocationName = UserDefaults.standard.string(forKey: "WorkLocationName") ?? ""
        userData.ZipCode = UserDefaults.standard.string(forKey: "ZipCode") ?? ""
        userData.accountno = UserDefaults.standard.string(forKey: "accountno") ?? ""
        userData.anniversarydate = UserDefaults.standard.string(forKey: "anniversarydate") ?? ""
        userData.bankname = UserDefaults.standard.string(forKey: "bankname") ?? ""
        userData.bloodgroup = UserDefaults.standard.string(forKey: "bloodgroup") ?? ""
        userData.branchname = UserDefaults.standard.string(forKey: "branchname") ?? ""
        userData.cast = UserDefaults.standard.string(forKey: "cast") ?? ""
        userData.category = UserDefaults.standard.string(forKey: "category") ?? ""
        userData.dob = UserDefaults.standard.string(forKey: "dob") ?? ""
        userData.edu = UserDefaults.standard.string(forKey: "edu") ?? ""
        userData.gender = UserDefaults.standard.string(forKey: "gender") ?? ""
        userData.genderID = UserDefaults.standard.string(forKey: "genderID") ?? ""
        userData.ifsccode = UserDefaults.standard.string(forKey: "ifsccode") ?? ""
        userData.joiningdate = UserDefaults.standard.string(forKey: "joiningdate") ?? ""
        userData.name = UserDefaults.standard.string(forKey: "name") ?? ""
        userData.pancard = UserDefaults.standard.string(forKey: "pancard") ?? ""
        userData.per_email = UserDefaults.standard.string(forKey: "per_email") ?? ""
        userData.per_mobile = UserDefaults.standard.string(forKey: "per_mobile") ?? ""
        userData.qualification = UserDefaults.standard.string(forKey: "qualification") ?? ""
        userData.religion = UserDefaults.standard.string(forKey: "religion") ?? ""
        
        return userData
        
    }
    
}


//
//  SplashScreenViewController.swift
//  MediProc
//
//  Created by Avante on 06/03/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import SwiftyGif

class SplashScreenViewController: UIViewController {

    let logoAnimationView = LogoAnimationView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(logoAnimationView)
        logoAnimationView.pinEdgesToSuperView()
       // logoAnimationView.logoGifImageView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
      //  logoAnimationView.logoGifImageView.startAnimatingGif()
    }
    
}

extension SplashScreenViewController: SwiftyGifDelegate {
    func gifDidStop(sender: UIImageView) {
      //  logoAnimationView.isHidden = true
    }
    
}

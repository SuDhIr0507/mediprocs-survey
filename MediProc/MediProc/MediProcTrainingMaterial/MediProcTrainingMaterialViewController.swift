 //
//  MediProcTrainingMaterialViewController.swift
//  MediProc
//
//  Created by Avante on 24/04/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class MediProcTrainingMaterialViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,NVActivityIndicatorViewable {
    
    @IBOutlet weak var Navview: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableviewTrainingMaterial: UITableView!
    
    var ClientArray = [AnyObject]()
    var searchArray = [AnyObject]()
    var searchActive : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableviewTrainingMaterial.delegate = self
        tableviewTrainingMaterial.dataSource = self
        
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x:0, y: 64, width: Navview.frame.size.width, height:0.7)
        bottomBorder.backgroundColor = Color().hexStringToUIColor(hex: "#0060C5").cgColor
        Navview.layer.addSublayer(bottomBorder)
        
        applyGradient()
        applyGradientView()
        GetAllVendorList()
        
    }
   
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func applyGradientView()
    {
        let GLlayer = CAGradientLayer()
        let hexclass = HexFromColorCodeClass()
        var  first = UIColor()
        var  second = UIColor()
        first = hexclass.hexStringToUIColor(hex:"#0060C5")
        second = hexclass.hexStringToUIColor(hex:"#2BB1FE")
        GLlayer.startPoint = CGPoint(x: 0, y: 0.5)
        GLlayer.endPoint = CGPoint(x: 1, y: 0.5)
        GLlayer.colors = [first,second]
        
        self.Navview.layer.addSublayer(GLlayer)
    }
    
    func applyGradient() {
        
        let gradient = CAGradientLayer()
        let hexclass = HexFromColorCodeClass()
        
        let Start = hexclass.hexStringToUIColor(hex:"#0060C5").cgColor
        let End = hexclass.hexStringToUIColor(hex:"#2BB1FE").cgColor
        
        gradient.colors = [Start,End]
        gradient.locations = [0.0,0.5,1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.frame = CGRect(origin: CGPoint.zero, size: self.view.frame.size) //self.view.bounds
        self.view.layer.insertSublayer(gradient, at:0)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        //searchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        if searchBar.text == "" {
            searchActive = false
        }
        else{
            searchActive = true
            
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchActive = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchActive = false
        searchBar.text = ""
        searchBar.endEditing(true)
        
        self.tableviewTrainingMaterial.reloadData()
    }
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        return ((searchBar.text?.count ?? 0) + text.count - Int(range.length) > 25) ? false : true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchArray.removeAll()
        
        if searchText.count != 0 {
            searchActive = true
            searchResult()
            
        } else {
            searchActive = false
        }
        
        tableviewTrainingMaterial.reloadData()
    }
    
    func searchResult()
    {
        let string = searchBar.text!
        
        let searchPredicate = NSPredicate(format: "(VendorName CONTAINS[cd] %@)",string)
        let array = (self.ClientArray as NSArray).filtered(using: searchPredicate)
        searchArray = array as [AnyObject]
        print(searchArray)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
                if searchActive == true{
        
                    return searchArray.count
                }
        
        return self.ClientArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MediProcVendorListTableViewCell", for: indexPath) as! MediProcVendorListTableViewCell
        
        if searchActive == true
        {
            cell.lblVendorName.text = self.searchArray[indexPath.row]["VendorName"] as? String
        }
        else
        {
            cell.lblVendorName.text = self.ClientArray[indexPath.row]["VendorName"] as? String
        }
        
        cell.bgview.layer.borderWidth = 0.5
        cell.bgview.layer.borderColor = UIColor.lightGray.cgColor
        cell.bgview.layer.cornerRadius = 10
        cell.bgview.backgroundColor = UIColor.white
    
        cell.selectionStyle = .none
        tableviewTrainingMaterial.separatorColor = UIColor.clear
        tableviewTrainingMaterial.tableFooterView = UIView()
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if searchActive == true
        {
            
        let UserID = searchArray[indexPath.row]["UserID"] as? NSNumber
        UserDefaults.standard.set(UserID,forKey: "SelectedUserID")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcVendorListVC") as! MediProcVendorListViewController
           nextViewController.UserID = UserID!
           nextViewController.type = "Document"
        self.present(nextViewController, animated:false, completion:nil)
            
        }
            
        else
        {
            let UserID = ClientArray[indexPath.row]["UserID"] as? NSNumber
            UserDefaults.standard.set(UserID, forKey: "SelectedUserID")
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcVendorListVC") as! MediProcVendorListViewController
            nextViewController.UserID = UserID!
            nextViewController.type = "Document"
            self.present(nextViewController, animated:false, completion:nil)
        }
    }
    
    func GetAllVendorList()
    {
        let urlString = WebService.BASE_URL + WebService.GetAllVendorList
        
        startAnimating()
        let jsonObject:[String:String] = ["":""]
        print("search text is",jsonObject)
        Utils().callUrlSession(urlString: urlString, method: "POST", params:jsonObject , completion: { (response) in
            print(response)
            let status = response.value(forKey: "status") as! String
            let message = response.value(forKey: "message") as! String
            
            if status == "Success"
            {
                self.ClientArray = (((response.value(forKey: "output") as AnyObject) ) as! [AnyObject])
                print("GetAllVendorList data is ",self.ClientArray)
                
                DispatchQueue.main.async {
                    
                   self.tableviewTrainingMaterial.reloadData()
                   self.stopAnimating()
                }
                
            }
            else if status == "Fail"
            {
                Alert().showAlertMessage(vc:self, titleStr: "status", messageStr: message)
            }
        })
    }

    @IBAction func btnBackCLicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcHomeVC") as! MediProcHomeViewController
        self.present(nextViewController, animated:false, completion:nil)
    }
    
}

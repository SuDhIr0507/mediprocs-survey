//
//  MediProcVendorDocumentViewController.swift
//  MediProc
//
//  Created by Avante on 29/04/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Alamofire
import QuickLook

class MediProcVendorDocumentViewController: UIViewController,NVActivityIndicatorViewable,UITableViewDelegate,UITableViewDataSource,QLPreviewControllerDataSource {

    var UserID = NSNumber()
    var DocumentArray = [AnyObject]()
    var previewItem = NSURL()
    
    @IBOutlet weak var tableviewDocument: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         GetVendorTrainingMaterial()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.DocumentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MediProcVendorDocumentTableViewCell", for: indexPath) as! MediProcVendorDocumentTableViewCell
    
        cell.textLabel?.text = self.DocumentArray[indexPath.row]["MaterialTrainingName"] as? String
      
        cell.selectionStyle = .default
        tableviewDocument.separatorColor = UIColor.lightGray
        tableviewDocument.tableFooterView = UIView()
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let Filename = self.DocumentArray[indexPath.row]["UrlPath"] as? String
        
        Utils().downloadFile(urlString: Filename!, completion: { (response) in
            self.stopAnimating()
            print(response)
            let quickLookController = QLPreviewController()
            quickLookController.dataSource = self
            self.previewItem = response
            self.present(quickLookController, animated: true, completion: nil)
        })
    }
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        
        return previewItem
    }
    
    
    func GetVendorTrainingMaterial()
    {
        startAnimating()
      
        let urlString = WebService.BASE_URL + WebService.GetVendorTrainingMaterial
        let userID = UserDefaults.standard.value(forKey:"SelectedUserID") as? NSNumber
        let jsonObject:[String:String] = ["VendorID":"\(userID ?? 0)","Trainingtype":"1"]
        print("search text of document is",jsonObject)
        Utils().callUrlSession(urlString: urlString, method: "POST", params:jsonObject , completion: { (response) in
            print(response)
            let status = response.value(forKey: "status") as! String
            let message = response.value(forKey: "message") as! String
            
            if status == "Success"
            {
              self.DocumentArray = (((response.value(forKey: "output") as AnyObject) ) as! [AnyObject])
                print("GetAllVendorList data is ",self.DocumentArray)
                
                DispatchQueue.main.async {
                    
                    self.tableviewDocument.reloadData()
                    self.stopAnimating()
                }
                
            }
            else if status == "Fail"
            {
                 DispatchQueue.main.async {
                  self.stopAnimating()
                Alert().showAlertMessage(vc:self, titleStr: "status", messageStr: "Training Document is not Available")
                }
            }
        })
    }
 
        /*
        startAnimating()
        let urlString = WebService.BASE_URL_BETA + WebService.GetVendorTrainingMaterial
        let params:[String:Any] = ["VendorID":"\(UserID)","Trainingtype":"1"]
        print(params)
        Alamofire.request(urlString, method: .post, parameters: params)
            .responseJSON { response in
                print("response",response)
                if response.result.isSuccess
                {
                    self.stopAnimating()
                    
                    if let result: Any = response.result.value{
                        let JSON = result as? NSDictionary
                        print(response)
                        let alert = UIAlertController(title: "Status", message: result as? String, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
                            
                            
                            
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcHomeVC") as! MediProcHomeViewController
                            self.present(nextViewController, animated:false, completion:nil)
                            
                        }))
                        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    else if response.result.isFailure
                    {
                        let alert = UIAlertController(title: "Error", message:response.result as? String, preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                }
 */

  }


//
//  YoutubeVideoVC.h
//  DJ LAILAK
//
//  Created by Mac on 10/01/18.
//  Copyright © 2018 AppsPlanet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YoutubeVideoVC : UIViewController
+ (NSString *)extractYoutubeIdFromLink:(NSString *)link;
@end

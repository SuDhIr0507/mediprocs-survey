
//
//  YoutubeVideoVC.m
//  DJ LAILAK
//
//  Created by Mac on 10/01/18.
//  Copyright © 2018 AppsPlanet. All rights reserved.
//

#import "YoutubeVideoVC.h"

@implementation YoutubeVideoVC
+ (NSString *)extractYoutubeIdFromLink:(NSString *)link {
    
    //NSString *urlStr = [link absoluteString];
    // retrieve id param from url (ex http://www.youtube.com/watch?v=uCYTpCYLqbs)
    //NSString *ID = [link substringFromIndex:[link rangeOfString:@"="].location + 1];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc]init];
    indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    
    [indicator startAnimating];
    
    NSString *regexString = @"((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)";
    NSRegularExpression *regExp = [NSRegularExpression regularExpressionWithPattern:regexString
                                                                            options:NSRegularExpressionCaseInsensitive
                                                                              error:nil];
    
    NSArray *array = [regExp matchesInString:link options:0 range:NSMakeRange(0,link.length)];
    if (array.count > 0) {
        NSTextCheckingResult *result = array.firstObject;
         [indicator stopAnimating];
        return [link substringWithRange:result.range];
        
    }
    else {
        return @"not found";
    }
    return nil;
}
@end

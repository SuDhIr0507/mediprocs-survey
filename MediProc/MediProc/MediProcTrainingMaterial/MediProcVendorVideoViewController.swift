//
//  MediProcVendorVideoViewController.swift
//  MediProc
//
//  Created by Avante on 29/04/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import youtube_ios_player_helper


class MediProcVendorVideoViewController: UIViewController,NVActivityIndicatorViewable,UITableViewDelegate,UITableViewDataSource  {

    @IBOutlet weak var tableviewVideo: UITableView!
    
    var videourl = NSString ()
    var UserID = NSNumber()
    var VideoArray = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         GetVendorTrainingMaterial()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.VideoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MediProcVendorVideoTableViewCell", for: indexPath) as! MediProcVendorVideoTableViewCell
        
        cell.lblVideoName?.text = self.VideoArray[indexPath.row]["MaterialTrainingName"] as? String
      
        videourl =  (self.VideoArray[indexPath.row]["UtubUrlPath"] as? String as NSString?)!
        var videoid = NSString()
        videoid = YoutubeVideoVC.extractYoutubeId(fromLink:videourl as String!)! as NSString
    
        let strUrl = String(format:"https://img.youtube.com/vi/%@/hqdefault.jpg",videoid)
        var url = NSURL(string:strUrl)
        print("image url",strUrl)
        cell.imgview?.setImageFromURl(stringImageUrl: strUrl)
       
        
        cell.selectionStyle = .default
        tableviewVideo.separatorColor = UIColor.lightGray
        tableviewVideo.tableFooterView = UIView()
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        // let currentCell = tableView.cellForRow(at: indexPath)! as! MediProcVendorVideoTableViewCell
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "VendorPlayVideoVC") as! MediProcVendorPlayVideoViewController
        nextViewController.youtubevideourl = videourl
        self.present(nextViewController, animated:false, completion:nil)
        
    }
    
    func GetVendorTrainingMaterial()
    {
        let urlString = WebService.BASE_URL + WebService.GetVendorTrainingMaterial
        startAnimating()
        let userID = UserDefaults.standard.value(forKey:"SelectedUserID") as? NSNumber
        let jsonObject:[String:String] = ["VendorID":"\(userID ?? 0)","Trainingtype":"2"]
        print("search text is",jsonObject)
        Utils().callUrlSession(urlString: urlString, method: "POST", params:jsonObject , completion: { (response) in
            print(response)
            let status = response.value(forKey: "status") as! String
            let message = response.value(forKey: "message") as! String
            
            if status == "Success"
            {
                self.VideoArray = (((response.value(forKey: "output") as AnyObject) ) as! [AnyObject])
                print("GetAllVendorList data is ",self.VideoArray)
                
                DispatchQueue.main.async {
                    
                    self.tableviewVideo.reloadData()
                    self.stopAnimating()
                }
                
            }
            else if status == "Fail"
            {
                DispatchQueue.main.async {
                    
                  self.stopAnimating()
                Alert().showAlertMessage(vc:self, titleStr: "status", messageStr: "Training Video is not Available")
                }
            }
        })
    }

}

extension UIImageView{
    
    func setImageFromURl(stringImageUrl url: String){
        
        if let url = NSURL(string: url) {
            if let data = NSData(contentsOf: url as URL) {
                self.image = UIImage(data: data as Data)
            }
        }
 
    }
}

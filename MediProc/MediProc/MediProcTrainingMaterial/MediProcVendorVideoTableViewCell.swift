//
//  MediProcVendorVideoTableViewCell.swift
//  MediProc
//
//  Created by Avante on 29/04/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import youtube_ios_player_helper

class MediProcVendorVideoTableViewCell: UITableViewCell,YTPlayerViewDelegate {

    @IBOutlet weak var imgview: UIImageView!
    @IBOutlet weak var lblVideoName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

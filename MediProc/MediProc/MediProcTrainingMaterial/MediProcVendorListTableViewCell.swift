//
//  MediProcVendorListTableViewCell.swift
//  MediProc
//
//  Created by Avante on 26/04/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit

class MediProcVendorListTableViewCell: UITableViewCell {

    @IBOutlet weak var bgview: UIView!
    @IBOutlet weak var lblVendorName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

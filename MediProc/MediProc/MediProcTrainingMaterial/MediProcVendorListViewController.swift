//
//  MediProcVendorListViewController.swift
//  MediProc
//
//  Created by Avante on 29/04/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit

class MediProcVendorListViewController: UIViewController,SlidingContainerViewControllerDelegate {

    
    @IBOutlet weak var navview: UIView!
    @IBOutlet weak var bgview: UIView!
     var UserID = NSNumber()
     var type = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        applyGradientView()
        applyGradient()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let callDrop = UserDefaults.standard.string(forKey: "callDropDown")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let vc1 = storyBoard.instantiateViewController(withIdentifier: "VendorDocumentVC") as! MediProcVendorDocumentViewController
                let vc2 = storyBoard.instantiateViewController(withIdentifier: "VendorVideoVC") as! MediProcVendorVideoViewController
        
        
        let slidingContainerViewController = SlidingContainerViewController (
            parent: self,
            contentViewControllers: [vc1,vc2],
            titles: ["Document", "Video"])
        
        self.bgview.addSubview(slidingContainerViewController.view)
        
        slidingContainerViewController.sliderView.appearance.outerPadding = 0
        slidingContainerViewController.sliderView.appearance.innerPadding = 50
        slidingContainerViewController.sliderView.appearance.fixedWidth = true
        slidingContainerViewController.delegate = self
        //slidingContainerViewController.setCurrentViewControllerAtIndex(0)
        
        if type == "Document"
        {
             slidingContainerViewController.setCurrentViewControllerAtIndex(0)
        }

        else if type == "Video"
         {
         slidingContainerViewController.setCurrentViewControllerAtIndex(1)
        }
    }
    
    func applyGradientView()
    {
        let GLlayer = CAGradientLayer()
        let hexclass = HexFromColorCodeClass()
        var  first = UIColor()
        var  second = UIColor()
        first = hexclass.hexStringToUIColor(hex:"#0060C5")
        second = hexclass.hexStringToUIColor(hex:"#2BB1FE")
        GLlayer.startPoint = CGPoint(x: 0, y: 0.5)
        GLlayer.endPoint = CGPoint(x: 1, y: 0.5)
        GLlayer.colors = [first,second]
        
        self.navview.layer.addSublayer(GLlayer)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func applyGradient() {
        
        let gradient = CAGradientLayer()
        let hexclass = HexFromColorCodeClass()
        
        let Start = hexclass.hexStringToUIColor(hex:"#0060C5").cgColor
        let End = hexclass.hexStringToUIColor(hex:"#2BB1FE").cgColor
        
        gradient.colors = [Start,End]
        gradient.locations = [0.0,0.5,1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.frame = CGRect(origin: CGPoint.zero, size: self.view.frame.size) //self.view.bounds
        self.view.layer.insertSublayer(gradient, at:0)
    }
    
    func viewControllerWithColorAndTitle (_ color: UIColor, title: String) -> UIViewController {
        let vc = UIViewController ()
        vc.view.backgroundColor = color
        
        let label = UILabel (frame: vc.view.frame)
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.font = UIFont (name: "HelveticaNeue-Light", size: 25)
        label.text = title
        
        label.sizeToFit()
        label.center = view.center
        
        vc.view.addSubview(label)
        
        return vc
    }
    
    // MARK: SlidingContainerViewControllerDelegate
    
    func slidingContainerViewControllerDidMoveToViewController(_ slidingContainerViewController: SlidingContainerViewController, viewController: UIViewController, atIndex: Int) {
        
    }
    
    func slidingContainerViewControllerDidShowSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
    
    func slidingContainerViewControllerDidHideSliderView(_ slidingContainerViewController: SlidingContainerViewController) {
        
    }
  
    @IBAction func btnBackClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TrainingMaterialVC") as! MediProcTrainingMaterialViewController
        self.present(nextViewController, animated:false, completion:nil)
    }
    
}

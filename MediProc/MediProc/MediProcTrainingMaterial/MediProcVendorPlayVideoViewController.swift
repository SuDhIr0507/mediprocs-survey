//
//  MediProcVendorPlayVideoViewController.swift
//  MediProc
//
//  Created by Avante on 30/04/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import youtube_ios_player_helper
import NVActivityIndicatorView

class MediProcVendorPlayVideoViewController: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var Playerview: YTPlayerView!
    var youtubevideourl = NSString()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        startAnimating()
        var videoid = NSString()
        videoid = (YoutubeVideoVC.extractYoutubeId(fromLink: youtubevideourl as String!)! as NSString)
        Playerview.load(withVideoId: videoid as String)
        stopAnimating()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "TrainingMaterialVC") as! MediProcTrainingMaterialViewController
//        self.present(nextViewController, animated:false, completion:nil)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcVendorListVC") as! MediProcVendorListViewController
        nextViewController.type = "Video"
        //UserDefaults.standard.set("Video", forKey: "callDropDown")
        self.present(nextViewController, animated:false, completion:nil)
    }
    

}

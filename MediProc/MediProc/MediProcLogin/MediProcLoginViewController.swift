//
//  MediProcLoginViewController.swift
//  MediProc
//
//  Created by Avante on 28/02/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SkyFloatingLabelTextField
import Alamofire
class MediProcLoginViewController: UIViewController,UITextFieldDelegate,NVActivityIndicatorViewable,CustomPopupAlertDelegate {

    @IBOutlet weak var Loginbgview: UIView!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var textfieldPassword: SkyFloatingLabelTextFieldWithIcon!

    @IBOutlet weak var textfieldUsername: SkyFloatingLabelTextFieldWithIcon!
    
    @IBOutlet weak var btnRegister: UIButton!
    
    @IBOutlet weak var btnForgotPassword: UIButton!
    
    @IBOutlet weak var btnViewPassword: UIButton!
    var alertController = UIAlertController()
    var clicked = Bool()
    var HLLResponsearray = NSArray()
   
    var mobileNum = NSString()
    var mobileNumber = UITextField()
    var newPassword = NSString()
    var otpString = NSString()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        Loginbgview.layer.cornerRadius = 20
        btnLogin.layer.cornerRadius = 20
        
        textfieldUsername.delegate = self
        textfieldPassword.delegate = self
        //mobileNumber.delegate = self
        //
        
//        btnRegister.layer.cornerRadius = 20
//        btnRegister.layer.borderWidth = 1.5
//        btnRegister.layer.borderColor = Color().hexStringToUIColor(hex: "#1561AD").cgColor
        
          textfieldUsername.iconType = .image
          textfieldPassword.iconType = .image
          textfieldUsername.iconImage = UIImage(imageLiteralResourceName: "user1.png")
          textfieldPassword.iconImage = UIImage(imageLiteralResourceName: "password1.png")
        
        
         clicked = false
        
    }
    func addRightTextButon(viewText:UITextField, imagename: String) {
        viewText.leftViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 20, width: 25, height: 25))
        let image = UIImage(named: imagename)
        imageView.image = image
        viewText.leftView = imageView
    }
    func callLoginApi()
    {
        
        if TextFieldValidation().isTextFieldIsEmpty(testStr:textfieldUsername.text!) != true
        {
            Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please enter User ID")
        }
        else if TextFieldValidation().isTextFieldIsEmpty(testStr:textfieldPassword.text!) != true
        {
            Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please enter Password")
        }
        else
        {
             startAnimating()
            let urlString =  WebService.BASE_URL + WebService.USER_LOGIN 
            let jsonObject:[String:String] = ["UserName":textfieldUsername.text!,"Password":textfieldPassword.text!]
            
            Utils().callUrlSession(urlString: urlString, method: "POST", params: jsonObject, completion: { (response) in
                
                print(response)
                let status = response.value(forKey: "status") as? String
                let message = response.value(forKey: "message") as! String
                self.stopAnimating()
                if status == "Success"
                {
                    let array = response.value(forKey: "output") as! NSArray
                    let UserObject = array.firstObject as! NSDictionary
                    print("login response data",UserObject)
                    UserDefaults.standard.set("login", forKey: "status")
                    UserData().saveUserData(userObject:UserObject)

                    DispatchQueue.main.async {
                        self.stopAnimating()
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcHomeVC") as! MediProcHomeViewController
                        self.present(nextViewController, animated:false, completion:nil)
                    }
                }
                else if status == "Fail"
                {
                    DispatchQueue.main.async {

                        self.stopAnimating()
                       // Alert().showAlertMessage(vc:self, titleStr:status!, messageStr: message)
                    }

                   self.HLLCONNECTcallLoginApi()

                }
            })
 
            
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    /*
    override public func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        
        if action == #selector(copy(_:)) ||  {
            return false
        }
        
        return true
    }
    */
//    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        if action == #selector(self.copy(_:)) {
//            return false
//        }
//        if action == #selector(self.cut(_:)) {
//            return false
//        }
//        return super.canPerformAction(action, withSender: sender)
//    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == textfieldUsername  {
            textfieldUsername.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AD"))
        }
        if textField==textfieldPassword {
            textfieldPassword.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AD"))
        }
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        if textField == textfieldUsername  {
            textfieldUsername.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AD"))
        }
        if textField==textfieldPassword {
            textfieldPassword.settextfieldborder(color:Color().hexStringToUIColor(hex: "#1561AD") )
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        textfieldUsername.resignFirstResponder()
        textfieldPassword.resignFirstResponder()
        mobileNumber.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textfieldUsername
        {
            let maxLength = 30
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
        if textField == textfieldPassword
        {
            let maxLength = 20
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
        if textField == alertController.textFields![0]
        {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
              }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 10
        
        }
        return false
        
    }
    
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@","^(?=.*[A-Z])(?=.*[!@#$&*]).{2,}$")
        return passwordTest.evaluate(with: password)
    }
    
    func isPasswordSame(password: String , confirmPassword : String) -> Bool {
        
        if password == confirmPassword{
            return true
        }else{
            return false
        }
    }
    
    @IBAction func btnLoginClicked(_ sender: Any) {
        
        if Reachability().isConnectedToNetwork() == true {
            print("Connected to the internet")
            self.callLoginApi()
        } else {
            print("No internet connection")
            Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please check your internet connection")
        }
    }
    
    func HLLCONNECTcallLoginApi()
    {
        
        if TextFieldValidation().isTextFieldIsEmpty(testStr:textfieldUsername.text!) != true
        {
            Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please enter username")
        }
        else if TextFieldValidation().isTextFieldIsEmpty(testStr:textfieldPassword.text!) != true
        {
            Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please enter password")
        }
        else
        {
            startAnimating()
            let urlString =  WebService.USER_LOGIN_HLLCONNECT
            let jsonObject:[String:String] = ["UserEmail":textfieldUsername.text!,"Password":textfieldPassword.text!]
            
            Utils().callUrlSession(urlString: urlString, method: "POST", params: jsonObject, completion: { (response) in
                
                print(response)
                let status = response.value(forKey: "status") as! String
                let message = response.value(forKey: "message") as! String
                
                if status == "Success"
                {
                    self.HLLResponsearray = (response.value(forKey: "output") as? NSArray)!
                    print("HLLConnect response from api",self.HLLResponsearray)
                    let userObject = self.HLLResponsearray.firstObject as! NSDictionary
                 
                    HLLConnectUserData().HLLConnectsaveUserData(userObject:userObject)
                    self.AddPhleboDetailsApi()
                    DispatchQueue.main.async {
                        
                        self.stopAnimating()
                      
                        
                        //
                        //Alert().showAlertMessage(vc:self, titleStr: "Status", messageStr: "HLLCONNECT Login Successfully")
                        
                        //                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        //                        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "HLLConnectDashboardHomeVC") as! HLLConnectDashboardHomeViewController
                        //                        self.present(nextViewController, animated:false, completion:nil)
                    }
                }
                else if status == "Fail"
                {
                    
                    DispatchQueue.main.async {
                        self.stopAnimating()
                        Alert().showAlertMessage(vc:self, titleStr: status, messageStr: message)
                        
                    }
                }
            })
        }
    }
    func AddPhleboDetailsApi()
    {
       startAnimating()
        
     
        let urlString = WebService.AddPhleboDetails
        let jsonObject:Parameters = ["UserRoleID":"10","FIRSTNAME":HLLConnectUserData().getHLLConnectUserData().FirstName,"MIDNAME":HLLConnectUserData().getHLLConnectUserData().MiddleName,"LASTNAME":HLLConnectUserData().getHLLConnectUserData().LastName,"MobileNo":HLLConnectUserData().getHLLConnectUserData().per_mobile,"EmailId":HLLConnectUserData().getHLLConnectUserData().per_email,"DISTLGDCODE":HLLConnectUserData().getHLLConnectUserData().DISTLGDCODE,"PinCode":"0","UserAddress":HLLConnectUserData().getHLLConnectUserData().PAddress,"CreateionBy":"1"]
      
       
        var jsonStr = NSString()

        do {
            let jsonData = try JSONSerialization.data(withJSONObject:jsonObject, options: [.prettyPrinted])
            if let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) {
                print(jsonString)
                jsonStr = jsonString as NSString
            }
        } catch {
            print(error)
        }
        
      
        print("jsonObject",jsonStr)

        Alamofire.request(urlString, method: .post, parameters:["UserData":jsonStr])
            .responseJSON{ response in
                
            print("AddPhleboDetails Api Response data",response)
                let status = response.response?.statusCode
                if status == 200
                {
                    let idd = response.value
                    print("idd value is ",idd)
                    UserDefaults.standard.set(idd, forKey:"HLLUserID")
                    UserDefaults.standard.set("login", forKey: "status")
                    UserDefaults.standard.set(true,forKey: "IsHLLLogin")
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcHomeVC") as! MediProcHomeViewController
                self.present(nextViewController, animated:false, completion:nil)
                }

        }
    }
    @IBAction func btnViewPasswordClicked(_ sender: Any) {
      
        
        if clicked == false
        {
            clicked = true
            textfieldPassword.isSecureTextEntry = false
            btnViewPassword.setImage(UIImage(named: "PasswordShow.png"), for: .normal)
        }
        else if clicked == true
        {
            clicked = false
            textfieldPassword.isSecureTextEntry = true
            btnViewPassword.setImage(UIImage(named: "Passwordhide.png"), for: .normal)
        }
    
    }
    
    @IBAction func btnRegisterClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcRegisterVC") as! MediProcRegisterViewController
        self.present(nextViewController, animated:false, completion:nil)
    }
    
    @IBAction func btnForgotPasswordClicked(_ sender: Any) {
        
        let popvc = UIStoryboard(name: "Main", bundle:nil).instantiateViewController(withIdentifier: "CustomPopupAlertVC") as! CustomPopupAlertViewController
        self.addChild(popvc)
        popvc.view.frame = self.view.frame
        self.view.addSubview(popvc.view)
        popvc.didMove(toParent: self)
        popvc.delegate = self
        
    }
    
    func didTapFilter(_ sender: CustomPopupAlertViewController, Flag: String) {
        
        
    }
    
    func validateMobile(value: String) -> Bool
    {
        let PHONE_REGEX = "^[6-9][0-9]{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        print("result",result)
        return result
    }
    
}

extension UITextField {
    
func settextfieldborder(color:UIColor) {
    let border = CALayer()
    let width = CGFloat(1.0)
    border.borderColor = color.cgColor
    border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height:  self.frame.size.height)
    border.borderWidth = width
    self.layer.addSublayer(border)
    self.layer.masksToBounds = true
}

func  settextfieldLeftviewmode(image:UIImage)
{
    self.leftViewMode = UITextField.ViewMode.always
    let imageView = UIImageView(frame: CGRect(x:5, y: 5, width: 20, height: 20))
    
    imageView.image = image;
    let view = UIView(frame : CGRect(x:5, y: 0, width: 25, height: 25))
    view.addSubview(imageView)
    self.leftView = view
    
}
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//
//                        guard let textFieldText = textField.text,
//                            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
//                                return false
//                        }
//                        let substringToReplace = textFieldText[rangeOfTextToReplace]
//                        let count = textFieldText.count - substringToReplace.count + string.count
//                        return count <= 10
//
//        //return true
//    }
}

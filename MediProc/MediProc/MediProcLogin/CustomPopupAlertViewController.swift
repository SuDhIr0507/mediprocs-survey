//
//  CustomPopupAlertViewController.swift
//  MediProc
//
//  Created by Avante on 27/08/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
protocol CustomPopupAlertDelegate : class {
    func didTapFilter(_ sender:CustomPopupAlertViewController, Flag:String)
    
}


class CustomPopupAlertViewController: UIViewController,NVActivityIndicatorViewable,UITextFieldDelegate {

    weak var delegate: CustomPopupAlertDelegate?
    @IBOutlet weak var bgview: UIView!
    @IBOutlet weak var ResetPasswordview: UIView!
    @IBOutlet weak var textfieldOTP: UITextField!
    @IBOutlet weak var textfieldNewPassword: UITextField!
    @IBOutlet weak var textfieldConfirmPassword: UITextField!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnResendOTP: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var ForgotPasswordview: UIView!
  
    @IBOutlet weak var btnCancelForgotPassword: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var textfieldMobileNumber: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        bgview?.backgroundColor = UIColor.black.withAlphaComponent(0.5)  //UIColor(white: 1, alpha: 0.6)
        
        ForgotPasswordview.layer.shadowColor = UIColor.black.cgColor
        ForgotPasswordview.layer.shadowOpacity = 1
        ForgotPasswordview.layer.shadowOffset = CGSize.zero
        ForgotPasswordview.layer.shadowRadius = 3
        ForgotPasswordview.layer.cornerRadius = 10
        
//        btnSelectType.layer.borderWidth = 0.5
//        btnSelectType.layer.borderColor = UIColor.lightGray.cgColor
//        btnSelectType.layer.cornerRadius = 10
        
//        textfieldMobileNumber.layer.cornerRadius = 10
//        textfieldOTP.layer.cornerRadius = 10
//        textfieldNewPassword.layer.cornerRadius = 10
//        textfieldConfirmPassword.layer.cornerRadius = 10
        
        textfieldMobileNumber.layer.borderWidth = 0.5
        textfieldMobileNumber.layer.borderColor = UIColor.lightGray.cgColor
        textfieldOTP.layer.borderWidth = 0.5
        textfieldOTP.layer.borderColor = UIColor.lightGray.cgColor
        textfieldNewPassword.layer.borderWidth = 0.5
        textfieldNewPassword.layer.borderColor = UIColor.lightGray.cgColor
        textfieldConfirmPassword.layer.borderWidth = 0.5
        textfieldConfirmPassword.layer.borderColor = UIColor.lightGray.cgColor
        
        btnDone.layer.cornerRadius = 10
        btnResendOTP.layer.cornerRadius = 10
        btnCancel.layer.cornerRadius = 10
        btnSend.layer.cornerRadius = 10
        btnCancelForgotPassword.layer.cornerRadius = 10
        
        ResetPasswordview.layer.shadowColor = UIColor.black.cgColor
        ResetPasswordview.layer.shadowOpacity = 1
        ResetPasswordview.layer.shadowOffset = CGSize.zero
        ResetPasswordview.layer.shadowRadius = 3
        ResetPasswordview.layer.cornerRadius = 10
        
        ResetPasswordview.isHidden = true
        ForgotPasswordview.isHidden = false
        
        showAnimate()
    }
    
    @IBAction func btnDoneClicked(_ sender: Any) {
       
        if Reachability().isConnectedToNetwork() {
            print("Yes! internet is available.")
            
             self.callResetPasswordApi()
        }
        else
        {
            let alert = UIAlertController(title: "Error", message: "Please check your internet connection", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func btnResendOTpClicked(_ sender: Any) {
        
        if Reachability().isConnectedToNetwork() {
            print("Yes! internet is available.")
            
            self.callForgotPasswordApi()
        }
        else
        {
            let alert = UIAlertController(title: "Error", message: "Please check your internet connection", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        
        self.ResetPasswordview.isHidden = true
        self.ForgotPasswordview.isHidden = true
       
        removeAnimate()
        
    }
    
    @IBAction func btnSendClicked(_ sender: Any) {
        
        if Reachability().isConnectedToNetwork() {
            print("Yes! internet is available.")
            if self.validateMobile(value: textfieldMobileNumber.text!) == false
            {
                let alert = UIAlertController(title: "Warning", message: "Please enter valid mobile number", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                callForgotPasswordApi()
            }
        }
        else
        {
            let alert = UIAlertController(title: "Error", message: "Please check your internet connection", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
        
        
    }
    
    
    @IBAction func btnCancelForgotPasswordClicked(_ sender: Any) {
        self.ResetPasswordview.isHidden = true
        self.ForgotPasswordview.isHidden = true
        removeAnimate()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textfieldConfirmPassword
        {
            if self.isPasswordSame(password: textfieldNewPassword.text!, confirmPassword: textfieldConfirmPassword.text!)
            {
                textfieldConfirmPassword.resignFirstResponder()
            }
            else
            {
            let errorAlert = UIAlertController(title: "Error", message: "Please enter correct password", preferredStyle: .alert)
             errorAlert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(errorAlert, animated: true, completion: nil)
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        textfieldOTP.resignFirstResponder()
        textfieldNewPassword.resignFirstResponder()
        
        textfieldMobileNumber.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textfieldOTP
        {
            let maxLength = 6
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
        if textField == textfieldNewPassword
        {
            let maxLength = 16
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
        if textField == textfieldConfirmPassword
        {
            let maxLength = 16
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
        if textField == textfieldMobileNumber
        {
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 10
            
        }
        return false
        
    }
    func callForgotPasswordApi()
    {
        let urlString = WebService.FORGOT_PASSWORD
        let jsonObject:[String:String] = ["MobileNo":textfieldMobileNumber.text!]
        
        Alamofire.request(urlString, method: .post, parameters:jsonObject)
            .responseJSON{ response in
                print(response)
                let result: AnyObject = response.result.value as AnyObject
                
                if result is String
                {
                    print("result is",result)
                    let str = ("\(result)")
                    if str == "Optional(OTP send successfully)" || str == "OTP send successfully"
                    {
                        
                        self.ResetPasswordview.isHidden = false
                        self.ForgotPasswordview.isHidden = true
                      
                    }
                    
                }
                else if result is NSDictionary
                {
                    let msg = result.value(forKey: "Message") as! String
                    Alert().showAlertMessage(vc:self, titleStr: "Error", messageStr: msg)
                }
                self.stopAnimating()
                
                //            let status = response.value(forKey: "status") as! String
                //            let message = response.value(forKey: "message") as! String
                //
                //            //
                //            if status == "Success" || status == "success"
                //            {
                //                DispatchQueue.main.async {
                //
                //                }
                //            }
                //            else if status == "Fail" || status == "fail" || status == "OTP not send"
                //            {
                //
                //                Alert().showAlertMessage(vc:self, titleStr: "Error", messageStr: message)
                //            }
                
                
                
        }
    }
    func callResetPasswordApi()
    {
        startAnimating()
        let urlString = WebService.RESET_PASSWORD
        let jsonObject:[String:String] = ["MobileNo":textfieldMobileNumber.text!,"Otp":textfieldOTP.text!,"Password":textfieldNewPassword.text!]
        print("params are",jsonObject)
        Alamofire.request(urlString, method: .post, parameters:jsonObject)
            .responseJSON{ response in
                self.stopAnimating()
                print(response)
                let result: Any = response.result.value
                if result is String
                {
                    
                    Alert().showAlertMessage(vc:self, titleStr:"Status", messageStr: result as! String)
                    DispatchQueue.main.async {
                        self.stopAnimating()
                        self.ResetPasswordview.isHidden = true
                        self.ForgotPasswordview.isHidden = true
                        self.removeAnimate()
                    }
                    
                }
                
                if result is NSDictionary
                {
                    let message = (result as AnyObject).value(forKey: "Message") as! String
                    Alert().showAlertMessage(vc:self, titleStr:"Status", messageStr: message as! String)
                    DispatchQueue.main.async {
                        self.stopAnimating()
                        //self.ResetPasswordview.isHidden = true
                        //self.ForgotPasswordview.isHidden = true
                        
                        //self.removeAnimate()
                    }
                }
               
        }
        
    }
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@","^(?=.*[A-Z])(?=.*[!@#$&*]).{2,}$")
        return passwordTest.evaluate(with: password)
    }
    
    func isPasswordSame(password: String , confirmPassword : String) -> Bool {
        
        if password == confirmPassword{
            return true
        }else{
            return false
        }
    }
    
    func validateMobile(value: String) -> Bool
    {
        let PHONE_REGEX = "^[6-9][0-9]{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        print("result",result)
        return result
    }
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0
        }, completion: {(finished : Bool) in
            if(finished)
            {
                self.willMove(toParent: nil)
                self.view.removeFromSuperview()
                self.removeFromParent()
            }
        })
    }
}

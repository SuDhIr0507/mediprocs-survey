//
//  MediProcLabCheckListInfoViewController.swift
//  MediProc
//
//  Created by Avante on 10/04/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import FZAccordionTableView
import Alamofire
import CoreData

class MediProcLabCheckListInfoViewController: UIViewController,NVActivityIndicatorViewable,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    let moContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var tableviewLabCheckList: UITableView!
    @IBOutlet weak var btnCheckInfo: UIButton!
    @IBOutlet weak var btnLabInfo: UIButton!
    
    var LabsubQuesArr = [AnyObject]()
    var sections = [Category]()
    let items = [Any]()
    var selectedIndx = -1
    var thereIsCellTapped = false
    var radiobutton = NSString()
    var LabanswerArray = [AnyObject]()
    var LabsavedData = [AnyObject]()
    var didselectedIndx = Int()
    var object = NSDictionary()
    var Labquestionobject = NSDictionary()
    var LabSubquestionobject = NSDictionary()
    var Labsubquestionarray = [AnyObject]()
    var Labfinalsubquestionarray = [AnyObject]()
    var Labfinaljsonsubquestionarray = [AnyObject]()
    var LabcheckListArray = [AnyObject]()
    var Labmainquestion = String()
    var LabcheckIdHeader = Int()
    var labObject = [String:Any]()
    var MainArray = [AnyObject]()
    var subArray = [AnyObject]()
    
    @IBOutlet weak var btnSaveNext: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        applyGradient()
        
        tableviewLabCheckList.delegate = self
        tableviewLabCheckList.dataSource = self
        
        btnSaveNext.layer.cornerRadius = 10
        
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x:btnCheckInfo.frame.origin.x , y: btnCheckInfo.frame.height + 1, width:btnCheckInfo.frame.width
            , height:1.0)
        bottomBorder.backgroundColor = Color().hexStringToUIColor(hex:"#0060C5").cgColor
        btnCheckInfo.layer.addSublayer(bottomBorder)
        
        let bottomBorder1 = CALayer()
        bottomBorder1.frame = CGRect(x:btnLabInfo.frame.origin.x, y: btnLabInfo.frame.height + 1, width:btnLabInfo.frame.width
            , height:1.0)
        bottomBorder1.backgroundColor = UIColor.white.cgColor
        btnLabInfo.layer.addSublayer(bottomBorder1)
        
        tableviewLabCheckList.backgroundColor = Color().hexStringToUIColor(hex:"#F7F7F7")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability().isConnectedToNetwork() == true {
            print("Connected to the internet")
            
            let checklistarray = UserData().getCheckListArrayData().globalCheckListArray
            print(checklistarray)
            if checklistarray.count != 0 {
                LabcheckListArray = checklistarray
                GetAllChecklistSubQuestionsApi()
            }
            else{
                WebserviceCall().getCheckListQuestions({ Array in
                    self.LabcheckListArray = Array as [AnyObject]
                    self.GetAllChecklistSubQuestionsApi()
                })
            }
            
        } else {
            
            print("No internet connection")
            
            let checklistarray = UserData().getCheckListArrayData().globalCheckListArray
            if checklistarray.count != 0 {
                LabcheckListArray = checklistarray
                GetAllChecklistSubQuestionsApi()
            }
            else{
                Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please Check Your Internet Connection")
            }
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func btnLabInfoClicked(_ sender: Any) {

    }
    @IBAction func btnCheckInfoClicked(_ sender: Any) {
        
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcHomeVC") as! MediProcHomeViewController
        self.present(nextViewController, animated:false, completion:nil)
    }
    
    func applyGradient() {
        
        let gradient = CAGradientLayer()
        let hexclass = HexFromColorCodeClass()
        
        let Start = hexclass.hexStringToUIColor(hex:"#0060C5").cgColor
        let End = hexclass.hexStringToUIColor(hex:"#2BB1FE").cgColor
        
        gradient.colors = [Start,End]
        gradient.locations = [0.0,0.5,1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.frame = CGRect(origin: CGPoint.zero, size: self.view.frame.size)
        self.view.layer.insertSublayer(gradient, at:0)
    }
    
    func GetAllChecklistSubQuestionsApi()
    {
                
                for dict in self.LabcheckListArray{
                    
                    self.subArray.removeAll()
                    let subQesArray = dict.value(forKey: "SubQuestion") as! [AnyObject]
                    let Checkid = dict.value(forKey: "Checkid")
                    let Material = dict.value(forKey: "Material")
                    
                    for dictSub in subQesArray{
                        
                        let Checkid = (dictSub as AnyObject).value(forKey: "Checkid")
                        let QuesType = (dictSub as AnyObject).value(forKey: "QuesType") as! String
                        let Question = (dictSub as AnyObject).value(forKey: "Question")
                        let Subcheckid = (dictSub as AnyObject).value(forKey: "Subcheckid")
                        let Subid = (dictSub as AnyObject).value(forKey: "Subid")
                        let IsRemarkNeeded = (dictSub as AnyObject).value(forKey: "IsRemarkNeeded")
                        
                        var subDictionary = NSDictionary()
                        
                        if QuesType == "Textbox"{
                            
                            subDictionary = ["Checkid":Checkid!,"QuesType":QuesType,"Question":Question!,"Subcheckid":Subcheckid!,"Subid":Subid!,"subQueAns":"","IsRemarkNeeded":IsRemarkNeeded as Any]
                        }
                        else if QuesType == "RadioButton"{
                            
                            subDictionary = ["Checkid":Checkid!,"QuesType":QuesType,"Question":Question!,"Subcheckid":Subcheckid!,"Subid":Subid!,"subQueAns":"No","IsRemarkNeeded":IsRemarkNeeded as Any]
                        }
                        
                        self.subArray.append(subDictionary as AnyObject)
                    }
                    print(self.subArray)
                    
                    let dictionary = ["SubQuestion":self.subArray,"Checkid":Checkid!,"Material":Material!,"mainQueAns":""]
                    
                    self.MainArray.append(dictionary as AnyObject)
                    
                    print(self.MainArray)
                }
                
                print(self.MainArray)
                
                for (index, dict) in self.MainArray.enumerated()
                {
                    let str = dict.value(forKey:"Material") as! String
                    let ceckId = dict.value(forKey:"Checkid") as! Int
                    let title = ("\(index + 1)\(". ")\(str)")
                    //let title = ("\(ceckId)\(". ")\(str)")
                    let subQesArray = dict.value(forKey: "SubQuestion")
                    
                    self.sections.append(contentsOf:[Category(title:title, checkId:ceckId, items:subQesArray as! [[String : Any]])])
                }
        
                self.tableviewLabCheckList.reloadData()
            
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //        if (self.selectedIndx == section) {
        let items = self.sections[section].items
        return items.count
        //        } else {
        //            return 0;
        //        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if indexPath.section == selectedIndx && thereIsCellTapped{
            
            return 100
            
        }else{
            return 0
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "LabSectionTableViewCell") as!LabSectionTableViewCell
        headerCell.lblHeader.text = self.sections[section].title
        headerCell.checkId = self.sections[section].checkId
        headerCell.btnYes.tag = section
        headerCell.btnYes.addTarget(self, action: #selector(MediProcLabCheckListInfoViewController.btnYesClick(sender:)), for: .touchUpInside)
        headerCell.btnNo.tag = section
        headerCell.btnNo.addTarget(self, action: #selector(MediProcLabCheckListInfoViewController.btnNoClick(sender:)), for: .touchUpInside)
        print("selected index is",selectedIndx)
        
        if thereIsCellTapped == true{
            
            Labmainquestion = headerCell.lblHeader.text!
            LabcheckIdHeader = headerCell.checkId
            
        }
        
        if section == selectedIndx
        {
            headerCell.btnYes.setImage( UIImage(named: "radio-btn2.png"), for:.normal)
            headerCell.btnNo.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        }
            
        else
        {
            headerCell.btnNo.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
            headerCell.btnYes.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        }
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabExpandeTableViewCell") as! LabExpandeTableViewCell
        
        let items = self.sections[indexPath.section].items
        let item = items[indexPath.row]
        let type = item["QuesType"] as? String
        // print("type",item)
          let subindex = selectedIndx+1
          cell.selectionStyle = .none
        
        if type == "Textbox"{
            
            cell.bgtxtView.isHidden = false
            cell.bgRadiobtView.isHidden = true
            cell.textfieldAnswer.delegate = self
            
            let toolBar = UIToolbar()
            toolBar.barStyle = UIBarStyle.default
            toolBar.isTranslucent = true
            let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcLabCheckListInfoViewController.doneDatePickerPressed))
            
            toolBar.setItems([space,doneButton], animated: false)
            toolBar.isUserInteractionEnabled = true
            toolBar.sizeToFit()
            cell.textfieldAnswer.inputAccessoryView = toolBar
            
            let items = self.sections[indexPath.section].items
            let item = items[indexPath.row]
           // item["Checkid"] as? NSNumber ?? 0
            cell.lblSrNotextfield.text = ("\(subindex)") + "." + ("\(item["Subid"] as? NSNumber ?? 0)") + " "
            cell.lblCell.text = ("\(item["Question"] as? String ?? " ")")
            cell.textfieldAnswer.text = ("\(item["subQueAns"] as? String ?? " ")")
            cell.textfieldAnswer.tag = indexPath.row
            cell.textfieldAnswer.keyboardType = .default

            cell.textfieldAnswer.addTarget(self, action: #selector(MediProcLabCheckListInfoViewController.textFieldDidChange(sender:)), for:.editingDidEnd)
            
        }
            
        else if type == "RadioButton"{
            
            cell.bgtxtView.isHidden = true
            cell.bgRadiobtView.isHidden = false
            let items = self.sections[indexPath.section].items
            let item = items[indexPath.row]
            
            let subID = item["Subid"] as? NSNumber
            
            if subID == 5 || subID == 6{
                cell.txtRemark.isHidden = false
            }
            else if subID == 1 {
                cell.txtRemark.isHidden = true
            }
            
            //item["Checkid"] as? NSNumber ?? 0
            cell.lblSrNoRadio.text = ("\(subindex)") + "." + ("\(item["Subid"] as? NSNumber ?? 0)") + " "
            cell.lblCell2.text = ("\(item["Question"] as? String ?? "")")
            let radioCount = ("\(item["subQueAns"] as? String ?? "")")
            cell.txtRemark.text = ("\(item["Remark"] as? String ?? "")")
            cell.txtRemark.tag = indexPath.row
            
            if radioCount == "Yes"{
                cell.btnYes.setImage(UIImage(named: "radio-btn2.png"), for:.normal)
                cell.btnNo.setImage(UIImage(named: "radio-btn1.png"), for: .normal)
                cell.txtRemark.isUserInteractionEnabled = true
            }
            else if radioCount == "No"{
                cell.btnYes.setImage(UIImage(named: "radio-btn1.png"), for:.normal)
                cell.btnNo.setImage(UIImage(named: "radio-btn2.png"), for: .normal)
                cell.txtRemark.isUserInteractionEnabled = false
            }
            
            cell.btnYes.tag = indexPath.row
            cell.btnYes.addTarget(self, action: #selector(MediProcLabCheckListInfoViewController.btnYesClicked(sender:)), for: .touchUpInside)
            cell.btnNo.tag = indexPath.row
            cell.btnNo.addTarget(self, action: #selector(MediProcLabCheckListInfoViewController.btnNoClicked(sender:)), for: .touchUpInside)
            
            cell.txtRemark.addTarget(self, action: #selector(MediProcLabCheckListInfoViewController.textFieldDidChangeRemark(sender:)), for:.editingDidEnd)
        }
        
        return cell
    }
    
    @objc func doneDatePickerPressed(){
        self.view.endEditing(true)
        
    }
    
    @objc func textFieldDidChange(sender: UITextField) {
        
        let index = IndexPath(row: sender.tag, section: selectedIndx)
        let cell: LabExpandeTableViewCell = self.tableviewLabCheckList.cellForRow(at: index) as! LabExpandeTableViewCell
        print(index)
        
        Labmainquestion = self.MainArray[selectedIndx]["Material"] as! String
        LabcheckIdHeader = self.MainArray[selectedIndx]["Checkid"] as! Int
        
        var checkListQueArray = [AnyObject]()
        checkListQueArray = self.sections[selectedIndx].items as [AnyObject]
        
        let Subcheckid = checkListQueArray[sender.tag]["Subcheckid"] as? NSNumber
        let Subid = checkListQueArray[sender.tag]["Subid"] as? NSNumber
        let checkId = checkListQueArray[sender.tag]["Checkid"] as? Int
        
        let tempArray = self.MainArray
        
        self.MainArray.removeAll()
        
        for dict in tempArray {
            
            self.subArray.removeAll()
            let Checkiddd = dict.value(forKey: "Checkid") as? Int
            
            if Checkiddd == LabcheckIdHeader {
                
                let Checkid = dict.value(forKey: "Checkid") as? Int
                let Material = dict.value(forKey: "Material")
                let subQesArray = dict.value(forKey: "SubQuestion") as! [AnyObject]
                
                for dictSub in subQesArray{
                    
                    let Subcheckiddd = (dictSub as AnyObject).value(forKey: "Subid") as? NSNumber
                    
                    if Subid == Subcheckiddd {
                        
                        let QuesType = (dictSub as AnyObject).value(forKey: "QuesType")
                        let Question = (dictSub as AnyObject).value(forKey: "Question")
                        let Subcheckid = (dictSub as AnyObject).value(forKey: "Subcheckid")
                        let Subid = (dictSub as AnyObject).value(forKey: "Subid")
                        
                        let subDictionary = ["QuesType":QuesType!,"Question":Question!,"Subcheckid":Subcheckid!,"Subid":Subid!,"subQueAns":cell.textfieldAnswer.text!,"IsRemarkNeeded":0 as Int, "Remark":""]
                        
                        self.subArray.append(subDictionary as AnyObject)
                    }
                    else{
                        
                        self.subArray.append(dictSub as AnyObject)
                        
                    }
                }
                
                let dictionary = ["SubQuestion":self.subArray,"Checkid":Checkid!,"Material":Material!,"mainQueAns":"Yes"]
                
                self.MainArray.append(dictionary as AnyObject)
                
            }
            else{
                
                self.MainArray.append(dict as AnyObject)
            }
        }
        
        print(self.MainArray)
        self.sections.removeAll()
        
        for (index, dict) in self.MainArray.enumerated()
        {
            let str = dict.value(forKey:"Material") as! String
            let ceckId = dict.value(forKey:"Checkid") as! Int
            //index + 1
            let title = ("\(index + 1)\(". ")\(str)")  // ("\(ceckId)\(". ")\(str)")
            let subQesArray = dict.value(forKey: "SubQuestion")
            
            self.sections.append(contentsOf:[Category(title:title, checkId:ceckId, items:subQesArray as! [[String : Any]])])
        }
        
        self.tableviewLabCheckList.reloadData()
        
        
        appendDataIntoArray(sectionindex: selectedIndx, mainquestion: Labmainquestion, subsectionindex: sender.tag, subquestion: cell.lblCell.text!, subAnswer:cell.textfieldAnswer.text!, type:"Textbox", subId: Subid as! Int, subcheckId: Subcheckid as! Int, checkId: checkId!)
        
    }
    
    @objc func textFieldDidChangeRemark(sender: UITextField) {
        
        self.view.endEditing(true)
        
        let index = IndexPath(row: sender.tag, section: selectedIndx)
        let cell: LabExpandeTableViewCell = self.tableviewLabCheckList.cellForRow(at: index) as! LabExpandeTableViewCell
        print(index)
        
        Labmainquestion = self.MainArray[selectedIndx]["Material"] as! String
        LabcheckIdHeader = self.MainArray[selectedIndx]["Checkid"] as! Int
        
        var checkListQueArray = [AnyObject]()
        checkListQueArray = self.sections[selectedIndx].items as [AnyObject]
        print("checkListQueArray is ",checkListQueArray)
        
        let Subcheckid = checkListQueArray[sender.tag]["Subcheckid"] as? NSNumber
        let Subid = checkListQueArray[sender.tag]["Subid"] as? NSNumber
        let checkId = checkListQueArray[sender.tag]["Checkid"] as? Int
        
        let tempArray = self.MainArray
        
        self.MainArray.removeAll()
        
        for dict in tempArray {
            
            self.subArray.removeAll()
            let Checkiddd = dict.value(forKey: "Checkid") as? Int
            
            if Checkiddd == LabcheckIdHeader {
                
                let Checkid = dict.value(forKey: "Checkid") as? Int
                let Material = dict.value(forKey: "Material")
                let subQesArray = dict.value(forKey: "SubQuestion") as! [AnyObject]
                
                for dictSub in subQesArray{
                    
                    let Subcheckiddd = (dictSub as AnyObject).value(forKey: "Subid") as? NSNumber
                    
                    if Subid == Subcheckiddd {
                       
                        let QuesType = (dictSub as AnyObject).value(forKey: "QuesType")
                        let Question = (dictSub as AnyObject).value(forKey: "Question")
                        let Subcheckid = (dictSub as AnyObject).value(forKey: "Subcheckid")
                        let Subid = (dictSub as AnyObject).value(forKey: "Subid")
                        print(cell.txtRemark.text as Any)
                        let subDictionary = ["QuesType":QuesType!,"Question":Question!,"Subcheckid":Subcheckid!,"Subid":Subid!,"subQueAns":radiobutton as String,"IsRemarkNeeded":1 as Int, "Remark":cell.txtRemark.text!]
                        
                        self.subArray.append(subDictionary as AnyObject)
                    }
                    else{
                        
                        self.subArray.append(dictSub as AnyObject)
                        
                    }
                }
                
                let dictionary = ["SubQuestion":self.subArray,"Checkid":Checkid!,"Material":Material!,"mainQueAns":"Yes"]
                
                self.MainArray.append(dictionary as AnyObject)
                
            }
            else{
                
                self.MainArray.append(dict as AnyObject)
            }
        }
        
        self.sections.removeAll()
        
        for (index, dict) in self.MainArray.enumerated()
        {
            let str = dict.value(forKey:"Material") as! String
            let ceckId = dict.value(forKey:"Checkid") as! Int
            //index + 1
            let title = ("\(index+1)\(". ")\(str)")  // ("\(ceckId)\(". ")\(str)")
            let subQesArray = dict.value(forKey: "SubQuestion")
            
            self.sections.append(contentsOf:[Category(title:title, checkId:ceckId, items:subQesArray as! [[String : Any]])])
        }
        
        self.tableviewLabCheckList.reloadData()
        
        
    }
    
    func appendDataIntoArray(sectionindex:Int,mainquestion:String,subsectionindex:Int,subquestion:String,subAnswer:String, type:String, subId:Int, subcheckId:Int, checkId:Int)
    {
        
        Labfinalsubquestionarray.removeAll()
        
        for dict in self.LabcheckListArray
        {
            
            let checkIddd = dict.value(forKey: "Checkid") as! Int
            
            if LabcheckIdHeader == checkIddd{
                
                LabsubQuesArr = dict.value(forKey: "SubQuestion") as! [AnyObject]
                
                for dict2 in self.LabsubQuesArr
                {
                    let subI = dict2.value(forKey: "Subid") as! Int
                    
                    if subI == subId{
                        
                        if type == "Textbox"{
                            
                            object = ["QuesType":"Textbox",
                                      "Question":subquestion,
                                      "Subcheckid":subcheckId,
                                      "Subid":subId,
                                      "subQueAns":subAnswer
                            ]
                        }
                        else if type == "RadioButton" {
                            
                            object = ["QuesType":"RadioButton",
                                      "Question":subquestion,
                                      "Subcheckid":subcheckId,
                                      "Subid":subId,
                                      "subQueAns":subAnswer
                            ]
                        }
                        
                        if Labsubquestionarray.count > 0 {
                            
                            // for dict in subquestionarray{
                            
                            
                            //  print("array count of removing duplicate",subquestionarray.count)
                            
                            for (index,element) in Labfinalsubquestionarray.enumerated()
                            {
                                let sub = element.value(forKey: "Subid") as! Int
                                if sub == subId {
                                    // print("index value",subquestionarray)
                                    Labsubquestionarray.remove(at:index)
                                }
                                else{
                                    
                                }
                                //}
                                
                                //}
                                
                                
                            }
                        }
                        
                        Labsubquestionarray.append(object)
                        
                        break
                    }
                }
                
                let checkId = dict.value(forKey: "Checkid")
                Labquestionobject = ["Checkid":checkId as Any, "Material":mainquestion, "SubQuestion":Labsubquestionarray]
                Labfinalsubquestionarray.append(Labquestionobject)
                //print("finalsubquestionarray data",finalsubquestionarray)
                
                break
            }
            
        }
        
        
    }
    @objc func btnYesClicked(sender:UIButton!){
        
        let tappedIndexPath = IndexPath(row: sender.tag, section:selectedIndx)
        let cell = tableviewLabCheckList.cellForRow(at:tappedIndexPath) as! LabExpandeTableViewCell
        cell.btnYes.setImage(UIImage(named: "radio-btn2.png"), for:.normal)
        cell.btnNo.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        radiobutton = "Yes"
        cell.txtRemark.isUserInteractionEnabled = true
        
        Labmainquestion = self.MainArray[selectedIndx]["Material"] as! String
        LabcheckIdHeader = self.MainArray[selectedIndx]["Checkid"] as! Int
        
        var checkListQueArray = [AnyObject]()
        checkListQueArray = self.sections[selectedIndx].items as [AnyObject]
        let Subcheckid = checkListQueArray[sender.tag]["Subcheckid"] as? NSNumber
        let Subid = checkListQueArray[sender.tag]["Subid"] as? NSNumber
        let checkId = checkListQueArray[sender.tag]["Checkid"] as? Int
        
        ////////////////////////////////////////////////////////
        
        let tempArray = self.MainArray
        
        self.MainArray.removeAll()
        
        for dict in tempArray {
            
            self.subArray.removeAll()
            let Checkiddd = dict.value(forKey: "Checkid") as? Int
            
            if Checkiddd == LabcheckIdHeader {
                
                let Checkid = dict.value(forKey: "Checkid") as? Int
                let Material = dict.value(forKey: "Material")
                let subQesArray = dict.value(forKey: "SubQuestion") as! [AnyObject]
                
                for dictSub in subQesArray{
                    
                    let Subcheckiddd = (dictSub as AnyObject).value(forKey: "Subid") as? NSNumber
                    
                    if Subid == Subcheckiddd {
                        
                        let QuesType = (dictSub as AnyObject).value(forKey: "QuesType")
                        let Question = (dictSub as AnyObject).value(forKey: "Question")
                        let Subcheckid = (dictSub as AnyObject).value(forKey: "Subcheckid")
                        let Subid = (dictSub as AnyObject).value(forKey: "Subid") as? NSNumber
                        
                        var subDictionary = NSDictionary()
                        
                        if Subid == 5 || Subid == 6{
                            subDictionary = ["QuesType":QuesType!,"Question":Question!,"Subcheckid":Subcheckid!,"Subid":Subid!,"subQueAns":radiobutton as String,"IsRemarkNeeded":1 as Int, "Remark":cell.txtRemark.text!]
                        }
                        else if Subid == 1{
                            subDictionary = ["QuesType":QuesType!,"Question":Question!,"Subcheckid":Subcheckid!,"Subid":Subid!,"subQueAns":radiobutton as String,"IsRemarkNeeded":0 as Int, "Remark":""]
                        }
                        
                        self.subArray.append(subDictionary as AnyObject)
                    }
                    else{
                        
                        self.subArray.append(dictSub as AnyObject)
                        
                    }
                }
                
                let dictionary = ["SubQuestion":self.subArray,"Checkid":Checkid!,"Material":Material!,"mainQueAns":"Yes"]
                
                self.MainArray.append(dictionary as AnyObject)
                
            }
            else{
                
                self.MainArray.append(dict as AnyObject)
            }
        }
        
        print(self.MainArray)
        self.sections.removeAll()
        
        for (index, dict) in self.MainArray.enumerated()
        {
            let str = dict.value(forKey:"Material") as! String
            let ceckId = dict.value(forKey:"Checkid") as! Int
            //index + 1
            let title = ("\(index + 1)\(". ")\(str)")  // ("\(ceckId)\(". ")\(str)")
            let subQesArray = dict.value(forKey: "SubQuestion")
            
            self.sections.append(contentsOf:[Category(title:title, checkId:ceckId, items:subQesArray as! [[String : Any]])])
        }
        
        self.tableviewLabCheckList.reloadData()
        
        ////////////////////////////////////////////////////////
        
//        appendDataIntoArray(sectionindex: selectedIndx, mainquestion: Labmainquestion, subsectionindex: sender.tag, subquestion: cell.lblCell2.text!, subAnswer:radiobutton as String, type:"RadioButton", subId: Subid as! Int, subcheckId: Subcheckid as! Int, checkId: checkId!)
        
    }
    
    @objc func btnNoClicked(sender:UIButton!){
        
        let tappedIndexPath = IndexPath(row: sender.tag, section:selectedIndx)
        let cell = tableviewLabCheckList.cellForRow(at:tappedIndexPath) as! LabExpandeTableViewCell
        cell.btnNo.setImage(UIImage(named: "radio-btn2.png"), for:.normal)
        cell.btnYes.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        radiobutton = "No"
        cell.txtRemark.text = ""
        cell.txtRemark.isUserInteractionEnabled = false
        
        LabcheckIdHeader = self.MainArray[selectedIndx]["Checkid"] as! Int
        
        var checkListQueArray = [AnyObject]()
        checkListQueArray = self.sections[selectedIndx].items as [AnyObject]
        let Subcheckid = checkListQueArray[sender.tag]["Subcheckid"] as? NSNumber
        let Subid = checkListQueArray[sender.tag]["Subid"] as? NSNumber
        let checkId = checkListQueArray[sender.tag]["Checkid"] as? Int
        
        ////////////////////////////////////////////////////////
        
        let tempArray = self.MainArray
        
        self.MainArray.removeAll()
        
        for dict in tempArray {
            
            self.subArray.removeAll()
            let Checkiddd = dict.value(forKey: "Checkid") as? Int
            
            if Checkiddd == LabcheckIdHeader {
                
                let Checkid = dict.value(forKey: "Checkid") as? Int
                let Material = dict.value(forKey: "Material")
                let subQesArray = dict.value(forKey: "SubQuestion") as! [AnyObject]
                
                for dictSub in subQesArray{
                    
                    let Subcheckiddd = (dictSub as AnyObject).value(forKey: "Subid") as? NSNumber
                    
                    if Subid == Subcheckiddd {
                        
                        let QuesType = (dictSub as AnyObject).value(forKey: "QuesType")
                        let Question = (dictSub as AnyObject).value(forKey: "Question")
                        let Subcheckid = (dictSub as AnyObject).value(forKey: "Subcheckid")
                        let Subid = (dictSub as AnyObject).value(forKey: "Subid")
                        
                        let subDictionary = ["QuesType":QuesType!,"Question":Question!,"Subcheckid":Subcheckid!,"Subid":Subid!,"subQueAns":radiobutton as String,"IsRemarkNeeded":0 as Int, "Remark":""]
                        
                        self.subArray.append(subDictionary as AnyObject)
                    }
                    else{
                        
                        self.subArray.append(dictSub as AnyObject)
                        
                    }
                }
                
                let dictionary = ["SubQuestion":self.subArray,"Checkid":Checkid!,"Material":Material!,"mainQueAns":"Yes"]
                
                self.MainArray.append(dictionary as AnyObject)
                
            }
            else{
                
                self.MainArray.append(dict as AnyObject)
            }
        }
        
        print(self.MainArray)
        self.sections.removeAll()
        
        for (index, dict) in self.MainArray.enumerated()
        {
            let str = dict.value(forKey:"Material") as! String
            let ceckId = dict.value(forKey:"Checkid") as! Int
            //index + 1
            let title = ("\(index + 1)\(". ")\(str)")  // ("\(ceckId)\(". ")\(str)")
            let subQesArray = dict.value(forKey: "SubQuestion")
            
            self.sections.append(contentsOf:[Category(title:title, checkId:ceckId, items:subQesArray as! [[String : Any]])])
        }
        
        self.tableviewLabCheckList.reloadData()
        
        ////////////////////////////////////////////////////////
        
        
//        appendDataIntoArray(sectionindex: selectedIndx, mainquestion: Labmainquestion, subsectionindex: sender.tag, subquestion: cell.lblCell2.text!, subAnswer:radiobutton as String, type:"RadioButton", subId: Subid as! Int, subcheckId: Subcheckid as! Int, checkId: checkId!)
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("selected row indexpath",indexPath.section)
        didselectedIndx = indexPath.section
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.tableView(tableviewLabCheckList, heightForRowAt: indexPath)
    }
    @objc func btnYesClick(sender:UIButton!)
    {
        
        let tappedIndexPath = IndexPath(row:sender.tag, section:didselectedIndx)
        print("selected index of btn YES",tappedIndexPath)
        
        let MainCheckid = self.LabcheckListArray[didselectedIndx]["Material"] as! String
        // print("didselectindex",didselectedIndx)
        
        if Labfinalsubquestionarray.count > 0 {
            
            for (index,element) in Labfinalsubquestionarray.enumerated()
            {
                let sub = element.value(forKey:"Material") as! String
                if sub == MainCheckid {
                    //print("array count of removing duplicate",finalsubquestionarray)
                    Labfinalsubquestionarray.remove(at:index)
                    //print("finalsubquestionarray removed Object",element)
                }
                else{
                    
                }
                Labfinalsubquestionarray.append(element)
                break
                
            }
            
        }
       Labfinaljsonsubquestionarray.append(contentsOf:Labfinalsubquestionarray)
        Labfinalsubquestionarray.removeAll()
        print("finaljsonsubquestionarray data",Labfinaljsonsubquestionarray)
        
        if selectedIndx != sender.tag {
            
            self.thereIsCellTapped = true
            self.selectedIndx = sender.tag
            
            
        }
//        else {
//
//            self.thereIsCellTapped = false
//            self.selectedIndx = -1
//
//        }
        
        tableviewLabCheckList.reloadData()
    }
    
    @objc func btnNoClick(sender:UIButton!){
        
        if selectedIndx == sender.tag {
            self.thereIsCellTapped = false
            self.selectedIndx = -1
            
        }
//        else {
//            
//            self.thereIsCellTapped = true
//            self.selectedIndx = sender.tag
//        }
        
        let tempArray = self.MainArray
        
        self.MainArray.removeAll()
        self.sections.removeAll()
        
        for dict in tempArray{
            
            self.subArray.removeAll()
            let subQesArray = dict.value(forKey: "SubQuestion") as! [AnyObject]
            let Checkid = dict.value(forKey: "Checkid")
            let Material = dict.value(forKey: "Material")
            
            for dictSub in subQesArray {
                
                let QuesType = (dictSub as AnyObject).value(forKey: "QuesType") as! String
                let Question = (dictSub as AnyObject).value(forKey: "Question")
                let Subcheckid = (dictSub as AnyObject).value(forKey: "Subcheckid")
                let Subid = (dictSub as AnyObject).value(forKey: "Subid")
                let IsRemarkNeeded = (dictSub as AnyObject).value(forKey: "IsRemarkNeeded")
                
                var subDictionary = NSDictionary()
                
                if QuesType == "Textbox"{
                    
                    subDictionary = ["QuesType":QuesType,"Question":Question!,"Subcheckid":Subcheckid!,"Subid":Subid!,"subQueAns":"","IsRemarkNeeded":IsRemarkNeeded as Any]
                }
                else if QuesType == "RadioButton"{
                    
                    subDictionary = ["QuesType":QuesType,"Question":Question!,"Subcheckid":Subcheckid!,"Subid":Subid!,"subQueAns":"No","IsRemarkNeeded":IsRemarkNeeded as Any]
                }
                
                self.subArray.append(subDictionary as AnyObject)
            }
            
            let dictionary = ["SubQuestion":self.subArray,"Checkid":Checkid!,"Material":Material!,"mainQueAns":""]
            
            self.MainArray.append(dictionary as AnyObject)
        }
        
        print(self.MainArray)
        
        for (index , dict) in self.MainArray.enumerated()
        {
            let str = dict.value(forKey:"Material") as! String
            let ceckId = dict.value(forKey:"Checkid") as! Int
            
            let title = ("\(index+1)\(". ")\(str)")  // ("\(ceckId)\(". ")\(str)")
            let subQesArray = dict.value(forKey: "SubQuestion")
            print(subQesArray)
            // self.mainquestionindex = index
            self.sections.append(contentsOf:[Category(title:title, checkId:ceckId, items:subQesArray as! [[String : Any]])])
            //self.mainquestionindex = index + 1
        }
        
        tableviewLabCheckList.reloadData()
        
        
    }
    
    
    @IBAction func btnSaveNextClicked(_ sender: Any) {
        
        //   finaljsonsubquestionarray.append(contentsOf:finalsubquestionarray)
        // finalsubquestionarray.removeAll()

        Labfinaljsonsubquestionarray.append(contentsOf:Labsubquestionarray)
        //print("All question's Answer data Lastly",Labfinaljsonsubquestionarray)
        
        var sum = Int()
        var tempArray = [AnyObject]()
        
        for dict in self.MainArray{
            
            let MainAns = dict.value(forKey: "mainQueAns") as! String
            
            if MainAns == "Yes"{
                
                sum = sum + 1
                
                let array = (dict as AnyObject).value(forKey: "SubQuestion") as! NSArray
                
                for dic in array{
                    
                    tempArray.append(dic as AnyObject)
                }
                
            }
            else{
                sum = sum - 1
            }
            
        }
        
        if sum == 0
        {
            callSaveData()
        }
        else if sum != 0
        {
            
            var sumSub = Int()
            for dictionary in tempArray{
                
                let subAns = (dictionary as AnyObject).value(forKey: "subQueAns") as! String
                
                if subAns != ""{
                    sumSub = sumSub + 1
                }
                else {
                    sumSub = sumSub - 1
                }
            }
            
            if sumSub == tempArray.count{
                callSaveData()
            }
            else{
                Alert().showAlertMessage(vc: self, titleStr: "Warning!", messageStr: "Please Fill All mandatory field")
            }
        }
        

    }
    
    func callSaveData(){


        ////////////////////////////////////////////////////////
        var sampleArray = [AnyObject]()
        
        for dict in self.MainArray{
            
            let MainAns = dict.value(forKey: "mainQueAns") as! String
            
            if MainAns == "Yes"{
                
                sampleArray.append(dict as AnyObject)
            }
        }
        
        
        if labObject.isEmpty == false
        {
            
            var flagYesNo = String()
            
            if sampleArray.isEmpty{
                
                let finalDict = ["checklist_json":sampleArray] as NSDictionary
                var jsonObject = [String:Any]()
                jsonObject = ["Regd_json":labObject,"DR_LAB_json":"","CheckList_JSON":finalDict]
                print("jsonObject of Doctor",jsonObject)
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier:"labWishlist") as! MediprocsWishListLabViewController
                nextViewController.storeObject = jsonObject
                self.present(nextViewController, animated:false, completion:nil)
                
            }
            else{
            
            outerLoop: for dict in sampleArray{
                
                let subQesArray = (dict as AnyObject).value(forKey: "SubQuestion") as! NSArray
                
                for dictSub in subQesArray{
                    
                    let Subcheckiddd = (dictSub as AnyObject).value(forKey: "Subid") as? NSNumber
                    
                    if Subcheckiddd == 5 || Subcheckiddd == 6 {
                        
                        let subQueAns = (dictSub as AnyObject).value(forKey: "subQueAns") as? String
                        
                        if subQueAns == "Yes"{
                            
                            let remark = (dictSub as AnyObject).value(forKey: "Remark") as? String
                            
                            if remark == ""
                            {
                                flagYesNo = "No"
                                break outerLoop
                            }
                            else
                            {
                                flagYesNo = "Yes"
                            }
                        }
                        else if subQueAns == "No"{
                            
                            flagYesNo = "Yes"
                        }
                    }
                }
            }
        
         if flagYesNo == "Yes"{
            
            let finalDict = ["checklist_json":sampleArray] as NSDictionary
            var jsonObject = [String:Any]()
            jsonObject = ["Regd_json":labObject,"DR_LAB_json":"","CheckList_JSON": finalDict]
            print("jsonObject of Doctor",jsonObject)
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier:"labWishlist") as! MediprocsWishListLabViewController
            nextViewController.storeObject = jsonObject
            self.present(nextViewController, animated:false, completion:nil)
            
         }
         else
         {
            Alert().showAlertMessage(vc: self, titleStr:"Warning !", messageStr:"Please enter remark")
         }
            
       }
    }
   else{
    
        let alert = UIAlertController(title: "Status", message:"Please Fill All mandatory field", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
            
      }
        
}

    
}

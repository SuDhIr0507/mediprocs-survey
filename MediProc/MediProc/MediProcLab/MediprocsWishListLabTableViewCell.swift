//
//  MediprocsWishListLabTableViewCell.swift
//  MediProc
//
//  Created by codeworx on 9/24/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit

class MediprocsWishListLabTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblBrandName: UILabel!
    @IBOutlet weak var lblSpecification: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}

//
//  MediProcLabViewController.swift
//  MediProc
//
//  Created by Avante on 14/03/19.
//  Copyright © 2019 Avante. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation
import SkyFloatingLabelTextField
import Alamofire

class MediProcLabViewController: UIViewController,UITextFieldDelegate,CLLocationManagerDelegate,NVActivityIndicatorViewable,UITextViewDelegate {
    
    fileprivate let pickerDistrict = ToolbarPickerView()
    fileprivate let pickerTaluka = ToolbarPickerView()
    fileprivate let pickerCity = ToolbarPickerView()
    
    fileprivate var districtArray = [[String : AnyObject]]()
    fileprivate var talukaArray = [[String : AnyObject]]()
    fileprivate var cityArray = [[String : AnyObject]]()
    
    var districtID = Int()
    var talukaID = Int()
    var cityID = Int()
    var flag = String()
    
    @IBOutlet weak var txtSelectTaluka: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSelectDistrict: SkyFloatingLabelTextField!
    @IBOutlet weak var txtSelectCity: SkyFloatingLabelTextField!
    
    @IBOutlet weak var navview: UIView!
    @IBOutlet weak var LabInfoview: UIView!
    @IBOutlet weak var OwnerInfoview: UIView!
    @IBOutlet weak var textfieldLabName: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldLandlineNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldEmailID: SkyFloatingLabelTextField!
    @IBOutlet weak var textviewAddress: UITextView!

    @IBOutlet weak var textfieldAvgnoPatient: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldAvgNoTestOutsource: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldMiddleName: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var textfieldOwnerMobileNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldOwnerEmailID: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldCurrentLocation: SkyFloatingLabelTextField!
    @IBOutlet weak var textfieldVendorName: UITextField!
    @IBOutlet weak var btnRefresh: UIButton!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var btnSaveNext: UIButton!
    @IBOutlet weak var btnLabInfo: UIButton!
    @IBOutlet weak var btnLabCheckList: UIButton!
    
    var locationManager = CLLocationManager()
    var didFindMyLocation = false
    var latitude = Double()
    var longitude = Double()
    var currentlatitude = Double()
    var currentlongitude = Double()
    var gender = String()
    var combinestring = String()
    var array = [AnyObject]()
    var Labarray = [AnyObject]()

    
    override func viewDidLoad() {
        super.viewDidLoad()


        gender = "1"
        
        txtSelectDistrict.delegate = self
        txtSelectTaluka.delegate = self
        txtSelectCity.delegate = self
        
        addRightTextButon(viewText: txtSelectDistrict, imagename: "dropdown")
        addRightTextButon(viewText: txtSelectTaluka, imagename: "dropdown")
        addRightTextButon(viewText: txtSelectCity, imagename: "dropdown")
        
        self.pickerDistrict.dataSource = self
        self.pickerDistrict.delegate = self
        self.pickerDistrict.toolbarDelegate = self
        
        self.pickerTaluka.dataSource = self
        self.pickerTaluka.delegate = self
        self.pickerTaluka.toolbarDelegate = self
        
        self.pickerCity.dataSource = self
        self.pickerCity.delegate = self
        self.pickerCity.toolbarDelegate = self
        
        
        self.txtSelectDistrict.inputView = self.pickerDistrict
        self.txtSelectDistrict.inputAccessoryView = self.pickerDistrict.toolbar
        
        self.txtSelectTaluka.inputView = self.pickerTaluka
        self.txtSelectTaluka.inputAccessoryView = self.pickerTaluka.toolbar
        
        self.txtSelectCity.inputView = self.pickerCity
        self.txtSelectCity.inputAccessoryView = self.pickerCity.toolbar
        
       
        applyGradientView()
        applyGradient()
        
        
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x:0, y: btnLabInfo.frame.height + 1, width:btnLabInfo.frame.width+1, height:1.0)
        bottomBorder.backgroundColor = Color().hexStringToUIColor(hex: "#0060C5").cgColor
        btnLabInfo.layer.addSublayer(bottomBorder)

        LabInfoview.layer.cornerRadius = 5
        OwnerInfoview.layer.cornerRadius = 5
        LabInfoview.layer.cornerRadius = 5
        
        textviewAddress.layer.borderWidth = 0.5
        textviewAddress.layer.borderColor = UIColor.lightGray.cgColor
        textviewAddress.textContainerInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
        btnRefresh.layer.borderWidth = 0.5
        btnRefresh.layer.borderColor = Color().hexStringToUIColor(hex: "#1561AD").cgColor
        btnRefresh.layer.cornerRadius = 10
        
        btnClear.layer.borderWidth = 1.0
        btnClear.layer.borderColor = Color().hexStringToUIColor(hex: "#1561AD").cgColor
        btnClear.layer.cornerRadius = 10
        
        btnSaveNext.layer.borderWidth = 0.5
        btnSaveNext.layer.borderColor = Color().hexStringToUIColor(hex: "#1561AD").cgColor
        btnSaveNext.layer.cornerRadius = 10
        
        // For use in foreground
        
        let toolBar1 = UIToolbar()
        toolBar1.barStyle = UIBarStyle.default
        toolBar1.isTranslucent = true
        let space1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton1 = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcLabViewController.doneDatePickerPressed1))
        
        toolBar1.setItems([space1, doneButton1], animated: false)
        toolBar1.isUserInteractionEnabled = true
        toolBar1.sizeToFit()
        textfieldMobileNumber.inputAccessoryView = toolBar1
        
        
        let toolBar2 = UIToolbar()
        toolBar2.barStyle = UIBarStyle.default
        toolBar2.isTranslucent = true
        let spaceLandlineNumber = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButtonLandlineNumber = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcLabViewController.doneDatePickerPressedLandlineNumber))
        
        toolBar2.setItems([spaceLandlineNumber,doneButtonLandlineNumber], animated: false)
        toolBar2.isUserInteractionEnabled = true
        toolBar2.sizeToFit()
        textfieldLandlineNumber.inputAccessoryView = toolBar2
        
        let toolBarEmailID = UIToolbar()
        toolBarEmailID.barStyle = UIBarStyle.default
        toolBarEmailID.isTranslucent = true
        let spaceEmailID = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButtonEmailID = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcLabViewController.doneDatePickerPressedEmailID))
        
        toolBarEmailID.setItems([spaceEmailID, doneButtonEmailID], animated: false)
        toolBarEmailID.isUserInteractionEnabled = true
        toolBarEmailID.sizeToFit()
        textfieldEmailID.inputAccessoryView = toolBarEmailID
        
        let toolBarAveragePatient = UIToolbar()
        toolBarAveragePatient.barStyle = UIBarStyle.default
        toolBarAveragePatient.isTranslucent = true
        let spaceAveragePatient = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButtonAveragePatient = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcLabViewController.doneDatePickerPressedAveragePatient))
        toolBarAveragePatient.setItems([spaceAveragePatient, doneButtonAveragePatient], animated: false)
        toolBarAveragePatient.isUserInteractionEnabled = true
        toolBarAveragePatient.sizeToFit()
        textfieldAvgnoPatient.inputAccessoryView = toolBarAveragePatient
        
        let toolBarAverageTestOutsource = UIToolbar()
        toolBarAverageTestOutsource.barStyle = UIBarStyle.default
        toolBarAverageTestOutsource.isTranslucent = true
        let spaceAverageTestOutsource = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButtonAverageTestOutsource = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcLabViewController.doneDatePickerPressedAverageTestOutsource))
        toolBarAverageTestOutsource.setItems([spaceAverageTestOutsource, doneButtonAverageTestOutsource], animated: false)
        toolBarAverageTestOutsource.isUserInteractionEnabled = true
        toolBarAverageTestOutsource.sizeToFit()
        textfieldAvgNoTestOutsource.inputAccessoryView = toolBarAverageTestOutsource
        
        
        let toolBarOwnerEmailID = UIToolbar()
        toolBarOwnerEmailID.barStyle = UIBarStyle.default
        toolBarOwnerEmailID.isTranslucent = true
        let spaceOwnerEmailID = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButtonOwnerEmailID = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcLabViewController.doneDatePickerPressedOwnerEmailID))
        
        toolBarOwnerEmailID.setItems([spaceOwnerEmailID, doneButtonOwnerEmailID], animated: false)
        toolBarOwnerEmailID.isUserInteractionEnabled = true
        toolBarOwnerEmailID.sizeToFit()
        textfieldOwnerEmailID.inputAccessoryView = toolBarOwnerEmailID
        
        
        let toolBar3 = UIToolbar()
        toolBar3.barStyle = UIBarStyle.default
        toolBar3.isTranslucent = true
        let space3 = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton3 = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(MediProcLabViewController.doneDatePickerPressed3))
        
        toolBar3.setItems([space3, doneButton3], animated: false)
        toolBar3.isUserInteractionEnabled = true
        toolBar3.sizeToFit()
        textfieldOwnerMobileNumber.inputAccessoryView = toolBar3
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Reachability().isConnectedToNetwork() == true {
            print("Connected to the internet")
            
            ////////// Get district list //////////////
            
            let districtarray = UserData().getDistrictArrayData().globalDistrictArray
            if districtarray.count != 0 {
                self.districtArray = districtarray as! [[String : AnyObject]]
                pickerDistrict.reloadAllComponents()
            }
            else{
                WebserviceCall().getAllDistrictFromServer({ Array in
                    self.districtArray = Array as! [[String : AnyObject]]
                    self.pickerDistrict.reloadAllComponents()
                })
            }
            
            //////////////////////////////////////////////
            
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }
            
            self.locationManager.delegate = self
            self.locationManager.startUpdatingLocation()
            
            
        } else {
            print("No internet connection")
           
            ////////// Get District list //////////////
            
            let districtarray = UserData().getDistrictArrayData().globalDistrictArray
            if districtarray.count != 0 {
                self.districtArray = districtarray as! [[String : AnyObject]]
                pickerDistrict.reloadAllComponents()
            }
            else{
                Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please Check Your Internet Connection")
            }
            
            //////////////////////////////////////////////
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == txtSelectDistrict
        {
            flag = "District"
            txtSelectTaluka.text = ""
            talukaID = 0
            txtSelectCity.text = ""
            cityID = 0
            self.txtSelectDistrict.inputView = self.pickerDistrict
            self.txtSelectDistrict.inputAccessoryView = self.pickerDistrict.toolbar
        }
        else if textField == txtSelectTaluka
        {
            if txtSelectDistrict.text == "" && districtID == 0
            {
                txtSelectTaluka.resignFirstResponder()
                Alert().showAlertMessage(vc:self, titleStr: "Error", messageStr:"Please select district")
            }
            else{
                flag = "Taluka"
                txtSelectCity.text = ""
                cityID = 0
                self.txtSelectTaluka.inputView = self.pickerTaluka
                self.txtSelectTaluka.inputAccessoryView = self.pickerTaluka.toolbar
                getTalukaAndCityData(districtId:districtID)
            }
        }
        else if textField == txtSelectCity
        {
            if txtSelectDistrict.text == "" && districtID == 0
            {
                txtSelectCity.resignFirstResponder()
                Alert().showAlertMessage(vc:self, titleStr: "Error", messageStr:"Please select district")
            }
            else{
                flag = "City"
                self.txtSelectCity.inputView = self.pickerCity
                self.txtSelectCity.inputAccessoryView = self.pickerCity.toolbar
                getTalukaAndCityData(districtId:districtID)
            }
        }
    }
    
    @objc func doneDatePickerPressedAveragePatient()
    {
        self.view.endEditing(true)
        
        textfieldAvgnoPatient.resignFirstResponder()
    }
    
    @objc func doneDatePickerPressedAverageTestOutsource()
    {
        self.view.endEditing(true)
        
        textfieldAvgNoTestOutsource.resignFirstResponder()
    }
    
    @objc func doneDatePickerPressedLandlineNumber(){
        self.view.endEditing(true)
        
        textfieldLandlineNumber.resignFirstResponder()
        
//        if validateLandline(value: textfieldLandlineNumber.text!)
//        {
//            //print("Valid Landline number",textfieldLandlineNumber.text)
//        }
//        else
//        {
//            let alert = UIAlertController(title: "Warning", message: "Please Enter Valid Landline Number", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
        
    }
    
    @objc func doneDatePickerPressedEmailID(){
        self.view.endEditing(true)
        
        textfieldEmailID.resignFirstResponder()
        
//        if isValidEmail(testStr: textfieldEmailID.text!) == false
//        {
//            let alert = UIAlertController(title: "Warning", message: "Please Enter Valid Email ID", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//            self.present(alert, animated: true, completion: nil)
//        }
        
    }
    
    @objc func doneDatePickerPressed1(){
        self.view.endEditing(true)
        
        textfieldMobileNumber.resignFirstResponder()
        
        if validateMobile(value: textfieldMobileNumber.text!) == false
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Valid Mobile Number", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func doneDatePickerPressedOwnerEmailID(){
        self.view.endEditing(true)
        
        textfieldOwnerEmailID.resignFirstResponder()
        
        if isValidEmail(testStr: textfieldOwnerEmailID.text!) == false
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Valid Email ID", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func doneDatePickerPressed3(){
        self.view.endEditing(true)
        
        textfieldOwnerMobileNumber.resignFirstResponder()
        
        if validateMobile(value: textfieldOwnerMobileNumber.text!) == false
        {
            let alert = UIAlertController(title: "Warning", message: "Please Enter Valid Mobile Number", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        
        currentlatitude = (location?.coordinate.latitude)!
        currentlongitude = (location?.coordinate.longitude)!
     //   print("currentlatitude",currentlatitude)
       // print("currentlongitude",currentlongitude)
        let lat = String(format: "%.7f", currentlatitude)
        let longit = String(format: "%.7f", currentlongitude)
         combinestring = lat + "," + longit
    
        print("combinestring",combinestring)
        textfieldCurrentLocation.text = ("\(combinestring)")
           locationManager.stopUpdatingLocation()
         getAddressFromLatLon(pdblLatitude: lat, withLongitude: longit)
        
        
    }
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
     
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks as? [CLPlacemark]
                
                if pm?.count ?? 0 > 0 {
                    let pm = placemarks![0]
                    
//                    print("placemark array",pm)
//
//                    print(pm.country)
//                    print(pm.locality)
//                    print(pm.subLocality)
//                    print(pm.thoroughfare)
//                    print(pm.postalCode)
//                    print(pm.subThoroughfare)
                    
                    var addressString : String = ""
                    //                    if pm.areasOfInterest != nil
                    //                    {
                    //                        addressString = addressString + pm.areasOfInterest! + ", "
                    //                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.subThoroughfare != nil {
                        addressString = addressString + pm.subThoroughfare! + "\n"
                    }
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.administrativeArea != nil
                    {
                        addressString = addressString + pm.administrativeArea! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                
                    print(addressString)
                    self.textviewAddress.text = addressString
                    
                    
                }
                
        })
        
    }
    
    
    func GetAddressApi()
    {
        
        //let urlString = "https://maps.googleapis.com/maps/api/geocode/json?latlng=18.512730,73.926720&key=AIzaSyBLPzuix_QbeNkpg4CvvDym-591afIMR4s"
        
        let urlString = "https://maps.googleapis.com/maps/api/geocode/json?"
        let params: Parameters = ["latlng":combinestring,"key":"AIzaSyAUlF_pBZZVArSb6ZeiI8NfytWjdtraP0M"]
        print(urlString)
        Alamofire.request(urlString, method: .get, parameters: params)
            .responseJSON { response in
                
                if response.result.isSuccess
                {
                    self.stopAnimating()
                    
                    if let result: Any = response.result.value{
                        let JSON = result as! NSDictionary
                        print(JSON)
                        let responsearray = (JSON.value(forKey:"results") as AnyObject) as? NSArray
                        if responsearray != nil
                        {
                            let dict = responsearray?.firstObject as? NSDictionary
                            let address = dict?.value(forKey: "formatted_address") as? String
                            print("Address of location",address)
                            self.textviewAddress.text = address
                            
                        }
                    }
                    else if response.result.isFailure
                    {
                        
                    }
                }
        }
    }
    
    func checkLocation()
    {
        // 1
        let status  = CLLocationManager.authorizationStatus()
        
        // 2
        if status == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
            return
        }
        
        // 3
        if status == .denied || status == .restricted {
            let alert = UIAlertController(title: "Location Services Disabled", message: "Please Enable Location Services in Settings", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: .default, handler:{action in
                
                let settingsUrl = URL(string: UIApplication.openSettingsURLString)
                if UIApplication.shared.canOpenURL(settingsUrl!)  {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl!, completionHandler: { (success) in
                        })
                    }
                    else  {
                        let url = URL(string : "prefs:root=")
                        if UIApplication.shared.canOpenURL(url!) {
                            UIApplication.shared.openURL(url!)
                        }
                    }
                }
                
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler:nil)
            
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            
            present(alert, animated: true, completion: nil)
            return
        }
        
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
    }
    
    func applyGradientView()
    {
        let GLlayer = CAGradientLayer()
        let hexclass = HexFromColorCodeClass()
        var  first = UIColor()
        var  second = UIColor()
        first = hexclass.hexStringToUIColor(hex:"#0060C5")
        second = hexclass.hexStringToUIColor(hex:"#2BB1FE")
        GLlayer.startPoint = CGPoint(x: 0, y: 0.5)
        GLlayer.endPoint = CGPoint(x: 1, y: 0.5)
        GLlayer.colors = [first,second]
        
        self.navview.layer.addSublayer(GLlayer)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func applyGradient() {
        
        let gradient = CAGradientLayer()
        let hexclass = HexFromColorCodeClass()
        
        let Start = hexclass.hexStringToUIColor(hex:"#0060C5").cgColor
        let End = hexclass.hexStringToUIColor(hex:"#2BB1FE").cgColor
        
        gradient.colors = [Start,End]
        gradient.locations = [0.0,0.5,1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.frame = CGRect(origin: CGPoint.zero, size: self.view.frame.size) //self.view.bounds
        self.view.layer.insertSublayer(gradient, at:0)
    }
    @IBAction func btnLabInfoClicked(_ sender: Any) {
        
    }
    
    @IBAction func btnCheckListClicked(_ sender: Any) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        
        textfieldLabName.resignFirstResponder()
        textviewAddress.resignFirstResponder()
        textfieldFirstName.resignFirstResponder()
        textfieldMiddleName.resignFirstResponder()
        textfieldLastName.resignFirstResponder()
        textfieldVendorName.resignFirstResponder()
    
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textfieldMobileNumber || textField == textfieldOwnerMobileNumber 
        {
            
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 10
            
        }
        if textField == textfieldLandlineNumber
        {
            
            let currentCharacterCount = textField.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 14
            
        }
        if textField == textfieldFirstName ||  textField == textfieldLabName || textField == textfieldVendorName
        {
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
        if textField == textfieldMiddleName
        {
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
        if textField == textfieldLastName
        {
            let maxLength = 100
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            return true && newString.length <= maxLength
        }
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            
            textviewAddress.resignFirstResponder()
            return false
        }
        return true
    }

    @IBAction func btnBackClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "MediProcHomeVC") as! MediProcHomeViewController
        self.present(nextViewController, animated:false, completion:nil)
    }
    
    
    @IBAction func btnMaleClicked(_ sender: Any) {
        
        self.btnMale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
        self.btnFemale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        gender = "1"
        
    }
    
    @IBAction func btnFemaleClicked(_ sender: Any) {
        
        self.btnMale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        self.btnFemale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
        gender = "2"
    }
    @IBAction func btnRefreshClicked(_ sender: Any) {
        
        checkLocation()
        
    }
    
    @IBAction func btnClearClicked(_ sender: Any) {
        
        textfieldLabName.text = ""
        textfieldMobileNumber.text = ""
        textfieldLandlineNumber.text = ""
        textfieldEmailID.text = ""
        textviewAddress.text = ""
        
        txtSelectDistrict.text = ""
        txtSelectTaluka.text = ""
        txtSelectCity.text = ""
        
        textfieldAvgnoPatient.text = ""
        textfieldAvgNoTestOutsource.text = ""
        textfieldFirstName.text = ""
        textfieldMiddleName.text = ""
        textfieldLastName.text = ""
        textfieldOwnerMobileNumber.text = ""
        textfieldOwnerEmailID.text = ""
        textfieldVendorName.text = ""
       
        self.btnMale.setImage( UIImage(named: "radio-btn2.png"), for: .normal)
        self.btnFemale.setImage( UIImage(named: "radio-btn1.png"), for: .normal)
        
    }
    
    @IBAction func btnSaveNextClicked(_ sender: Any) {
        
        if textfieldLabName.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter lab name")
        }
        else if textfieldMobileNumber.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please Enter Mobile Number")
        }
        else if validateMobile(value: textfieldMobileNumber.text!) == false
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please Enter Valid Mobile Number")
        }
        else if textviewAddress.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please Enter Address")
        }
        else if  txtSelectDistrict.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please select district")
        }
        else if txtSelectTaluka.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please select taluka")
        }
        else if txtSelectCity.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please select city")
        }
        else if textfieldAvgnoPatient.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter avg no of patient per day")
        }
        else if textfieldAvgNoTestOutsource.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please Enter avg no of test outsource per day")
        }
        else if textfieldFirstName.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter first name")
        }
        else if textfieldLastName.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter last name")
        }
        else if textfieldOwnerMobileNumber.text == ""
        {
             Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please Enter Owner Mobile Number")
        }
        else if validateMobile(value: textfieldOwnerMobileNumber.text!) == false
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please enter valid owner mobile number")
        }
           
        else if textfieldOwnerEmailID.text == ""
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please Enter Owner Email ID")
        }
        else if  isValidEmail(testStr: textfieldOwnerEmailID.text!) == false
        {
            Alert().showAlertMessage(vc: self, titleStr: "Warning !", messageStr: "Please Enter Valid Owner Email ID")
        }
        else
        {
            
            var array = [AnyObject]()
            
            var userid = String()
            let IsHLLLogin = UserDefaults.standard.bool(forKey: "IsHLLLogin")
            
            if IsHLLLogin == true
            {
                userid = (UserDefaults.standard.value(forKey: "HLLUserID") as? String)!
                print("userid value is",userid)
            }
            else if IsHLLLogin == false
            {
                userid = UserData().getUserData().UserID
            }
            
            print("userid os lab",userid)
            let object = ["AvgPatientsPerDay":textfieldAvgnoPatient.text!,"AvgTestsPerDay":textfieldAvgNoTestOutsource.text!,"CITYCODE":cityID as Any,"DISTLGDCODE":districtID,"EmailId":textfieldOwnerEmailID.text!,"FirstName":textfieldFirstName.text!,"GenderId":gender,"LabEmail":textfieldOwnerEmailID.text!,"LabLandLine":textfieldLandlineNumber.text!,"LabMobile":textfieldMobileNumber.text!,"LabName":textfieldLabName.text!,"LastName":textfieldLastName.text!,"Latitude":currentlatitude,"Logitude":currentlongitude,"MidName":textfieldMiddleName.text!,"MobileNo":textfieldOwnerMobileNumber.text!,"RoleID":"3","TALLGDCODE":talukaID,"UserId":userid,"AddedBy":userid,"SupplierDetails":textfieldVendorName.text!] as [String : Any]
            
            
            self.locationManager.stopUpdatingLocation()
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LabCheckListInfoVC") as! MediProcLabCheckListInfoViewController
            nextViewController.labObject = object
            self.present(nextViewController, animated:false, completion:nil)
            
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")

        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    func validateMobile(value: String) -> Bool
    {
        let PHONE_REGEX = "^[6-9][0-9]{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        print("result",result)
        return result
    }
    
    func validateLandline(value: String) -> Bool
    {
        let PHONE_REGEX = "^[0-9]{14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    func addRightTextButon(viewText:UITextField, imagename: String) {
        viewText.rightViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        let image = UIImage(named: imagename)
        imageView.image = image
        viewText.rightView = imageView
    }
    
}

extension MediProcLabViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerDistrict
        {
            return self.districtArray.count
        }
        else if pickerView == pickerTaluka
        {
            return self.talukaArray.count
        }
        else if pickerView == pickerCity
        {
            return self.cityArray.count
        }

        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerDistrict
        {
            return self.districtArray[row]["DISTNAME"] as? String
        }
        else if pickerView == pickerTaluka
        {
            return self.talukaArray[row]["TALNAME"] as? String
        }
        else if pickerView == pickerCity
        {
            return self.cityArray[row]["CityName"] as? String
        }
    
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerDistrict
        {
            self.txtSelectDistrict.text = self.districtArray[row]["DISTNAME"] as? String
            districtID = self.districtArray[row]["DISTLGDCODE"] as? Int ?? 0
        }
        else if pickerView == pickerTaluka
        {
            self.txtSelectTaluka.text = self.talukaArray[row]["TALNAME"] as? String
            talukaID = self.talukaArray[row]["TALLGDCODE"] as? Int ?? 0
        }
        else if pickerView == pickerCity
        {
            self.txtSelectCity.text = self.cityArray[row]["CityName"] as? String
            cityID = self.cityArray[row]["CityCode"] as? Int ?? 0
        }
    
    }
}

extension MediProcLabViewController : ToolbarPickerViewDelegate {
    
    func didTapDone() {
        
        if flag == "District" && districtArray.count > 0{
            let selectedIndex2 = pickerDistrict.selectedRow(inComponent: 0)
            self.txtSelectDistrict.text = self.districtArray[selectedIndex2]["DISTNAME"] as? String
            districtID = self.districtArray[selectedIndex2]["DISTLGDCODE"] as? Int ?? 0
            txtSelectDistrict.resignFirstResponder()
            getTalukaAndCityData(districtId:districtID)
        }
        else if flag == "Taluka" && talukaArray.count > 0{
            let selectedIndex3 = pickerTaluka.selectedRow(inComponent: 0)
            self.txtSelectTaluka.text = self.talukaArray[selectedIndex3]["TALNAME"] as? String
            talukaID = self.talukaArray[selectedIndex3]["TALLGDCODE"] as? Int ?? 0
            txtSelectTaluka.resignFirstResponder()
        }
        else if flag == "City" && cityArray.count > 0{
            let selectedIndex4 = pickerCity.selectedRow(inComponent: 0)
            self.txtSelectCity.text = self.cityArray[selectedIndex4]["CityName"] as? String
            cityID = self.cityArray[selectedIndex4]["CityCode"] as? Int ?? 0
            txtSelectCity.resignFirstResponder()
        }
        else{
            self.txtSelectDistrict.resignFirstResponder()
            self.txtSelectTaluka.resignFirstResponder()
            self.txtSelectCity.resignFirstResponder()
        }
    }
    func getTalukaAndCityData(districtId:Int){
        
        if Reachability().isConnectedToNetwork() == true {
            print("Connected to the internet")
            
            let taluarray = UserData().getTalukaArrayDataDistrictWise(districtId:("\(districtID)")).globalTalukaArray
            print(taluarray)
            if taluarray.count != 0 {
                self.talukaArray = taluarray as! [[String : AnyObject]]
                pickerTaluka.reloadAllComponents()
            }
            else{
                
                WebserviceCall().getAllTalukaOnDistrict(districtCode: ("\(districtID)"), { Array in
                    self.talukaArray = Array as! [[String : AnyObject]]
                    print(self.talukaArray)
                    self.pickerTaluka.reloadAllComponents()
                })
                
            }
            //////////////////////////////////////////
            let cityarray = UserData().getCityArrayDataDistrictWise(districtId:("\(districtID)")).globalCityArray
            print(cityarray)
            if cityarray.count != 0 {
                self.cityArray = cityarray as! [[String : AnyObject]]
                pickerCity.reloadAllComponents()
            }
            else{
                
                WebserviceCall().getAllCityOnDistrict(districtCode: ("\(districtID)"), { Array in
                    self.cityArray = Array as! [[String : AnyObject]]
                    print(self.cityArray)
                    self.pickerCity.reloadAllComponents()
                })
            }
            //////////////////////////////////////////
            
        }
        else{
            
            let taluarray = UserData().getTalukaArrayDataDistrictWise(districtId:("\(districtID)")).globalTalukaArray
            print(taluarray)
            if taluarray.count != 0 {
                self.talukaArray = taluarray as! [[String : AnyObject]]
                pickerTaluka.reloadAllComponents()
            }
            else{
                Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please Check Your Internet Connection")
            }
            //////////////////////////////////////////
            
            let cityarray = UserData().getCityArrayDataDistrictWise(districtId:("\(districtID)")).globalCityArray
            print(cityarray)
            if cityarray.count != 0 {
                self.cityArray = cityarray as! [[String : AnyObject]]
                self.pickerCity.reloadAllComponents()
            }
            else{
                Alert().showAlertMessage(vc:self, titleStr: "Warning", messageStr:"Please Check Your Internet Connection")
            }
            
        }
    }
    func didTapCancel() {
        self.txtSelectDistrict.resignFirstResponder()
        self.txtSelectTaluka.resignFirstResponder()
        self.txtSelectCity.resignFirstResponder()
    }
}
